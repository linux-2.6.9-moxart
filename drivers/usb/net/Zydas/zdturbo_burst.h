#ifndef __ZDTURBO_BURST__H
#define __ZDTURBO_BURST__H
BOOLEAN Turbo_BurstSTA_Check(void);
void Turbo_BurstOn(void);
void Turbo_BurstOff(void);
BOOLEAN Turbo_getBurst_Status(void);
#endif
