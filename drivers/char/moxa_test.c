/*
 * History:
 * Date		Aurhor			Comment
 * 02-23-2006	Victor Yu.		Create it.
 */
//#define __KERNEL_SYSCALLS__
#include <linux/config.h>
#include <linux/proc_fs.h>
#include <linux/devfs_fs_kernel.h>
#include <linux/unistd.h>
#include <linux/string.h>
#include <linux/ctype.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/miscdevice.h>
#include <linux/fcntl.h>
#include <linux/init.h>
#include <linux/poll.h>
#include <linux/spinlock.h>
#include <linux/delay.h>
#include <linux/timer.h>
#include <linux/ioport.h>
#include <linux/interrupt.h>
#include <linux/sched.h>
#include <linux/signal.h>
#include <linux/mm.h>
#include <linux/kmod.h>
#include <asm/io.h>
#include <asm/uaccess.h>
#include <asm/system.h>
#include <asm/irq.h>
#include <asm/bitops.h>

//#define VICTOR_DEBUG
#ifdef VICTOR_DEBUG
#define dbg_printk(x...)	printk(x)
#else
#define dbg_printk(x...)
#endif

static void __exit moxa_test_exit(void)
{
}

static int __init moxa_test_init(void)
{
	while ( 1 );
	return 0;
}

module_init(moxa_test_init);
module_exit(moxa_test_exit);

MODULE_AUTHOR("Victor Yu");
MODULE_LICENSE("GPL");
