#ifndef _H_MXHW_CRYPTO
#define _H_MXHW_CRYPTO

#ifdef DEBUG
#ifdef MODULE
#define DBG	printk
#else
#define DBG printf
#endif
#else
#define DBG( a... )
#endif

#define MXCIPHER_TYPE_ENC		1
#define MXCIPHER_TYPE_DEC		0

#define MXCIPHER_MODE_ECB		0
#define MXCIPHER_MODE_CBC		1
#define MXCIPHER_MODE_OFB		2
#define MXCIPHER_MODE_CFB		3
#define MXCIPHER_MODE_CTR		4
#define MXCIPHER_MODE_END		5

#define MXCIPHER_ALGO_DES		0
#define MXCIPHER_ALGO_3DES		1
#define MXCIPHER_ALGO_AES128	2
#define MXCIPHER_ALGO_AES192	3
#define MXCIPHER_ALGO_AES256	4
#define MXCIPHER_ALGO_END		5
#define MXCIPHER_ALGO_AES		6

#define MAX_MXCIPHER_LEN_KEYS 	32	/* DES (8), 3DES (24), AES (16,24,32) */
#define MAX_MXCIPHER_LEN_IVEC 	16	/* DES (8), 3DES (8), AES (16) */

#define MAX_CIPHER_REQUESTS		16
#define MAX_CIPHER_CLIENTS		32		/* maximum # of processes the driver is willing to handle */
#define MAX_CIPHER_PACKET		4112 	/* MAX_CIPHER_BUFFER+16 */

#endif
