#ifndef _H_MXHW_CRYPTO_IO
#define _H_MXHW_CRYPTO_IO

#include <mxhw_crypto.h>

#define	IOCTLSET_MXCIPHER_INFO	0
#define	IOCTLGET_MXCIPHER_INFO	1
#define	IOCTLSET_MXCIPHER_IVEC	2
#define	IOCTLGET_MXCIPHER_IVEC	3
#define	IOCTLSET_MXCIPHER_SIZE	4

#define CIPHER MCIPHER /* in case some other has the same definition */

typedef struct _CIPHER
{
	u_int	type:8;		/* 0:decrypt or 1:encrypt */
	u_int	algo:8;
	u_int	mode:8;
	u_int	bits:8;
	u_int	klen:16;		/* key lenght in bytes */
	u_int	blen:16;		/* block lenght in bytes */
	u_char	keys[MAX_MXCIPHER_LEN_KEYS];
} CIPHER;

#endif
