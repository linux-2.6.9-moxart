#ifndef _MXHW_CRYPTO_UIO
#define _MXHW_CRYPTO_UIO

int		mxcrypto_uio_register(u_int algo, u_int mode, u_char *key, u_int bits, int enc);
int		mxcrypto_uio_perform(int ctxId, const u_char *input, u_char *output, u_long len, 
			u_char *ivec, u_int ilen);
int		mxcrypto_uio_close(int ctxId);
#endif

