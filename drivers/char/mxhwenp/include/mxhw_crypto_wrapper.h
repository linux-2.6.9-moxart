#ifndef _MXHW_CRYPTO_WRAPPER
#define _MXHW_CRYPTO_WRAPPER

#include <mxhw_crypto.h>

#define HWACC_PKTLEN_MIN		64
#define HWACC_PKTLEN_MIN_DES	160

typedef struct _SSNTAB
{
	u_int 	flag;
	u_int	blen:16;
	u_int	klen:16;
	short	enId, deId;						/* forward/inverse operation */
	void 	*bkey;							/* keys to a session  */
	u_char	ukey[MAX_MXCIPHER_LEN_KEYS];
} SSNTAB;

SSNTAB* mxcrypto_create(const void *userKey, u_int klen, void *key, u_int blen, u_int is_aes);
int		mxcrypto_perfrom(u_int algo, u_int mode, const u_char *input, u_char *output, u_long length, 
					void *key1, void *key2, void *key3, void *ivec, u_int num, int enc);
int		mxcrypto_close(void *key);
void	mxcrypto_set_software(int sw);

#endif
