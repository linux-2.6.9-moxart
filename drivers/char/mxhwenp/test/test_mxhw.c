#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <mxhw_crypto_userio.h>
#include <mxhw_crypto_wrapper.h>
#include <cipher.h>
#include <test_ssl.h>
 
static u_char iv_sw[64], input_sw[MAX_BUF], output_sw[MAX_BUF];
static u_char iv_hw[64], input_hw[MAX_BUF], output_hw[MAX_BUF];

static u_char IV[64];

static void
hexit(char *msg, u_char *d, int len)
{
	int i;
	usleep(10000);
	printf("[%4d] %s ",len, msg);
	len = len>80? 40:len;
#if 1
	for (i=0;i<len;i++,d++)
	{
		printf("%02x",*d);
		if (i%4==3) printf(" ");
	}
#else
	for(;len >=4; d+=4,len-=4)
		printf("%08x ",*(u_int*) d);
	
	if (len>0)
	{
		d+=3;
		for (i=0;i<len;i++,d--)
			printf("%02x",*d);
	}
#endif
	printf("\n");
	usleep(10000);
}

TESTHW TestHW[]= 
{
	{MXCIPHER_ALGO_DES, 	MXCIPHER_MODE_ECB, "DES/ECB",		8,8,0,		test_DES_ecb_encrypt},
	{MXCIPHER_ALGO_DES, 	MXCIPHER_MODE_CBC, "DES/CBC",		8,8,0,		test_DES_cbc_encrypt},
	{MXCIPHER_ALGO_3DES, 	MXCIPHER_MODE_ECB, "3DES/ECB",		8,24,0,		test_DES_ecb3_encrypt},
	{MXCIPHER_ALGO_3DES, 	MXCIPHER_MODE_CBC, "3DES/CBC",		8,24,0,		test_DES_ede3_cbc_encrypt},
#ifdef CPE_ENGINE
	{MXCIPHER_ALGO_DES, 	MXCIPHER_MODE_OFB, "DES/OFB",		8,8,0,		test_DES_ofb_encrypt},
	{MXCIPHER_ALGO_DES, 	MXCIPHER_MODE_CFB, "DES/CFB",		8,8,0,		test_DES_cfb_encrypt},
	{MXCIPHER_ALGO_3DES, 	MXCIPHER_MODE_OFB, "3DES/OFB",		8,24,0,		test_DES_ede3_ofb64_encrypt},
	{MXCIPHER_ALGO_3DES, 	MXCIPHER_MODE_CFB, "3DES/CFB",		8,24,0,		test_DES_ede3_cfb64_encrypt},
#endif

#if OPENSSL_VERSION_NUMBER >= 0x00907000L
	{MXCIPHER_ALGO_AES128, 	MXCIPHER_MODE_ECB, "AES128/ECB",	16,16,1,	test_AES_ecb_encrypt},
	{MXCIPHER_ALGO_AES192, 	MXCIPHER_MODE_ECB, "AES192/ECB",	16,24,1,	test_AES_ecb_encrypt},
	{MXCIPHER_ALGO_AES256, 	MXCIPHER_MODE_ECB, "AES256/ECB",	16,32,1,	test_AES_ecb_encrypt},
	{MXCIPHER_ALGO_AES128, 	MXCIPHER_MODE_CBC, "AES128/CBC",	16,16,1,	test_AES_cbc_encrypt},
	{MXCIPHER_ALGO_AES192, 	MXCIPHER_MODE_CBC, "AES192/CBC",	16,24,1,	test_AES_cbc_encrypt},
	{MXCIPHER_ALGO_AES256, 	MXCIPHER_MODE_CBC, "AES256/CBC",	16,32,1,	test_AES_cbc_encrypt},
	{MXCIPHER_ALGO_AES128, 	MXCIPHER_MODE_CTR, "AES128/CTR",	16,16,0,	test_AES_ctr_encrypt},
	{MXCIPHER_ALGO_AES192, 	MXCIPHER_MODE_CTR, "AES192/CTR",	16,24,0,	test_AES_ctr_encrypt},
	{MXCIPHER_ALGO_AES256, 	MXCIPHER_MODE_CTR, "AES256/CTR",	16,32,0,	test_AES_ctr_encrypt},
#ifdef CPE_ENGINE
	{MXCIPHER_ALGO_AES128, 	MXCIPHER_MODE_CFB, "AES128/CFB",	16,16,0,	test_AES_cfb_encrypt},
	{MXCIPHER_ALGO_AES192, 	MXCIPHER_MODE_CFB, "AES192/CFB",	16,24,0,	test_AES_cfb_encrypt},
	{MXCIPHER_ALGO_AES256, 	MXCIPHER_MODE_CFB, "AES256/CFB",	16,32,0,	test_AES_cfb_encrypt},
	{MXCIPHER_ALGO_AES128, 	MXCIPHER_MODE_OFB, "AES128/OFB",	16,16,0,	test_AES_ofb_encrypt},
	{MXCIPHER_ALGO_AES192, 	MXCIPHER_MODE_OFB, "AES192/OFB",	16,24,0,	test_AES_ofb_encrypt},
	{MXCIPHER_ALGO_AES256, 	MXCIPHER_MODE_OFB, "AES256/OFB",	16,32,0,	test_AES_ofb_encrypt},
#if 0
	{MXCIPHER_ALGO_DES, 	MXCIPHER_MODE_CFB, "DES/CFB64",		8,8,1,		test_DES_cfb64_encrypt},
	{MXCIPHER_ALGO_AES128, 	MXCIPHER_MODE_CFB, "AES128/CFB128",	16,16,1,	test_AES_cfb128_encrypt},
#endif
#endif //0x00907000L
#endif
};

u_int TestHWSizes=sizeof(TestHW)/sizeof(TestHW[0]);

TESTHW*
test_find(char *name)
{
	TESTHW	*t;
	int m;
	
	for (m=0; m < TestHWSizes; m++)
	{
		t = &TestHW[m];
		if (t->do_cipher==NULL || strcmp(t->name, name))
			continue;	
		return t;
	}
	return NULL;
}

static void
test_inside(TESTHW	*t, u_int pktLen, int performance, int idx)
{
	u_int	i, num;
	
	if (performance==0) 
		num=1; 
	else
		num=performance; 
		
	fill_data (IV, t->blen); /* for CTR */
	
	pktLen=fill_data (input_sw, pktLen);
			
	memcpy(input_hw, input_sw, pktLen);
	memcpy(iv_sw, IV, t->blen);
	memcpy(iv_hw, IV, t->blen); 
	
	mxcrypto_set_software(ISHW);
	timeit(ISHW, idx, TSTART, 0);
	for (i=0; i < num; i++)
		t->do_cipher(input_hw, output_hw, pktLen, t->key1,t->key2,t->key3,
						t->mode!=MXCIPHER_MODE_ECB? iv_hw:NULL, DES_ENCRYPT);
	timeit(ISHW, idx, TEND, 0);

	mxcrypto_set_software(ISSW);
	timeit(ISSW, idx, TSTART, 0);
	for (i=0; i < num; i++)
		t->do_cipher(input_sw, output_sw, pktLen, t->key1,t->key2,t->key3,
						t->mode!=MXCIPHER_MODE_ECB? iv_sw:NULL, DES_ENCRYPT);
	timeit(ISSW, idx, TEND, 0);

	if (!performance)
	{
		/* compare the results */
		if (memcmp(output_hw,output_sw, pktLen))
		{
			hexit("USERHW ENCRYPT TO:", output_hw, pktLen);
			hexit("USERSW ENCRYPT TO:", output_sw, pktLen);
		}
		if(t->mode!=MXCIPHER_MODE_ECB && (memcmp(iv_hw,iv_sw, t->blen)))
		{
			hexit("USERHW ENCRYPT IV:", iv_hw, t->blen);
			hexit("USERSW ENCRYPT IV:", iv_sw, t->blen);
		}
	}
	if(t->mode==MXCIPHER_MODE_CTR)
	{
		memcpy(iv_sw, IV, t->blen);
		memcpy(iv_hw, IV, t->blen);
	}
	
	mxcrypto_set_software(ISHW);
	timeit(ISHW, idx, TSTART, 0);
	for (i=0; i < num; i++)
		t->do_cipher(output_hw, input_hw, pktLen, t->key1,t->key2,t->key3,
						t->mode!=MXCIPHER_MODE_ECB? iv_hw:NULL, DES_DECRYPT);
	timeit(ISHW, idx, TEND, 1);

	mxcrypto_set_software(ISSW);
	timeit(ISSW, idx, TSTART, 0);
	for (i=0; i < num; i++)
		t->do_cipher(output_sw, input_sw, pktLen, t->key1,t->key2,t->key3,
						t->mode!=MXCIPHER_MODE_ECB? iv_sw:NULL, DES_DECRYPT);
	timeit(ISSW, idx, TEND, 1);
						
	if (!performance)
	{
		/* compare the results */
		if (memcmp(input_hw,input_sw, pktLen))
		{
			hexit("USERHW DECRYPT TO:", input_hw, pktLen);
			hexit("USERSW DECRYPT TO:", input_sw, pktLen);
		}
		if(t->mode!=MXCIPHER_MODE_ECB && (memcmp(iv_hw,iv_sw, t->blen)))
		{
			hexit("USERHW DECRYPT IV:", iv_hw, t->blen);
			hexit("USERSW DECRYPT IV:", iv_sw, t->blen);
		}
	}
}
			
void
test_correctness(u_int numTimes)
{
	TESTHW	*t;
	u_int	i,m, is_aes,size;
	u_char 	keys[32];
	   
    printf("--------------correctness tests--------------------\n");
    printf("If you see any hex string, then there is something wrong with the tests\n\n");
	for (m=0; m < TestHWSizes; m++)
	{
		t = &TestHW[m];
		if (t->do_cipher==NULL)
			continue;
		
		size = 0;
		printf("<--- operate [%s] ", t->name);
		is_aes = (t->algo>=MXCIPHER_ALGO_AES128)? 1:0;
			
		fill_data (keys, 32);
		test_set_keys(t, keys, is_aes);
		for (i=0; i < numTimes; i++) test_inside(t, 0, 0, 0);
			
		size+=mxcrypto_close(t->key1);
		if (t->aes_dec)
			size+=mxcrypto_close(t->key2);
		printf("in %ld packets\n", size);
	}
}

static void
test_performance(u_int *pktSizes,u_int numPktSizes,u_int numPkts, u_int w)
{
	TESTHW	*t;
	u_int	i,m, is_aes, size=0;
	u_char 	keys[32];
	
   	printf("--------------performance tests--------------------\n");
	
	for (m=0; m < TestHWSizes; m++)
	{
		t = &TestHW[m];
		if (t->do_cipher==NULL)
			continue;
		is_aes = (t->algo>=MXCIPHER_ALGO_AES128)? 1:0;
		times_reset();
		fill_data (keys, 32);
		test_set_keys(t, keys, is_aes);
		for (i=0; i < numPktSizes; i++)
			test_inside(t, pktSizes[i], numPkts, i);

		size=0;
		size+=mxcrypto_close(t->key1);
		if (t->aes_dec)	size+=mxcrypto_close(t->key2);
		timereport(t->name, pktSizes, numPktSizes, numPkts, size);
	}
}

#ifndef CPE_ENGINE
#define swap(a,b)
#else
#if 1
#define SWAP32(v) ((v>>24)|((v&0x00ff0000)>>8)|((v&0x0000ff00)<<8)|(v<<24))
#else
#define SWAP32(v) v
#endif
void
swap(u_char *data, u_int len)
{
	u_int i, v;
	
	i = len%4;
	if (i>0)
		len += (4-i);
		
	for (i=0; i < len; i+=4)
	{
		v = *(u_int*) (data+i);
		*(u_int*)(data+i) = SWAP32(v);
	}
}
#endif

static void
test_hardware_data(TESTHW *t, int type)
{
    u_char 	input[256], output[256], keys[32], ivec[16], ovec[16], cipher[256];
	u_char *ckey, *cive, *text, *ciph;
    u_int 	size=0,dlen,is_aes=(t->algo>=MXCIPHER_ALGO_AES128)? 1:0;;
	
printf("<---- operate %s %s", t->name, type? "encryption":"decryption");

	memset(input, 0, 256);
	memset(output, 0, 256);
	if (t->algo==MXCIPHER_ALGO_DES)
	{
		dlen = sizeof(DES_TEXT[t->mode]);
		ckey = (u_char*) DES_KEYS[t->mode];
		cive = (u_char*) DES_IVEC[t->mode];
		text = (u_char*) DES_TEXT[t->mode];
		ciph = (u_char*) DES_CIPHER[t->mode];
	}
	else if (t->algo==MXCIPHER_ALGO_3DES)
	{
		dlen = sizeof(DES3_TEXT[t->mode]);
		ckey = (u_char*) DES3_KEYS[t->mode];
		cive = (u_char*) DES3_IVEC[t->mode];
		text = (u_char*) DES3_TEXT[t->mode];
		ciph = (u_char*) DES3_CIPHER[t->mode];
	} 
	else  
	{
		int algo = t->algo-MXCIPHER_ALGO_AES128;
			
		dlen = sizeof(AES_TEXT);
		ckey = (u_char*) AES_KEYS[algo%3];
		cive = (u_char*) (t->mode==MXCIPHER_MODE_CTR? AES_CTR_IVEC:AES_IVEC);
		text = (u_char*) AES_TEXT; 
		ciph = (u_char*) AES_CIPHER[t->mode*3+algo];
	}
	
	if (type==DES_DECRYPT)
	{
		u_char *tmp = ciph;
		
		ciph = text;
		text = tmp;
	}
		
	memcpy(keys,  ckey, 	t->klen);
	memcpy(ivec,  cive, 	t->blen);
	memcpy(ovec,  cive, 	t->blen);
	memcpy(input, text,  	dlen);
	memcpy(cipher,ciph, 	dlen);

	/* little endian converted to big endian */
	swap(keys, 	t->klen);
	swap(ivec, 	t->blen);
	swap(ovec, 	t->blen);
	swap(input, dlen);
	swap(cipher, dlen);
	
	if (t->mode==MXCIPHER_MODE_CFB)
	{
		if (t->algo==MXCIPHER_ALGO_DES)
			dlen = 10;
		else if (t->algo==MXCIPHER_ALGO_3DES)
			dlen = 8;
		else
			dlen = 18;
	}
	
	test_set_keys(t, keys, (t->algo>=MXCIPHER_ALGO_AES128));

	mxcrypto_set_software(ISHW);
	t->do_cipher(input, output, dlen, t->key1,t->key2,t->key3,
				(t->mode!=MXCIPHER_MODE_ECB)? ivec:NULL, type);
				
	if (memcmp(cipher, output, dlen)!=0)
	{
		hexit("PLAINKEYS:", keys,	t->klen); 
		if (t->mode != MXCIPHER_MODE_ECB)
		{
			hexit("PLAINIVEC:", ovec,	t->blen);
			hexit("CIPHEIVEC:", ivec,	t->blen);
		}			
//		hexit("PLAINTEXT:", input,	dlen); 
		hexit("CIPHERTXT:", (u_char*)cipher,	dlen);
		if (type==DES_ENCRYPT)
			hexit("ENCRYPTXX:", output,	dlen);
		else
			hexit("DECRYPTXX:", output,	dlen);
		printf("\n");
	}
	size+=mxcrypto_close(t->key1);
	if (is_aes && t->mode!=MXCIPHER_MODE_CTR && t->mode!=MXCIPHER_MODE_OFB)
		size+=mxcrypto_close(t->key2);
	printf (" %ld packets\n", size);
}

static int
test_gloden_pattern() 
{
    u_int 	i;
	TESTHW *t;

   	printf("--------------NIST gloden pattern tests--------------------\n");
	for (i=0; i < TestHWSizes; i++)
	{
		t = &TestHW[i];
		if (t->do_cipher==NULL)
			continue;
		
		test_hardware_data(t, DES_ENCRYPT);
		test_hardware_data(t, DES_DECRYPT);
	}	
	return 0;
}

#ifdef OVERFLOW_TEST
int	mxcrypto_uio_write(int ctxId, const u_char *input, u_long len, u_char *ivec, int ilen);
int	mxcrypto_uio_read(int ctxId, u_char *output, u_long len, u_char *ivec, int ilen);
int	mxcrypto_uio_queue(int ctxId);

static void
test_buffer_overflow(int max)
{
	TESTHW	*t;
	u_int	pktLen=1024, i,j;
	int		encId;

	t = &TestHW[1];
	if (t->do_cipher==NULL)
		return;

	encId = mxcrypto_uio_register(t->algo, t->mode, cipherKey, TYPE_ENCRYPT);
	if (encId<0)
		return;
	printf("operate [%s] %d packets with each in size of %d at file %d\n", t->name, max, pktLen, encId);
		
	fcntl(encId, F_SETFL, O_NONBLOCK);
	for (j=0; j<max; j++)
	{
		printf("write %d\n", mxcrypto_uio_queue(encId));
		if (mxcrypto_uio_write(encId, input_hw, pktLen, iv_hw, t->blen)<0)
		{
			printf("error in write at %d\n", j);
			break;
		}
	}
	sleep(2);
	for (i=0; i<j; i++)
	{
		printf("read %d\n", mxcrypto_uio_queue(encId));
		if (mxcrypto_uio_read(encId, input_hw, pktLen, iv_hw, t->blen)<0)
		{
			printf("error in read at %d\n", i);
			break;
		}
	}	
	mxcrypto_uio_close(encId);
}

static void
test_iocaller_overflow(int max)
{
	TESTHW	*t;
	int	i,j,k;
	int		encId[128];

	t = &TestHW[1];
	if (t->do_cipher==NULL)
		return;
	for (k=0; k < max; k++)
	{
	for (i=0; i<64; i++)
	{
		encId[i] = mxcrypto_uio_register(t->algo, t->mode, cipherKey, TYPE_ENCRYPT);
		if (encId[i]<0)
			break;
		printf("[%d] register at file %d\n", i, encId[i]);
	}
	for (j=i-1; j>=0; j--)	
	{
		printf("[%d] close at file %d\n", j, encId[j]);
		mxcrypto_uio_close(encId[j]);
	}
	}
}
#endif

void
usage(char *pn)
{
	printf("Usage:\n");
	printf("\t** correctness (compared with libcrypto.so) **\n\n");
	printf("\t\t> %s [<0>] [<# of packets>:1]\n\n", pn);
	printf("\t** correctness (NIST standard) **\n\n");
	printf("\t\t> %s <1>\n\n", pn);
	printf("\t** performance **\n\n");
	printf("\t\t> %s <2> [<# of packets>:5000] [<1 for hw, 2 for sw, 0 for both>:both]\n\n", pn);
}

int
main(int argc, char *argv[])
{
	unsigned int	pktSizes[]={128, 256, 384, 512, 640, 768, 896, 1024, 1152, 1280};
//	unsigned int	pktSizes[]={16, 32,  64,   80,  96, 112,  128,  144};
	unsigned int	numTimes, i=0;

	if (argc>1)		i = atoi(argv[1]);
	
	if (i==0) /* [<0>] [<# of runs>] */
	{	
		test_correctness((argc>2)? atoi(argv[2]):1);
	}
	else if (i==1)
	{	
		test_gloden_pattern();
	}
	else if (i==2) /* <2> [<# of packets>] [hw/sw] */
	{ 
		numTimes = sizeof(pktSizes)/sizeof(pktSizes[0]);
	
		test_performance(pktSizes,numTimes,(argc>2)? atoi(argv[2]):5000,(argc>3)? atoi(argv[3]):0);
	}
#ifdef OVERFLOW_TEST
	else if (i==3) /* <3>  */
	{ 
		test_buffer_overflow(64);
	}
	else
	{
		test_iocaller_overflow(64);
	}
#endif
	else
		usage(argv[0]);
	return 0;
}

