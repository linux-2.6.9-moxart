#include <stdio.h>
#include <string.h>
#include <time.h>
#ifndef WIN32
#include <sys/times.h>
#endif
#include <errno.h>
#include <fcntl.h>
#include <stdarg.h>
#include "netp.h"

enum
{
	TCPSERVER,
	TCPCLIENT
};

unsigned int
lookup_ip(char *host)
{
	struct hostent *he;
	unsigned int ip;

	ip = inet_addr(host);
	if (ip==INADDR_NONE)
	{
		he=gethostbyname(host);
		if (!he)
			return 0;
		memcpy(&ip, he->h_addr_list[0],he->h_length);
	}
	return ip;
}

int
bind_interface(int fd, unsigned int ip, int port)
{
	struct sockaddr_in sin;

	memset(&sin,0,sizeof(sin));
	sin.sin_family = AF_INET;
	sin.sin_addr.s_addr=ip;
	sin.sin_port = htons((unsigned short) port);
	if (bind(fd, (struct sockaddr *) &sin,sizeof(sin)) < 0)
		return -1;
	else
		return 0;
}

int
set_nonblocking(int fd)
{
#ifdef WIN32	
	long val=1;
	if (ioctlsocket (fd, FIONBIO, &val) != 0)
#else
	if (fcntl (fd, F_SETFL, O_NONBLOCK) != 0)
#endif
		return -1;
	else
		return 0;
}

int
tcp_socket(char *host, int port, int type)
{
	struct	sockaddr_in sin;
	int		sock;

	memset(&sin,0,sizeof(sin));
	sin.sin_family = AF_INET;
	sin.sin_port = htons((unsigned short)port);
	sin.sin_addr.s_addr = host? lookup_ip(host):htonl(INADDR_ANY);

	sock = socket(AF_INET, SOCK_STREAM, 0);
	if (sock > 0)
	{
	if (type==TCPSERVER)
		bind(sock, (struct sockaddr*) &sin,sizeof(sin));
	else
		connect(sock, (struct sockaddr*) &sin,sizeof(sin));
	}
	return sock;
}

ssize_t
readn(int fd, void *vptr, size_t n)
{
	size_t nleft=n;
	ssize_t nread;
	char *p = vptr;

	while(nleft > 0)
	{
		if ((nread = read(fd, p, nleft)) < 0)
		{
			if (errno==EINTR)
				nread=0;
			else
			return -1;
		} else if (nread==0)
			break;
		nleft -= nread;
		p += nread;
	}
	return n-nleft;
}

ssize_t
writen(int fd, void *vptr, size_t n)
{
	size_t nleft=n;
	ssize_t nread;
	char *p = vptr;

	while(nleft > 0)
	{
		if ((nread = write(fd, p, nleft)) < 0)
		{
			if (errno==EINTR)
				nread=0;
			else
			return -1;
		} else if (nread==0)
			break;
		nleft -= nread;
		p += nread;
	}
	return n;
}

unsigned char Buf[2048];

void
send_cmd (int fd, unsigned int cmd, const char *fmt, ...)
{
    va_list ap;
    size_t  l;
    u_short s;

    va_start (ap, fmt);
    vsnprintf (Buf + 4, sizeof (Buf) - 4, fmt, ap);
    va_end (ap);

	l = strlen (Buf + 4);
	s = (u_short) l;

	memcpy(Buf, &s, 2);
	s = (short) cmd;
	memcpy(Buf+2, &s, 2);

    writen (fd, Buf, 4 + l);
    *(Buf+4+l)=0;
printf("SEND:[%d,%d] %s\n",cmd, l, Buf+4);	
}

#define HANDLER(f) void f (int fd, unsigned int cmd, char *pkt, unsigned int len)

typedef struct
{
    unsigned int cmd;
    HANDLER ((*handler));
} HNDLR;

static int
split_line(char **av, char *pkt, int max)
{
	int n=0;
	
	while(*pkt && n < max)
	{
		while(*pkt==' ') pkt++;
		av[n++] = pkt++;
		while(*pkt==' ') pkt++;
	}
	return n;	
}

/* <name> <runs> <len1> [<len2> ....] */
HANDLER(meta_cipher_process)
{
	char *av[32];
	int i,ac, numPkts;
	double swT, hwT;
	
	ac = split_line(av, pkt, 32);
	if (ac < 3)
		return;
	
	numPkts = atoi(av[1]);
	for (i=2; i < ac; i++)
	{
		if (test_meta_cipher_process(av[0], numPkts, atoi(av[i]), &swT, &hwT))
			continue;
		send_cmd(fd, cmd, "%s %.2f %.2f", av[1], swT, hwT);
	}
}

HNDLR hndls[] = 
{
	{0, NULL},
	{1, meta_cipher_process},
};

int HNDLR_SIZE = sizeof(hndls)/sizeof(hndls[0]);

static void
server_dispatch(int fd)
{
	int n;
	u_short cmd, len;

	while(1)
	{
		n = readn(fd, Buf, 4);
		if (n<=0)
			return;
		memcpy(&len, Buf, 2);
		memcpy(&cmd, Buf+2, 2);
		if (cmd==0)
			break;
		n = readn(fd, Buf, len);
		if (n!=len)
			return;
		*(Buf+len)=0;;
printf("RECV:[%d,%d] %s\n",cmd, len, Buf);	
		
		for (n=0; n < HNDLR_SIZE; n++)
		if (hndls[n].cmd == cmd)
		{
			hndls[n].handler(fd, cmd, Buf, len);
			break;
		}
	}
}

int 
meta_main()
{
	struct	sockaddr_in caddr;
	socklen_t len;
	int		fd, sock;

#if defined(WIN32)
    WSADATA wsa;

    WSAStartup (MAKEWORD (1, 1), &wsa);
#endif /* !WIN32 */
	sock = tcp_socket(NULL,SERVER_PORT,TCPSERVER);
	listen(sock, 5);
printf("listen at port %d\n",SERVER_PORT);
	for (;;)
	{
		len = sizeof(caddr);
		fd  = accept(sock,(struct sockaddr*) &caddr,&len);
		if (fd > 0)
		{
	printf("accept a client at fd %d\n",fd);
			server_dispatch(fd);
			close(fd);
		}
	}
	close(sock);
#if defined(WIN32) && !defined(__CYGWIN__)
    WSACleanup ();
#endif
	return 0;
}



