#include <unistd.h>
#include <sys/types.h>
#ifdef MODULE
#define PTIME printk
#else
#include <time.h>
#include <sys/times.h>
#include <mxhw_crypto_wrapper.h>
#define PTIME printf
#endif
#include <test_ssl.h>

int
BIO_snprintf(char *buf, size_t size, const char *format, ...){return 0;}

static double timeSWT[16],timeHWU[16],timeHWS[16];

u_int
fill_data(u_char *buf, u_int size)
{
	u_char 	*p = buf;
	int 	i;
	static int first=1;

	if (first)
	{
		srand(time(0));
		first=0;
	}
	if (size==0)
		size = 160+((rand()%(MAX_BUF-160))&0xfffffff0);

   	for(i=0; i<size ;i++,p++)
		*p = rand()&0xff;

    return size;
}

	
void
times_reset(u_int numPktSizes)
{
	memset(timeSWT, 0, 16*sizeof(unsigned int));
	memset(timeHWU, 0, 16*sizeof(unsigned int));
	memset(timeHWS, 0, 16*sizeof(unsigned int));
}

unsigned int ixOsServTimestampGet(void);

u_int
timestamps()
{
#ifdef MODULE
	return ixOsServTimestampGet();
#else
	return times(0);
#endif
}

void
timeswhw(double *swT, double *hwT, int idx)
{
	*hwT = timeHWU[idx];
	*swT = timeSWT[idx];
}

void
timereport(char *name, u_int *pktSizes, u_int max, u_int numPkt, u_int total)
{
	unsigned int m;
	double bytes;

	if (max<2)
		return;
		
	PTIME("\n%ld/%ld\t%s*\t%s\t(M)\t(M)\tRATE\t%s*K\n",numPkt, total,name, name, name);
	for(m=0; m < max; m++)
	{
		bytes = (double) pktSizes[m]*numPkt/1e6;

		PTIME("%d\t%.2lf\t%.2lf\t%.2lf\t%.2lf\t%.2lf\t%.2lf\n", 
			pktSizes[m], 							/* # of packets */
			timeHWU[m], 							/* hw total time */
			timeSWT[m], 							/* hw system time */
			(timeHWU[m]!=0)? bytes/timeHWU[m]:0, 	/* hw in M bytes */
			(timeSWT[m]!=0)? bytes/timeSWT[m]:0,	/* sw in M bytes */
			(timeHWU[m]!=0)? timeSWT[m]/timeHWU[m]:0, /* rate */
			timeHWS[m]);							
	}
}

static unsigned int
timestick(unsigned int end, unsigned int start)
{
	return (end<start)? (0xffffffff-start+end+1):(end-start);
}

void
timeit(u_int hwsw, u_int idx, int type, int add) 
{
	static struct tms tstart, tend;
	static clock_t s, e;

	if (idx>15)
		return;
	
	if (type==TSTART)
	{
		s = times(&tstart);
	}
	else
	{
		double d;
		
		e = times(&tend);
		
		if (hwsw==ISSW)
		{
			d = (double) timestick(e, s)/sysconf(_SC_CLK_TCK);
			if (add)
				timeSWT[idx] += d;
			else
				timeSWT[idx] = d;
		}
		else
		{
			d = (double) timestick(e, s)/sysconf(_SC_CLK_TCK);
			if (add)
				timeHWU[idx] += d;
			else
				timeHWU[idx] = d;
			d = (double) timestick(tend.tms_stime,tstart.tms_stime)/sysconf(_SC_CLK_TCK);
			if (add)
				timeHWS[idx] += d;
			else
				timeHWS[idx] = d;
		}
	}
}

void
test_set_keys(TESTHW *t, u_char *keys, int IS_AES)
{
	if (IS_AES) 
	{
#if OPENSSL_VERSION_NUMBER >= 0x00907000L
		if (AES_set_encrypt_key(keys, t->klen<<3, &t->AESKey1) ||
			AES_set_decrypt_key(keys, t->klen<<3, &t->AESKey2))
			return;
		t->key1 = &t->AESKey1;
		t->key2 = &t->AESKey2;
		t->key3 = NULL;
#endif
	}
	else
	{
		DES_set_key_unchecked((DES_cblock*) keys, 		&t->DESks1);
		DES_set_key_unchecked((DES_cblock*) (keys+8), 	&t->DESks2);
		DES_set_key_unchecked((DES_cblock*) (keys+16), 	&t->DESks3);
		t->key1 = &t->DESks1;
		t->key2 = &t->DESks2;
		t->key3 = &t->DESks3;
	}
}

TCIPHER(test_DES_ecb_encrypt)
{
	if (length < HWACC_PKTLEN_MIN_DES || mxcrypto_perfrom(MXCIPHER_ALGO_DES, MXCIPHER_MODE_ECB, 
				(const u_char *) input, output, length, ks1, NULL, NULL, NULL,0, enc))
	{
		u_int len=0;
		while (len < length)
		{
			DES_ecb_encrypt((const_DES_cblock*) (input+len),(DES_cblock*)(output+len), 
				(DES_key_schedule*) ks1,enc);
			len+=8;
		}
	}
}
		     
TCIPHER(test_DES_cbc_encrypt)
{
	DES_ncbc_encrypt(input,output, length,(DES_key_schedule*)ks1,(DES_cblock*)ivec, enc);
}

#ifdef CPE_ENGINE
TCIPHER(test_DES_cfb_encrypt) 
{
	DES_cfb_encrypt(input,output,8,length,(DES_key_schedule*)ks1,(DES_cblock*)ivec, enc);
}

TCIPHER(test_DES_ofb_encrypt)
{
	int blks = 0;
	DES_ofb64_encrypt(input,output,length,(DES_key_schedule*)ks1,(DES_cblock*)ivec, &blks);
}

/* not supported by CPE */
TCIPHER(test_DES_cfb64_encrypt) 
{
	int blks=0;
	DES_cfb64_encrypt(input,output,length,(DES_key_schedule*)ks1,(DES_cblock*)ivec, &blks, enc);
}

#endif

TCIPHER(test_DES_ecb3_encrypt) 
{
	if (length < HWACC_PKTLEN_MIN || mxcrypto_perfrom(MXCIPHER_ALGO_3DES, MXCIPHER_MODE_ECB, 
					(const u_char *) input, output, length, ks1, ks2, ks3, NULL,0, enc))
	{
		u_int len=0;
	
		while (len < length)
		{
			DES_ecb3_encrypt(input+len, output+len, 
			(DES_key_schedule*)ks1, (DES_key_schedule*)ks2, (DES_key_schedule*)ks3, enc);
			len+=8;
		}
	}
}

TCIPHER(test_DES_ede3_cbc_encrypt)
{	
	DES_ede3_cbc_encrypt(input, output, length, 
		(DES_key_schedule*)ks1, (DES_key_schedule*)ks2, (DES_key_schedule*)ks3, (DES_cblock*)ivec, enc);
}

#ifdef CPE_ENGINE
TCIPHER(test_DES_ede3_ofb64_encrypt)
{	
	int blks = 0;
	DES_ede3_ofb64_encrypt(input, output, length, 
		(DES_key_schedule*)ks1, (DES_key_schedule*)ks2, (DES_key_schedule*)ks3, (DES_cblock*)ivec, &blks);
}

TCIPHER(test_DES_ede3_cfb64_encrypt)
{
	DES_ede3_cfb_encrypt(input, output, 8, length, 
		(DES_key_schedule*)ks1, (DES_key_schedule*)ks2, (DES_key_schedule*)ks3, (DES_cblock*)ivec, enc);
}
#endif

#if OPENSSL_VERSION_NUMBER >= 0x00907000L
TCIPHER(test_AES_ecb_encrypt)
{
	if (length < HWACC_PKTLEN_MIN || mxcrypto_perfrom(MXCIPHER_ALGO_AES, MXCIPHER_MODE_ECB, 
					(const u_char *) input, output, length, ks1, NULL, NULL, NULL, 0, enc)!=0)
	{
	u_int len=0;
	while (len < length)
	{
		AES_ecb_encrypt(input+len, output+len, (const AES_KEY *) (enc? ks1:ks2), enc);
		len+=AES_BLOCK_SIZE;
	}
	}
}

TCIPHER(test_AES_cbc_encrypt)
{
	AES_cbc_encrypt(input, output, length, (const AES_KEY *) (enc? ks1:ks2), ivec, enc);
}

TCIPHER(test_AES_ctr_encrypt) 
{
	unsigned char ctr[AES_BLOCK_SIZE];
	int blks=0;

	memset(ctr, 0, AES_BLOCK_SIZE);
	AES_ctr128_encrypt(input, output, length, (const AES_KEY *) ks1, ivec, ctr, &blks);
}

#ifdef CPE_ENGINE
TCIPHER(test_AES_cfb_encrypt)
{
	int blks=0;
	AES_cfb8_encrypt(input, output, length, (const AES_KEY *) ks1, ivec, &blks, enc);
}

TCIPHER(test_AES_cfb128_encrypt)
{
	int blks=0;
	AES_cfb128_encrypt(input, output, length, (const AES_KEY *) ks1, ivec, &blks, enc);
}

TCIPHER(test_AES_ofb_encrypt)
{
	int blks=0;
	AES_ofb128_encrypt(input, output, length, (const AES_KEY *) ks1, ivec, &blks);
}
#endif
#endif
