/*
 * Driver for Option High Speed Mobile Devices.
 *
 *  Copyright (C) 2007  Option International
 *  Copyright (C) 2007  Andrew Bird (Sphere Systems Ltd)	ajb@spheresystems.co.uk
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as
 *  published by the Free Software Foundation.
 *     
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 *	0.1	Option International
 *		2.4 driver 
 *	0.2	Andrew Bird (Sphere Systems Ltd)
 *		Initial port to 2.6, target 2.6.21/22	
 *	0.3	Andrew Bird (Sphere Systems Ltd)
 *		Multiple device support, startup & shutdown cleanups
 *	0.4	Andrew Bird (Sphere Systems Ltd)
 *		Initial support for Circuit Switched interface - no H/W handshaking
 *	0.5	Filip Aben ( Option )
 *		- Removed internal project names from comments
 *		- Removed dependency on Modem port which is not always there 
 *		- Added USB id's for other compatible Option devices
 *      0.6     Marco Himmer ( Option )
 *              - Send REZERO UNIT command to switch from UMS to 3G device
 *              - Move packed out of struct to avoid compiler warning
 *      0.7	Filip Aben
 *      	- Added 2.6.12+ compatability
 *
 */

#include <linux/sched.h>
#include <linux/slab.h>
#include <linux/init.h>
#include <linux/delay.h>
#include <linux/netdevice.h>
#include <linux/inetdevice.h>
#include <linux/etherdevice.h>
#include <linux/module.h>
#include <linux/ethtool.h>
#include <asm/uaccess.h>

#include <linux/usb.h>
#include <linux/timer.h>

#include <linux/tty.h>
#include <linux/tty_driver.h>
#include <linux/tty_flip.h>
#include <linux/kmod.h>

#include <asm/byteorder.h>
#include <linux/version.h>

/* #define DEBUG */
#define HSO_PROC

#ifdef HSO_PROC
#include <linux/proc_fs.h>
#endif

#include <net/arp.h>
#include <linux/ip.h>

/*
  Description of the device:
  
  Interface 0:  Contains the IP network interface on the bulk end points.
                The multiplexed serial ports are using the interrupt and control endpoints.
                Interrupt contains a bitmap telling which multiplexed serialport needs servicing.
  
  Interface 1:  Diagnostics port, uses bulk only, do not submit urbs until the port is opened, as
                this have a huge impact on the network port throughput.
  
  Interface 2:  Standard modem interface - circuit switched interface, should not be used.
 */

#define DRIVER_VERSION				"0.5"
#define MOD_AUTHOR                  "Option Wireless"

/* From CDC Spec Table 24 */
#define CS_INTERFACE_VAL					0x24

#define HSO__MAX_MTU                    1984	/*1536 */
#define DEFAULT_MTU						1500
#define DEFAULT_MRU                     1500

#define CTRL_URB_RX_SIZE                1024
#define CTRL_URB_TX_SIZE                64

#define BULK_URB_RX_SIZE                4096
#define BULK_URB_TX_SIZE                8192

#define MUX_INTR_BUF_SIZE             16
#define MUX_BULK_RX_BUF_SIZE			HSO__MAX_MTU
#define MUX_BULK_TX_BUF_SIZE			HSO__MAX_MTU
#define MUX_BULK_RX_BUF_COUNT         4
#define USB_TYPE_OPTION_VENDOR			0x20

/* These definitions are used with the struct hso_priv flags element
   - use *_bit operations on it. (bit indices not values.)*/
#define HSO_NET_RUNNING                 0
#define HSO_NET_TX_BUSY                 1
#define HSO_CARD_UNPLUG                 2

#define	HSO_NET_TX_TIMEOUT			(HZ*10)

#define SEND_ENCAPSULATED_COMMAND		0x00
#define GET_ENCAPSULATED_RESPONSE		0x01

/* Serial port defines and structs. */
#define HSO_THRESHOLD_THROTTLE			(7*1024)
#define HSO_THRESHOLD_UNTHROTTLE		(2*1024)

/* These definitions used in the Ethernet Packet Filtering requests */
/* See CDC Spec Table 62 */
#define MODE_FLAG_PROMISCUOUS			(1<<0)
#define MODE_FLAG_ALL_MULTICAST			(1<<1)
#define MODE_FLAG_DIRECTED			(1<<2)
#define MODE_FLAG_BROADCAST			(1<<3)
#define MODE_FLAG_MULTICAST			(1<<4)

/* CDC Spec class requests - CDC Spec Table 46 */
#define SET_ETHERNET_MULTICAST_FILTER		0x40
#define SET_ETHERNET_PACKET_FILTER		0x43

#define HSO_SERIAL_FLAG_THROTTLED 0
#define HSO_SERIAL_FLAG_TX_SENT	1
#define HSO_SERIAL_FLAG_RX_SENT	2

/* #define HSO_SERIAL_TTY_MAJOR	245 */	/* Experimental Major. */
#define HSO_SERIAL_MAGIC		0x48534f31

/* Variables and such */
#define HSO_SERIAL_TTY_MINORS       256	/*  Number of ttys to handle */

#define D__(lvl_, fmt, arg...) do { \
printk(lvl_ "[%d:%s]: " fmt "\n", __LINE__, __FUNCTION__, ## arg); } while(0)

#define NFO(args...) D__( KERN_INFO, ##args)
#define ERR(args...) D__( KERN_ERR,  ##args)
#define WARN(args...) D__( KERN_WARNING,  ##args)

#define D_(lvl, args...)  do { if(lvl & debug) NFO( args ); } while(0)

#define D1(args...) D_(0x01, ##args)
#define D2(args...) D_(0x02, ##args)
#define D3(args...) D_(0x04, ##args)
#define D4(args...) D_(0x08, ##args)
#define D5(args...) D_(0x10, ##args)
#define D D1

#define QFO(fmt, args...) do { \
	printk( KERN_INFO "hso: " fmt "\n", ##args); \
} while(0)

#if 0
#define DUMP(buf_, len_)                                                                                \
do {                                                                                                    \
    char info_[256];                                                                                    \
    u8 i_, count_=0;                                                                                    \
    for(i_=0;i_<len_;i_++) {                                                                            \
        count_ += snprintf(info_+count_, sizeof(info_)-count_, "%02x ", ((u8*)buf_)[i_]);               \
        if (i_!= 0 && (i_ % 35) == 0) { count_ += snprintf(info_+count_, sizeof(info_)-count_,"\n");}   \
    }                                                                                                   \
                                                                                                        \
    if (len_) {                                                                                         \
        NFO("%d: dump[%s]", len_, info_);                                                               \
    }                                                                                                   \
} while (0)
#endif

#ifdef DEBUG
static void dbg_dump(int line_count, const char *func_name, unsigned char *buf,
		     unsigned int len)
{
	u8 i = 0;

	printk("[%d:%s]: len %d", line_count, func_name, len);

	for (i = 0; i < len; i++) {
		if (!(i % 16))
			printk("\n    0x%03x:  ", i);
		printk("%02x ", (unsigned char)buf[i]);
	}
	printk("\n");
}
#define DUMP(buf_, len_) dbg_dump(__LINE__, __FUNCTION__, buf_, len_)
#define DUMP1(buf_, len_)   do{ if(0x01 & debug) DUMP(buf_, len_);}while(0)
#endif

enum type_intf {
	MUX_INTERFACE,
	QXDM_INTERFACE,
	CS_INTERFACE
};

enum pkt_parse_state {
	WAIT_IP,
	WAIT_DATA,
	WAIT_SYNC
};

struct option_descriptor {
	u8 length;
	u8 descriptor_type;
	u8 enabled_ports;
} __attribute__ ((packed)) ;

struct hso_priv {
	struct net_device_stats stats;
	struct net_device *net;
	struct usb_device *usb;
	struct option_descriptor option_info;
	unsigned long flags;
	u32 properties;

	struct proc_dir_entry *ourproc;

	int mux_ep_intr;
	int mux_ep_intr_size;
	void *mux_intr_buf;
	struct urb *mux_intr_urb;

	int mux_ep_bulk_in;
	int mux_ep_bulk_out;
	int mux_ep_bulk_out_size;
	int mux_ep_bulk_in_size;
	int mux_bInterval;
	struct urb *mux_bulk_rx_urb_pool[MUX_BULK_RX_BUF_COUNT];
	struct urb *mux_bulk_tx_urb;
	void *mux_bulk_rx_buf_pool[MUX_BULK_RX_BUF_COUNT];
	void *mux_bulk_tx_buf;

	spinlock_t net_lock;
	struct sk_buff *skb_rx_buf;
	enum pkt_parse_state rx_parse_state;
	unsigned short rx_buf_size, rx_buf_missing;
	struct iphdr rx_ip_hdr;
	struct ethhdr dummy_eth_head;

	__u16 bcdCDC;
	__u16 wMaxSegmentSize;
	__u16 wNumberMCFilters;
	__u16 mode_flags;

/*    struct usb_ctrlrequest  ctrl_req; */
};

struct hso_serial {
	int magic;
	u8 minor;
	u8 mux;	
	enum type_intf type;
	struct hso_priv *odev;

	/* rx/tx urb could be either a bulk urb or a control urb depending
	   on which serial port it is used on. */
	struct urb *rx_urb;
	u8 *rx_data;
	u16 rx_data_length;	/* should contain allocated length */

	struct urb *tx_urb;
	u8 *tx_data;
	u16 tx_data_length;	/* should contain allocated length */
	u16 tx_data_count;
	struct usb_ctrlrequest ctrl_req_tx;
	struct usb_ctrlrequest ctrl_req_rx;

	int ep_bulk_in;		/* for standard ports, not muxed ones */
	int ep_bulk_out;
	int ep_bulk_out_size;
	int ep_bulk_in_size;

	unsigned long flags;

	int (*write_data) (struct hso_serial *serial);

	/* from usb_serial_port */
	struct tty_struct *tty;
	int open_count;
	struct semaphore sem;
	void *private;
};

/*
  Globals
 */
static int debug = 0x00;

static const char driver_name[] = "hso";
static const char *version = __FILE__ ": " DRIVER_VERSION " " MOD_AUTHOR;

static struct tty_driver *tty_drv;

static struct hso_serial *serial_table[HSO_SERIAL_TTY_MINORS];
static spinlock_t serial_table_lock;

static struct proc_dir_entry *hso_proc_dir;
static struct proc_dir_entry *hso_proc_dir_devices;

static struct usb_driver hso_driver;

static struct usb_device_id hso_ids[] = {

	{.match_flags = USB_DEVICE_ID_MATCH_VENDOR | USB_DEVICE_ID_MATCH_PRODUCT | USB_DEVICE_ID_MATCH_INT_CLASS | USB_DEVICE_ID_MATCH_INT_SUBCLASS | USB_DEVICE_ID_MATCH_INT_PROTOCOL, .idVendor = 0x0af0, .idProduct = 0x1000, .bInterfaceClass = 8, .bInterfaceSubClass = 6, .bInterfaceProtocol = 80},
	{USB_DEVICE(0x0af0, 0x6711)},
	{USB_DEVICE(0x0af0, 0x6731)},
	{USB_DEVICE(0x0af0, 0x6751)},
	{USB_DEVICE(0x0af0, 0x6771)},
	{USB_DEVICE(0x0af0, 0x6791)}, 
	{USB_DEVICE(0x0af0, 0x6811)},
	{USB_DEVICE(0x0af0, 0x6911)},
	{USB_DEVICE(0x0af0, 0x6951)},
	{USB_DEVICE(0x0af0, 0x6971)},
	{USB_DEVICE(0x0af0, 0x7011)},
	{USB_DEVICE(0x0af0, 0x7031)},
	{USB_DEVICE(0x0af0, 0x7051)},
	{USB_DEVICE(0x0af0, 0x7071)},
	{USB_DEVICE(0x0af0, 0x7111)},
	{USB_DEVICE(0x0af0, 0x7211)},
	{USB_DEVICE(0x0af0, 0x7251)},
	{USB_DEVICE(0x0af0, 0x7271)},
	{USB_DEVICE(0x0af0, 0x7311)},
	{}
};

#if ( LINUX_VERSION_CODE < KERNEL_VERSION(2,6,20) )
static struct termios *hso_serial_termios[HSO_SERIAL_TTY_MINORS];
static struct termios *hso_serial_termios_locked[HSO_SERIAL_TTY_MINORS];
#else
static struct ktermios *hso_serial_termios[HSO_SERIAL_TTY_MINORS];
static struct ktermios *hso_serial_termios_locked[HSO_SERIAL_TTY_MINORS];
#endif

/*
 * Prototypes
 */
static struct hso_serial *hso_serial_start(struct hso_priv *odev, u8 minor,
		enum type_intf type, u8 num);

static void hso_serial_stop(struct usb_device *dev, u8 minor, struct hso_priv *odev);

static void put_rxbuf_data(struct urb *urb, struct hso_serial *serial);

#if ( LINUX_VERSION_CODE < KERNEL_VERSION(2,6,20) )
static void _hso_serial_set_termios(struct tty_struct *tty,
				    struct termios *old);
#else
static void _hso_serial_set_termios(struct tty_struct *tty,
				    struct ktermios *old);
#endif

static void hso_serial_disconnect(struct usb_device *usb, struct hso_priv *odev);

#if ( LINUX_VERSION_CODE < KERNEL_VERSION(2,6,20) )
static int hso_mux_submit_intr_urb(struct hso_priv *odev,int gfp);
#else
static int hso_mux_submit_intr_urb(struct hso_priv *odev,gfp_t gfp);
#endif

static int hso_mux_serial_read(struct hso_priv *odev, struct hso_serial *serial);

/*
 * Function declarations
 */

#if ( LINUX_VERSION_CODE < KERNEL_VERSION(2,6,20) )

#define kzalloc(x,y) kcalloc(x,1,y)

/* Following functions are copied straight from the 2.6.20 kernel to maintain compatability */

static inline int usb_endpoint_xfer_bulk(const struct usb_endpoint_descriptor *epd)
{
        return ((epd->bmAttributes & USB_ENDPOINT_XFERTYPE_MASK) ==
                USB_ENDPOINT_XFER_BULK);
}

static inline int usb_endpoint_dir_in(const struct usb_endpoint_descriptor *epd)
{
        return ((epd->bEndpointAddress & USB_ENDPOINT_DIR_MASK) == USB_DIR_IN);
}

static inline int usb_endpoint_dir_out(const struct usb_endpoint_descriptor *epd)
{
        return ((epd->bEndpointAddress & USB_ENDPOINT_DIR_MASK) == USB_DIR_OUT);
}

static inline int usb_endpoint_is_bulk_in(const struct usb_endpoint_descriptor *epd)
{
        return (usb_endpoint_xfer_bulk(epd) && usb_endpoint_dir_in(epd));
}

static inline int usb_endpoint_is_bulk_out(const struct usb_endpoint_descriptor *epd)
{
        return (usb_endpoint_xfer_bulk(epd) && usb_endpoint_dir_out(epd));
}


#endif


static int get_free_serial_index(void)
{
	int index;

        spin_lock(&serial_table_lock);

	for(index = 0;index < HSO_SERIAL_TTY_MINORS;index++) {
		if( serial_table[index] == NULL ) {
			spin_unlock(&serial_table_lock);
			return index;
		}
	}
			
	spin_unlock(&serial_table_lock);

	ERR("no free serial devices in table");

        return -1;
}

static void set_serial_by_index(unsigned index,struct hso_serial *serial)
{
        spin_lock(&serial_table_lock);
        serial_table[index] = serial;
        spin_unlock(&serial_table_lock);
}

static struct hso_serial *get_serial_by_index(unsigned index)
{
        struct hso_serial *serial;

        spin_lock(&serial_table_lock);
        serial = serial_table[index];
        spin_unlock(&serial_table_lock);

        return serial;
}

static struct hso_serial *get_serial_by_odev_and_mux(struct hso_priv *odev,
		unsigned mux)
{
        struct hso_serial *serial;
	int i;

        spin_lock(&serial_table_lock);

	for(i = 0;i < HSO_SERIAL_TTY_MINORS;i++) {
		if((serial = serial_table[i]) == NULL)
			continue;

		if(serial->odev == odev && serial->mux == mux) {
			spin_unlock(&serial_table_lock);
			return serial;
		}
	}

        spin_unlock(&serial_table_lock);

        return NULL;
}

/* returns serial struct and checks for validity of tty struct. */
static inline struct hso_serial *get_serial_by_tty(struct tty_struct *tty)
{

	if (tty == NULL || tty->driver_data == NULL) {
		return NULL;
	}

	return (struct hso_serial *) tty->driver_data;
}

/* global driver data */
static int hso_proc_options(char *buf, char **start, off_t offset,
		int count, int *eof, void *data)
{
	int len = 0;

	D1("count: %d", count);

	len += snprintf(buf + len, count - len, "debug: %02x\n", debug);

	return len;
}

/* per device instance data */
static int hso_proc_device_ttys(char *buf, char **start, off_t offset,
		int count, int *eof, void *data)
{
	int len = 0;
	int i;
	struct hso_serial *serial;

	D1("count: %d", count);

        spin_lock(&serial_table_lock);

	for(i = 0;i < HSO_SERIAL_TTY_MINORS;i++) {
		if((serial = serial_table[i]) == NULL)
			continue;

		if(serial->odev == data) {
			len += snprintf(buf + len, count - len, "/dev/%s%d\n",
					tty_drv->name, serial->minor);
		}
	}

        spin_unlock(&serial_table_lock);

	return len;
}

/* TODO: make this a device variable */
const unsigned char dummy_mac[6] = { 0, 1, 2, 3, 4, 5 };

static void packetizeRx(struct hso_priv *odev, unsigned char *ip_pkt,
			unsigned int count)
{
	unsigned short temp_bytes = 0, buffer_offset = 0, frame_len;
	unsigned char *tmp_rx_buf;
	struct ethhdr *eth_head;

#ifdef DEBUG
	D("Rx %d bytes", count);
	DUMP(ip_pkt, min(128, count));
#endif

	while (count) {
		switch (odev->rx_parse_state) {
		case WAIT_IP:	/* waiting for IP header. */
			/* wanted bytes - size of ip header */
			temp_bytes =
			    (count < odev->rx_buf_missing) ? count : odev->rx_buf_missing;

			memcpy(((unsigned char *)(&odev->rx_ip_hdr)) +
			       odev->rx_buf_size, ip_pkt + buffer_offset,
			       temp_bytes);

			odev->rx_buf_size += temp_bytes;
			buffer_offset += temp_bytes;
			odev->rx_buf_missing -= temp_bytes;
			count -= temp_bytes;

			if (!odev->rx_buf_missing) {	/* header is complete allocate
							   an sk_buffer and continue to WAIT_DATA */
				frame_len = ntohs(odev->rx_ip_hdr.tot_len);

				if (frame_len > DEFAULT_MRU) {
					odev->rx_parse_state = WAIT_SYNC;
					continue;
				}
				/* Allocate an sk_buff */
				if (!(odev->skb_rx_buf =
				     dev_alloc_skb(frame_len + 2 +
						   odev->net->hard_header_len))) {
					/* We got no receive buffer. */
					D("could not allocate memory");
					odev->rx_parse_state = WAIT_SYNC;
					return;
				}
				/* Here's where it came from */
				odev->skb_rx_buf->dev = odev->net;

				/* Make some headroom: standard alignment + the ethernet header. */
				skb_reserve(odev->skb_rx_buf,
					    2 + odev->net->hard_header_len);

				/* Copy what we got so far.
				   make room for iphdr after tail. */
				tmp_rx_buf = skb_put(odev->skb_rx_buf, sizeof(struct iphdr));
				memcpy(tmp_rx_buf, (char *)&(odev->rx_ip_hdr),
				       sizeof(struct iphdr));

				/* ETH_HLEN */
				odev->rx_buf_size = odev->net->hard_header_len + sizeof(struct iphdr);

				/* Filip actually use .tot_len */
				odev->rx_buf_missing = frame_len - sizeof(struct iphdr);
				odev->rx_parse_state = WAIT_DATA;
			}
			break;

		case WAIT_DATA:
			temp_bytes =
			    (count <
			     odev->rx_buf_missing) ? count : odev->rx_buf_missing;

			/* copy the rest of the bytes that are left in the buffer
			   into the waiting sk_buf. Make room for temp_bytes after tail. */
			tmp_rx_buf = skb_put(odev->skb_rx_buf, temp_bytes);
			memcpy(tmp_rx_buf, ip_pkt + buffer_offset, temp_bytes);

			odev->rx_buf_missing -= temp_bytes;
			count -= temp_bytes;
			buffer_offset += temp_bytes;
			odev->rx_buf_size += temp_bytes;
			if (!odev->rx_buf_missing) {
				/* Packet is complete. Inject into stack. */
				{
					/* Add fake ethernet header. */
					eth_head = (struct ethhdr *)skb_push(odev->skb_rx_buf,
							odev->net->hard_header_len);	/* decrease headroom */
					memcpy(eth_head, &odev->dummy_eth_head,
					       sizeof(struct ethhdr));
#if 0
					memcpy(eth_head->h_dest, odev->net->dev_addr , ETH_ALEN);   /* driver MAC */
					memcpy(eth_head->h_source, dummy_mac, ETH_ALEN); /* from dummy device */
					eth_head->h_proto = htons(ETH_P_IP);
#endif

					/* Not sure here either */
					odev->skb_rx_buf->protocol = eth_type_trans(odev->skb_rx_buf, odev->net);
					odev->skb_rx_buf->ip_summed = CHECKSUM_UNNECESSARY;	/* don't check it */

#ifdef DEBUG
					DUMP(ip_pkt, min(128, count));
#endif

					/* Ship it off to the kernel */
					netif_rx(odev->skb_rx_buf);
					odev->skb_rx_buf = NULL;	/* No longer our buffer. */

					/* update out statistics */
					odev->stats.rx_packets++;

					/* Hmmm, wonder if we have received the IP len or the ETH len. */
					odev->stats.rx_bytes += odev->rx_buf_size;
				}
				odev->rx_buf_size = 0;
				odev->rx_buf_missing = sizeof(struct iphdr);
				odev->rx_parse_state = WAIT_IP;
			}
			break;

		case WAIT_SYNC:
			D(" W_S");
			count = 0;
			break;
		default:
			D(" ");
			count--;
			break;
		}
	}
}

/* Moving data from usb to kernel (in interrupt state) */

#if ( LINUX_VERSION_CODE < KERNEL_VERSION(2,6,20) )
static void read_bulk_callback(struct urb *urb, struct pt_regs *regs)
#else
static void read_bulk_callback(struct urb *urb)
#endif
{
	struct hso_priv *odev = urb->context;
	struct net_device *net;
	int res;
	unsigned long flags;
	int status = urb->status; /* preparation for removal of
				     status from struct urb */

	if (status) {
		if(status != -ESHUTDOWN) {
			D1("nonzero bulk status received: %d", status);
		}
		return;
	}

	/* Sanity check */
	if (!odev || !test_bit(HSO_NET_RUNNING, &odev->flags)) {
		D1("BULK IN callback but driver is not active!");
		return;
	}

	net = odev->net;
	if (!netif_device_present(net)) {
		/* Somebody killed our network interface... */
		return;
	}

	if (urb->actual_length) {
		/* Handle the IP stream, add header and push it
		   onto network stack if the packet is complete. */
		spin_lock_irqsave(&odev->net_lock,flags);
		packetizeRx(odev, urb->transfer_buffer, urb->actual_length);
		spin_unlock_irqrestore(&odev->net_lock,flags);
	}

	/* We are done with this URB, resubmit it.
	   Prep the USB to wait for another frame. Reuse same as received.
	 */
	usb_fill_bulk_urb(
			urb,
			odev->usb,
			usb_rcvbulkpipe(odev->usb, odev->mux_ep_bulk_in),
			urb->transfer_buffer,
			MUX_BULK_RX_BUF_SIZE,
			read_bulk_callback,
			odev);

	/* Give this to the USB subsystem so it can tell us
	   when more data arrives.
	 */
	if ((res = usb_submit_urb(urb, GFP_ATOMIC))) {
		WARN("%s failed submit mux_bulk_rx_urb %d", __FUNCTION__, res);
	}
}

/* This will tell kernel to send us packets again */
#if ( LINUX_VERSION_CODE < KERNEL_VERSION(2,6,20) )
static void write_bulk_callback(struct urb *urb, struct pt_regs *regs)
#else
static void write_bulk_callback(struct urb *urb)
#endif
{
	struct hso_priv *odev = urb->context;
	int status = urb->status;

	D1("-----------------------------------");

	/* Sanity check */
	if (!odev || !test_bit(HSO_NET_RUNNING, &odev->flags)) {
		ERR("write_bulk_callback: device not running");
		return;
	}

	/* Do we still have a valid kernel network device? */
	if (!netif_device_present(odev->net)) {
		ERR("write_bulk_callback: net device not present");
		return;
	}

	if (status) {
		D1("%s: TX status %d", odev->net->name, status);
	}

	/* Tell the network interface we are
	   ready for another frame
	 */
	netif_wake_queue(odev->net);
}

#if ( LINUX_VERSION_CODE < KERNEL_VERSION(2,6,20) )
static void ctrl_callback(struct urb *urb, struct pt_regs *regs)
#else
static void ctrl_callback(struct urb *urb)
#endif
{
	struct hso_serial *serial = urb->context;
	struct usb_ctrlrequest *req = NULL;
	int status = urb->status;

	if (!serial) {
		return;
	}

	req = (struct usb_ctrlrequest *)(urb->setup_packet);

	D4("\n-------------------- got ctrl callback %02x ---------------------", status);
#if 0
	/* This is old values */
	if (req) {
		D4("bRequestType: 0x%02x", req->bRequestType);
		D4("bRequest:     0x%02x", req->bRequest);
		D4("wValue:       0x%04x", req->wValue);
		D4("wIndex:       0x%04x", req->wIndex);
		D4("wLength:      0x%04x (%d)", req->wLength, req->wLength);
	}
#endif

	if (status) {
		if(status != -ESHUTDOWN) {
			D1("nonzero ctrl status received: %d", status);
		}
		return;
	}

	D4("actual length = %d\n", urb->actual_length);

#ifdef DEBUG
	DUMP1(urb->transfer_buffer, urb->actual_length);
#endif

	if (req->bRequestType ==
	    (USB_DIR_IN | USB_TYPE_OPTION_VENDOR | USB_RECIP_INTERFACE)) {
		if (serial->open_count > 0)
			put_rxbuf_data(urb, serial);

                D1("urb->actual_length == %d", urb->actual_length);

		/* Re issue a read as long as we receive data. */
		if (urb->actual_length != 0) {
			hso_mux_serial_read(serial->odev, serial);
		} else {
			clear_bit(HSO_SERIAL_FLAG_RX_SENT, &serial->flags);
			hso_mux_submit_intr_urb(serial->odev,GFP_ATOMIC);
		}

	} else {	/* write request. */
		if (urb->actual_length != 0) {
                        clear_bit(HSO_SERIAL_FLAG_TX_SENT, &serial->flags);
                }
	}
}

/* Diag/CS port only
   Interrupt state
 */
#if ( LINUX_VERSION_CODE < KERNEL_VERSION(2,6,20) )
static void hso_std_serial_read_bulk_callback(struct urb *urb, struct pt_regs *regs)
#else
static void hso_std_serial_read_bulk_callback(struct urb *urb)
#endif
{
	struct hso_serial *serial = urb->context;
	int res;
	int status = urb->status;
/*    struct usb_ctrlrequest *req = NULL; */

	if (!serial) {
		return;
	}

	D4("\n-------------------- got serial_read_bulk callback %02x ---------------------", status);

        if (status) {
		if(status != -ESHUTDOWN) {
			D1("nonzero read bulk status received: %d", status);
		}
                return;
        }

	D1("actual length = %d\n", urb->actual_length);
#ifdef DEBUG
	DUMP1(urb->transfer_buffer, urb->actual_length);
#endif

	if (serial->open_count == 0)	/* No one is listening, dont resubmit it.. */
		return;

	if (status == 0) {	/* Valid data */
		put_rxbuf_data(urb, serial);
	} else if (status == -ENOENT || status == -ECONNRESET) {
		/* Unlinked - check for throttled port. */
		D2(" port %d, successfully unlinked urb", serial->minor);
	} else {
		D2(" port %d, status = %d for read urb", serial->minor, status);
		return;
	}

	/* We are done with this URB, resubmit it.
	   Prep the USB to wait for another frame */
	usb_fill_bulk_urb(serial->rx_urb,
			  serial->odev->usb,
			  usb_rcvbulkpipe(serial->odev->usb,
					  serial->ep_bulk_in),
			  serial->rx_data,
			  serial->rx_data_length,
			  hso_std_serial_read_bulk_callback,
			  serial);

	/* Give this to the USB subsystem so it can tell us
	   when more data arrives.
	 */
	if ((res = usb_submit_urb(serial->rx_urb, GFP_ATOMIC))) {
		ERR("%s failed submit serial rx_urb %d", __FUNCTION__, res);
	}
}

/* Diag/CS port only
 */
#if ( LINUX_VERSION_CODE < KERNEL_VERSION(2,6,20) )
static void hso_std_serial_write_bulk_callback(struct urb *urb, struct pt_regs *regs)
#else
static void hso_std_serial_write_bulk_callback(struct urb *urb)
#endif
{
	struct hso_serial *serial = urb->context;
	int status = urb->status;

	if (!serial)
		return;

	D1(" ");

	clear_bit(HSO_SERIAL_FLAG_TX_SENT, &serial->flags);

	if (status) {
		if(status != -ESHUTDOWN) {
			D1("nonzero write bulk status received: %d", status);
		}
		return;
	}

	return;
}

static int mux_device_request(struct hso_serial *serial, u8 type, u16 port,
			  struct urb *ctrl_urb,
			  struct usb_ctrlrequest *ctrl_req, u8 *ctrl_urb_data,
			  u32 size)
{
	int res = 0;
	int pipe = -1;

	/* Sanity check */
	if (!serial || !ctrl_urb || !ctrl_req) {
		ERR("Wrong arguments ");
		return -EINVAL;
	}

	ctrl_req->wValue = 0;
	ctrl_req->wIndex = port;	/* port to send to */
	ctrl_req->wLength = size;	/* Is this supposed to be this??? */

	if (type == GET_ENCAPSULATED_RESPONSE) { /* Reading command */
		ctrl_req->bRequestType =
		    USB_DIR_IN | USB_TYPE_OPTION_VENDOR | USB_RECIP_INTERFACE;
		ctrl_req->bRequest = GET_ENCAPSULATED_RESPONSE;
		pipe = usb_rcvctrlpipe(serial->odev->usb, 0);
	} else {				/* Writing command */
		ctrl_req->bRequestType =
		    USB_DIR_OUT | USB_TYPE_OPTION_VENDOR | USB_RECIP_INTERFACE;
		ctrl_req->bRequest = SEND_ENCAPSULATED_COMMAND;
		pipe = usb_sndctrlpipe(serial->odev->usb, 0);

#ifdef DEBUG
		DUMP(ctrl_urb_data, size);
#endif
	}

	D2("%s command (%02x) len: %d, port: %d",
	   type == GET_ENCAPSULATED_RESPONSE ? "Read" : "Write",
	   ctrl_req->bRequestType, ctrl_req->wLength, port);

	/* Load ctrl urb */
	ctrl_urb->transfer_flags = 0;

	usb_fill_control_urb(ctrl_urb,
			serial->odev->usb,
			pipe,
			(u8 *) ctrl_req,
			ctrl_urb_data,
			size,
			ctrl_callback,
			serial);

	/* Send it on merry way */
	if ((res = usb_submit_urb(ctrl_urb, GFP_ATOMIC))) {
		ERR("%s failed submit ctrl_urb %d type %d", __FUNCTION__, res, type);
		return res;
	}

	return size;
}

static int hso_mux_serial_read(struct hso_priv *odev, struct hso_serial *serial)
{
	int rc = 0;

	memset(serial->rx_data, 0, CTRL_URB_RX_SIZE);	/* rx_data_length */

	rc = mux_device_request(serial,
			    GET_ENCAPSULATED_RESPONSE,
			    serial->mux,
			    serial->rx_urb,
			    &serial->ctrl_req_rx,
			    serial->rx_data, serial->rx_data_length);
	return rc;
}

/*
   We we have a pending response, here we know what port to send the reponse to
 */
#if ( LINUX_VERSION_CODE < KERNEL_VERSION(2,6,20) )
static void intr_callback(struct urb *urb, struct pt_regs *regs)
#else
static void intr_callback(struct urb *urb)
#endif
{
	struct hso_priv *odev = urb->context;
	struct hso_serial *serial;
	unsigned char *port_req;
	int status = urb->status;
	int i;

	if (!odev) {
		return;
	}

	D4("\n---------------------- got intr callback %02x ---------------------", status);

	if (status) {
		if(status != -ESHUTDOWN) {
			D1("%s intr status %d", odev->net->name, status);
		}
		return;
	}

	port_req = urb->transfer_buffer;

	D4(" port_req = 0x%.2X\n", *port_req);

	for (i = 0; i < 8; i++) { /* max 8 channels on MUX */
		serial = get_serial_by_odev_and_mux(odev,i);
		if ((*port_req & (1 << i)) && serial != NULL) {
			D1("Pending read interrupt on port %d\n", i);
			if (!test_and_set_bit(HSO_SERIAL_FLAG_RX_SENT,
						&serial->flags)) {
				/* Setup and send a ctrl req read on port i */
				hso_mux_serial_read(odev,serial);
			} else {
				D1("Already pending a read on port %d\n", i);
			}
		}
	}

	/*	D1("issue ctrl read "); */
	/*	debug_read(odev, serial_table[0]); */
}

static inline int enable_net_traffic(struct hso_priv *odev)
{
	D1("Does not do anything yet.");
	return 0;
}

static inline void disable_net_traffic(struct hso_priv *odev)
{
	D1("Does not do anything yet");
}

/*
   Callback routines for kernel Ethernet Device
 */

static void hso_net_tx_timeout(struct net_device *net)
{
	struct hso_priv *odev = netdev_priv(net);

	D1("-----------------------------------");

	if (!odev) {
		return;
	}

	/* Tell syslog we are hosed. */
	WARN("%s: Tx timed out.", net->name);

	/* Tear the waiting frame off the list */
	usb_unlink_urb(odev->mux_bulk_tx_urb);

	/* Update statistics */
	odev->stats.tx_errors++;
}

/* Moving data from kernel to usb
 */
static int hso_net_start_xmit(struct sk_buff *skb, struct net_device *net)
{
	struct hso_priv *odev = netdev_priv(net);
	int res;

	/* Tell the kernel, "No more frames 'til we are done
	   with this one.'
	 */
	netif_stop_queue(net);

	skb_pull(skb, ETH_HLEN);

#ifdef DEBUG
	DUMP1(skb->data, skb->len);
#endif

	/* Copy it from kernel memory to OUR memory */
	memcpy(odev->mux_bulk_tx_buf, skb->data, skb->len);

	/* Fill in the URB for shipping it out. */
	usb_fill_bulk_urb(odev->mux_bulk_tx_urb, odev->usb,
			  usb_sndbulkpipe(odev->usb, odev->mux_ep_bulk_out),
			  odev->mux_bulk_tx_buf,
			  skb->len,
			  write_bulk_callback, odev);

	D1("len: %d/%d", skb->len, MUX_BULK_TX_BUF_SIZE);

	/* Deal with the Zero Length packet problem, I hope */
	odev->mux_bulk_tx_urb->transfer_flags |= URB_ZERO_PACKET;

	/* Send the URB on its merry way. */
	if ((res = usb_submit_urb(odev->mux_bulk_tx_urb, GFP_ATOMIC))) {
		WARN("failed mux_bulk_tx_urb %d", res);
		odev->stats.tx_errors++;
		netif_start_queue(net);
	} else {
		odev->stats.tx_packets++;
		odev->stats.tx_bytes += skb->len;
		net->trans_start = jiffies; /* And tell the kernel when
					       the last transmit started. */
	}

	dev_kfree_skb(skb);

	return res;
}

static struct net_device_stats *hso_net_get_stats(struct net_device *net)
{
	return &((struct hso_priv *) netdev_priv(net))->stats;
}

/* AJB, called when net I/F is brought up by ifconfig */
static int hso_net_open(struct net_device *net)
{
	struct hso_priv *odev = netdev_priv(net);
	int res;
	int i;
	unsigned long flags;

	/* Turn on the USB and let the packets flow!!! */
	if ((res = enable_net_traffic(odev))) {
		ERR("%s can't enable_net_traffic() - %d", __FUNCTION__, res);
		return -EIO;
	}
	/* STTH test code. reset counter. */
	odev->stats.rx_packets = 0;

	spin_lock_irqsave(&odev->net_lock,flags);
	odev->rx_parse_state = WAIT_IP;
	odev->rx_buf_size = 0;
	odev->rx_buf_missing = sizeof(struct iphdr);
	spin_unlock_irqrestore(&odev->net_lock,flags);

	for (i = 0; i < MUX_BULK_RX_BUF_COUNT; i++) {
		/* Prep a receive URB */
		usb_fill_bulk_urb(odev->mux_bulk_rx_urb_pool[i],
				  odev->usb,
				  usb_rcvbulkpipe(odev->usb,
						  odev->mux_ep_bulk_in),
				  odev->mux_bulk_rx_buf_pool[i],
				  MUX_BULK_RX_BUF_SIZE,
				  read_bulk_callback, odev);

		/* Put it out there so the device can send us stuff */
		if ((res = usb_submit_urb(odev->mux_bulk_rx_urb_pool[i],
						GFP_KERNEL))) {
			WARN("%s failed mux_bulk_rx_urb %d", __FUNCTION__, res);
		}
	}
	/* Tell the kernel we are ready to start receiving from it */
	netif_start_queue(net);

	/* We are up and running. */
	set_bit(HSO_NET_RUNNING, &odev->flags);

	return 0;
}

/* AJB, called when net I/F is brought down by ifconfig */
static int hso_net_close(struct net_device *net)
{
	struct hso_priv *odev = netdev_priv(net);
	int i;

	clear_bit(HSO_NET_RUNNING, &odev->flags);

	netif_stop_queue(net);

	/* If we are not already unplugged, turn off USB traffic */
	if (!test_bit(HSO_CARD_UNPLUG, &odev->flags)) {
		disable_net_traffic(odev);
	}

	/* We don't need the URBs anymore. */
	for (i = 0; i < MUX_BULK_RX_BUF_COUNT; i++) {
		usb_unlink_urb(odev->mux_bulk_rx_urb_pool[i]);
	}
	usb_unlink_urb(odev->mux_bulk_tx_urb);

	/* interrupt urb is still needed for the serial to work.... */

	return 0;
}

static int netdev_ethtool_ioctl(struct net_device *net, struct ifreq *rq)
{
	struct hso_priv *odev = netdev_priv(net);
	u32 cmd;
	char tmp[40];

	if (copy_from_user(&cmd, rq->ifr_data,sizeof(cmd)))
		return -EFAULT;

	switch (cmd) {
		/* get driver info */
	case ETHTOOL_GDRVINFO: {
			struct ethtool_drvinfo info = { ETHTOOL_GDRVINFO };
			strncpy(info.driver, driver_name, ETHTOOL_BUSINFO_LEN);
			strncpy(info.version, DRIVER_VERSION,
				ETHTOOL_BUSINFO_LEN);
			sprintf(tmp, "usb%d:%d", odev->usb->bus->busnum,
				odev->usb->devnum);
			strncpy(info.bus_info, tmp, ETHTOOL_BUSINFO_LEN);
			sprintf(tmp, "%s %x.%x", driver_name,
				((odev->bcdCDC & 0xff00) >> 8),
				(odev->bcdCDC & 0x00ff));
			strncpy(info.fw_version, tmp, ETHTOOL_BUSINFO_LEN);

			if (copy_to_user(rq->ifr_data, &info, sizeof(info)))
				return -EFAULT;

			return 0;
		}
		/* get link status */
	case ETHTOOL_GLINK: {
			struct ethtool_value edata = { ETHTOOL_GLINK };
			edata.data = netif_carrier_ok(net);

			if (copy_to_user(rq->ifr_data, &edata, sizeof(edata)))
				return -EFAULT;

			return 0;
		}
	}
	D1("Got unsupported ioctl: %x", cmd);
	return -EOPNOTSUPP;	/* the ethtool user space tool relies on this */
}

static int hso_net_ioctl(struct net_device *net, struct ifreq *rq, int cmd)
{
	struct hso_priv *odev = netdev_priv(net);

	switch (cmd) {

	case SIOCDEVPRIVATE + 1:/* Chose this one because SIOCDEVPRIVATE used somewhere else in this code */
		D5("Transmitted: %lu", odev->stats.tx_bytes);
		rq->ifr_ifru.ifru_ivalue = odev->stats.tx_bytes;
		return 0;

	case SIOCETHTOOL:
		return netdev_ethtool_ioctl(net, rq);

	default:
		return -ENOTTY;	/* per ioctl man page */
	}
}

/* FIXME AJB, work out what we need to do with multicast */
static void hso_net_set_multicast(struct net_device *net)
{
	struct hso_priv *odev = netdev_priv(net);
	int i;
	__u8 *buf;

	/* Tell the kernel to stop sending us frames while we get this all set up. */
	netif_stop_queue(net);

	/* Note: do not reorder, GCC is clever about common statements. */
	if (net->flags & IFF_PROMISC) {
		/* Unconditionally log net taps. */
		D1("%s: Promiscuous mode enabled", net->name);
		odev->mode_flags =
		    MODE_FLAG_ALL_MULTICAST | MODE_FLAG_DIRECTED |
		    MODE_FLAG_BROADCAST | MODE_FLAG_MULTICAST |
		    MODE_FLAG_PROMISCUOUS;
	} else if (net->mc_count > odev->wNumberMCFilters) {
		/* Too many to filter perfectly -- accept all multicasts. */
		D1("%s: too many MC filters for hardware, using allmulti",
		   net->name);
		odev->mode_flags =
		    MODE_FLAG_ALL_MULTICAST | MODE_FLAG_DIRECTED |
		    MODE_FLAG_BROADCAST | MODE_FLAG_MULTICAST;
	} else if (net->flags & IFF_ALLMULTI) {
		/* Filter in software */
		D1("%s: using allmulti", net->name);
		odev->mode_flags =
		    MODE_FLAG_ALL_MULTICAST | MODE_FLAG_DIRECTED |
		    MODE_FLAG_BROADCAST | MODE_FLAG_MULTICAST;
	} else {
		/* do multicast filtering in hardware */
		struct dev_mc_list *mclist;
		D1("%s: set multicast filters", net->name);
		odev->mode_flags =
		    MODE_FLAG_ALL_MULTICAST | MODE_FLAG_DIRECTED |
		    MODE_FLAG_BROADCAST | MODE_FLAG_MULTICAST;
		if (!(buf = kmalloc(6 * net->mc_count, GFP_ATOMIC))) {
			ERR("No memory to allocate?");
			goto exit;
		}
		for (i = 0, mclist = net->mc_list;
		     mclist && i < net->mc_count; i++, mclist = mclist->next) {
			memcpy(&mclist->dmi_addr, &buf[i * 6], 6);
		}
#if 0
		usb_control_msg(odev->usb, usb_sndctrlpipe(odev->usb, 0), SET_ETHERNET_MULTICAST_FILTER,	/* request */
				USB_TYPE_CLASS | USB_DIR_OUT | USB_RECIP_INTERFACE,	/* request type */
				cpu_to_le16(net->mc_count),	/* value */
				cpu_to_le16((u16) odev->mux_interface),	/* index */
				buf, (6 * net->mc_count),	/* size */
				HZ);	/* timeout */
#endif
		kfree(buf);
	}

      exit:
	/* Tell the kernel to start giving frames to us again. */
	netif_wake_queue(net);
}

static int parse_option_information(unsigned char *data, int length,
				    struct hso_priv *odev)
{
	int i = 0;

	if (length != 3) {
		ERR("Extralen don't contain expected data! length:%d", length);
		return -1;
	}

	/* Length */
	odev->option_info.length = data[i++];

	/* Type */
	odev->option_info.descriptor_type = data[i++];

	if (odev->option_info.descriptor_type != CS_INTERFACE_VAL) {
		ERR("Expected CS_INTERFACE_VAL, got: %02x",
		    odev->option_info.descriptor_type);
		return -1;
	}

	/* Enabled modem ports */
	odev->option_info.enabled_ports = data[i++];

	D1("length: %02x, type: %02x, subtype: %02x",
			odev->option_info.length,
			odev->option_info.descriptor_type,
			odev->option_info.enabled_ports);

	return 0;
}

static int get_mux_endpoints(struct usb_interface *intf, struct hso_priv *odev)
{
	int i;
	struct usb_endpoint_descriptor *endp = NULL;
	struct usb_host_interface *iface_desc = intf->cur_altsetting;

	/* Start out assuming we won't find anything we can use */
	odev->mux_ep_bulk_out = odev->mux_ep_bulk_in = odev->mux_ep_intr = 0;

	for (i = 0; i < iface_desc->desc.bNumEndpoints; i++) {
		endp = &iface_desc->endpoint[i].desc;

		if (!usb_endpoint_xfer_bulk(endp)) {
			if (!iface_desc->endpoint[i].extralen ||
					parse_option_information(
						iface_desc->endpoint[i].extra,
						iface_desc->endpoint[i].extralen,
						odev)) {
				ERR("Could not get option specific data, not our device then");
				return -1;
			}

			odev->mux_ep_intr =
				endp->bEndpointAddress & 0x7F;
			odev->mux_bInterval = endp->bInterval;
			odev->mux_ep_intr_size = endp->wMaxPacketSize;
		} else {
			if (usb_endpoint_is_bulk_in(endp)) {
				odev->mux_ep_bulk_in =
					endp->bEndpointAddress & 0x7F;
				odev->mux_ep_bulk_in_size =
					endp->wMaxPacketSize;
			} else {
				odev->mux_ep_bulk_out = endp->bEndpointAddress;
				odev->mux_ep_bulk_out_size =
					endp->wMaxPacketSize;
			}
		}
	}

	/* Now make sure we got both an IN and an OUT */
	if (odev->mux_ep_bulk_in && odev->mux_ep_bulk_out && odev->mux_ep_intr) {
		D1("detected BULK IN/OUT/interrupt packets of [size: %d/%d/%d, addr: %02x/%02x/%02x]",
			odev->mux_ep_bulk_in_size,
			odev->mux_ep_bulk_out_size,
			odev->mux_ep_intr_size,
			odev->mux_ep_bulk_in,
			odev->mux_ep_bulk_out,
			odev->mux_ep_intr);
		return 0;
	}
	return -1;
}

static int get_std_serial_endpoints(struct usb_interface *intf, struct hso_serial *serial)
{
	int i;
	struct usb_endpoint_descriptor *endp = NULL;
	struct usb_host_interface *iface_desc = intf->cur_altsetting;

	/* Start out assuming we won't find anything we can use */
	serial->ep_bulk_out = serial->ep_bulk_in = 0;

	for (i = 0; i < iface_desc->desc.bNumEndpoints; i++) {
		endp = &iface_desc->endpoint[i].desc;

		if (!usb_endpoint_xfer_bulk(endp)) {
			continue;
		}

		/* Check the first endpoint to see if it is IN or OUT */
		if (usb_endpoint_is_bulk_in(endp)) {
			serial->ep_bulk_in = endp->bEndpointAddress & 0x7F;
			serial->ep_bulk_in_size = endp->wMaxPacketSize;
		} else {
			serial->ep_bulk_out = endp->bEndpointAddress;
			serial->ep_bulk_out_size = endp->wMaxPacketSize;
		}
	}

	/* FIXME AJB, need to handle the interrupt endpoint also
	   for Circuit Switched port */


	/* Now make sure we got both an IN and an OUT */
	if (serial->ep_bulk_in && serial->ep_bulk_out) {
		D1("detected BULK IN/OUT packets of [size: %d/%d, addr: %02x/%02x]",
			serial->ep_bulk_in_size,
			serial->ep_bulk_out_size,
			serial->ep_bulk_in,
			serial->ep_bulk_out);
		return 0;
	}
	return -1;
}


#define UMS_SWITCH_CMD_LEN 31


static int check_ums_and_send_rezero(struct usb_interface *intf, struct usb_device *usb)
{
	struct usb_endpoint_descriptor *endp = NULL;
	struct usb_host_interface *iface_desc = intf->cur_altsetting;
	int retval = 0;
	int actual_length;


	u8  UmsSwitchCmd[] =
	{
	    0x55, 0x53, 0x42, 0x43, 0x78, 0x56, 0x34, 0x12,
	    0x01, 0x00, 0x00, 0x00, 0x80, 0x00, 0x06, 0x01,
	    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
	};


	/* Check UMS interface */
	if (iface_desc->desc.bNumEndpoints == 2 &&
	    iface_desc->desc.bInterfaceClass == 0x08 &&
	    iface_desc->desc.bInterfaceSubClass == 0x06 &&
            iface_desc->desc.bInterfaceProtocol == 0x50) {
		D1("UMS interface found");

		if (usb_endpoint_is_bulk_out(&iface_desc->endpoint[0].desc)) {
			endp = &iface_desc->endpoint[0].desc;
		} else {
			endp = &iface_desc->endpoint[1].desc;
		}	

		if ( 0 != usb_bulk_msg (usb, 
					usb_sndbulkpipe(usb, endp->bEndpointAddress),
					(void*)UmsSwitchCmd, 
					UMS_SWITCH_CMD_LEN,
					&actual_length,
					0)) {
			WARN("send UMS switch failed");
		} else {
			D1("UMS switch done");	
			retval = 1;
		}
	}
	
	return retval;
}


static int check_and_claim_interfaces(struct usb_device *usb,
		struct hso_priv *odev)
{
	struct usb_host_config *conf = usb->actconfig;
	int i;
	int result = 0;

	/*
	   0. Multiplexed interface - hsdpa net + muxed serial ports.
	   1. QXDM port - only used for debug
	   2. Modem port - only used for Circuit switched connections.
	   */

	/* AJB, only check 1 and 2, since 0 is claimed for us prior to probe */
	for (i = 1; i < conf->desc.bNumInterfaces; i++) {
                if (usb_interface_claimed(usb_ifnum_to_if(usb,i))) {
                        D1("Interface %d already claimed", i);
                        result = -1;
                }
        }
        if (result)
		return result;

	D1("No one has claimed our auxiliary interfaces, good");

	/* Check if we found an Option device */
	if (get_mux_endpoints(usb_ifnum_to_if(usb,0), odev)) {
		D1("Mux interface endpoints did not suit us");
		goto exit;
	}

	/* MUX interface is claimed for us already */

	/* Claim QXDM interface */
	usb_driver_claim_interface(&hso_driver,usb_ifnum_to_if(usb,1),odev);

	/* Claim C/S interface */
	if(conf->desc.bNumInterfaces > 2) {
		usb_driver_claim_interface(&hso_driver,usb_ifnum_to_if(usb,2),odev);
	}

	return conf->desc.bNumInterfaces;

exit:
	return -EIO;
}

static inline unsigned char hex2dec(unsigned char digit)
{

	if ((digit >= '0') && (digit <= '9')) {
		return (digit - '0');
	}
	/* Map all characters to 0-15 */
	if ((digit >= 'a') && (digit <= 'z')) {
		return (digit - 'a' + 10) % 16;
	}
	if ((digit >= 'A') && (digit <= 'Z')) {
		return (digit - 'A' + 10) % 16;
	}

	return 16;
}

/*
   Use the serial number to generate the MAC address
   FIXME AJB, unfortunately the devices I have, have this set to 'Serial Number'
   and so generate the same MAC address :-(
 */
static void set_ethernet_addr(struct hso_priv *odev)
{
	unsigned char mac_addr[6];
	int i;
	int len;
	unsigned char buffer[13];

	/* Let's assume we don't get anything */
	mac_addr[0] = 0x00;
	mac_addr[1] = 0x00;
	mac_addr[2] = 0x00;
	mac_addr[3] = 0x00;
	mac_addr[4] = 0x00;
	mac_addr[5] = 0x00;

	if (0 > (len = usb_string(odev->usb,
					odev->usb->descriptor.iSerialNumber, buffer, 13))) {
		ERR("Attempting to get MAC address failed: %d", -1 * len);
		return;
	}

	/* Sanity check */
	if (len != 12) {
		/* You gotta love failing sanity checks */
		ERR("Attempting to get MAC address returned %d bytes", len);
		return;
	}

	/* Fill in the mac_addr */
	for (i = 0; i < 6; i++) {
		if ((16 == buffer[2 * i]) || (16 == buffer[2 * i + 1])) {
			ERR("Bad value in MAC address i:%d", i);
		} else {
			mac_addr[i] =
			    (hex2dec(buffer[2 * i]) << 4) +
			    hex2dec(buffer[2 * i + 1]);
		}
	}

	mac_addr[0] = 0x00;
	mac_addr[1] = 0x03;

	/* Now copy it over to our network device structure */
	memcpy(odev->net->dev_addr, mac_addr, sizeof(mac_addr));

	/* Create the default fake ethernet header. */
	memcpy(odev->dummy_eth_head.h_dest, mac_addr, ETH_ALEN);
	memcpy(odev->dummy_eth_head.h_source, dummy_mac, ETH_ALEN);
	odev->dummy_eth_head.h_proto = htons(ETH_P_IP);
}

static void get_string(u8 * buf, int buf_len, int string_num, struct hso_priv *odev)
{
	int len;

	if (!buf) {
		ERR("No buffer?");
		return;
	}

	buf[0] = 0x00;

	if (string_num) {
		/* Put it into its buffer */
		len = usb_string(odev->usb, string_num, buf, buf_len);
		/* Just to be safe */
		buf[len] = 0x00;
	}
}

static void log_device_info(struct hso_priv *odev)
{
	unsigned char manu[256];
	unsigned char prod[256];
	unsigned char sern[256];
	unsigned char *mac_addr = odev->net->dev_addr;

	/* Try to get the device Manufacturer */
	get_string(manu, sizeof(manu), odev->usb->descriptor.iManufacturer, odev);

	/* Try to get the device Product Name */
	get_string(prod, sizeof(prod), odev->usb->descriptor.iProduct, odev);

	/* Try to get the device Serial Number */
	get_string(sern, sizeof(sern), odev->usb->descriptor.iSerialNumber, odev);

	/* Now send everything we found to the syslog */
	QFO("%s: %s %s %s", odev->net->name, manu, prod, sern);
	QFO("%s: %02X:%02X:%02X:%02X:%02X:%02X",
			odev->net->name,
			mac_addr[0], mac_addr[1], mac_addr[2],
			mac_addr[3], mac_addr[4], mac_addr[5]);
}

static void hso_free_memory(struct hso_priv *odev)
{
	int i;

	if (!odev)
		return;

	if (odev->mux_intr_urb) {
		usb_free_urb(odev->mux_intr_urb);
	}
	if (odev->mux_intr_buf) {
		kfree(odev->mux_intr_buf);
	}

	for (i = 0; i < MUX_BULK_RX_BUF_COUNT; i++) {
		if (odev->mux_bulk_rx_urb_pool[i]) {
			usb_free_urb(odev->mux_bulk_rx_urb_pool[i]);
		}
		if (odev->mux_bulk_rx_buf_pool[i]) {
			kfree(odev->mux_bulk_rx_buf_pool[i]);
		}
	}
	if (odev->mux_bulk_tx_urb) {
		usb_free_urb(odev->mux_bulk_tx_urb);
	}
	if (odev->mux_bulk_tx_buf) {
		kfree(odev->mux_bulk_tx_buf);
	}
}

static int hso_alloc_memory(struct hso_priv *odev)   /* FIXME AJB, need to make cleanup error path */
{
	int i;

	if (!odev)
		return -1;

	if (!(odev->mux_intr_urb = usb_alloc_urb(0, GFP_KERNEL))) {
		ERR("Could not allocate urb?");
		return -1;
	}
	if (!(odev->mux_intr_buf = kzalloc(MUX_INTR_BUF_SIZE, GFP_KERNEL))) {
		ERR("Could not allocate buf?");
		return -1;
	}

	for (i = 0; i < MUX_BULK_RX_BUF_COUNT; i++) {
		if (!(odev->mux_bulk_rx_urb_pool[i] = usb_alloc_urb(0, GFP_KERNEL))) {
			ERR("Could not allocate urb?");
			return -1;
		}
		if (!(odev->mux_bulk_rx_buf_pool[i] = kzalloc(MUX_BULK_RX_BUF_SIZE, GFP_KERNEL))) {
			ERR("Could not allocate buf?");
			return -1;
		}
	}

	if (!(odev->mux_bulk_tx_urb = usb_alloc_urb(0, GFP_KERNEL))) {
		ERR("Could not allocate urb?");
		return -1;
	}
	if (!(odev->mux_bulk_tx_buf = kzalloc(MUX_BULK_TX_BUF_SIZE, GFP_KERNEL))) {
		ERR("Could not allocate buf?");
		return -1;
	}

	return 0;
}

/* Forward declaration */

static int  hso_serial_init(void);
static void hso_serial_exit(void);

static void hso_net_init(struct net_device *net)
{
	struct hso_priv *odev = netdev_priv(net);

	memset(odev,0,sizeof(*odev));

	D("sizeof hso_priv is %d",sizeof(*odev));

	ether_setup(net);

	SET_MODULE_OWNER(net);
	net->open		= hso_net_open;
	net->stop		= hso_net_close;
/* set config */
	net->hard_start_xmit	= hso_net_start_xmit;
	net->do_ioctl		= hso_net_ioctl;
	net->get_stats		= hso_net_get_stats;
/* rebuild header */
	net->tx_timeout		= hso_net_tx_timeout;
	net->watchdog_timeo	= HSO_NET_TX_TIMEOUT;

	net->flags		|= IFF_NOARP;
/* features */
/* hard_header_cache */
	net->set_multicast_list	= hso_net_set_multicast;
	net->mtu		= DEFAULT_MTU - 14;
	net->tx_queue_len	= 10;

/* more ethernet defaults */
	odev->skb_rx_buf	= NULL;
	odev->rx_parse_state	= WAIT_IP;
	odev->wMaxSegmentSize	= DEFAULT_MTU;
	odev->wNumberMCFilters	= 0;		/* disable hardware filtering */

        spin_lock_init(&odev->net_lock);
}

#if ( LINUX_VERSION_CODE < KERNEL_VERSION(2,6,20) )
static int hso_mux_submit_intr_urb(struct hso_priv *odev,int gfp)
#else
static int hso_mux_submit_intr_urb(struct hso_priv *odev,gfp_t gfp)
#endif
{
	int res = 0;

	if (test_bit(HSO_CARD_UNPLUG, &odev->flags)) {
		WARN("%s closing so not rearming mux_intr_urb", __FUNCTION__);
		return 0;
	}

	D5("Arm and submit the interrupt URB");

	usb_fill_int_urb(odev->mux_intr_urb,
			odev->usb,
			usb_rcvintpipe(odev->usb, odev->mux_ep_intr),
			odev->mux_intr_buf,
			MUX_INTR_BUF_SIZE,
			intr_callback,
			odev, 
			(odev->usb->speed == USB_SPEED_HIGH) ? (1 << odev->mux_bInterval) : odev->mux_bInterval);

	if ((res = usb_submit_urb(odev->mux_intr_urb, gfp))) {
		WARN("%s failed mux_intr_urb %d", __FUNCTION__, res);
		goto exit;
	}
exit:
	return res;
}

/* AJB, called once for each unclaimed interface */
static int hso_probe(struct usb_interface *interface,
		     const struct usb_device_id *id)
{
	struct hso_priv *odev;
	int mux = 0;
	int i, res;
	struct net_device *net;
	struct usb_device *usb = interface_to_usbdev(interface);
	int minor, num_if;

	printk("++++++++++++++ being probed ++++++++++++++++=\n");
	D1("** Start probing **");

	D1("probe called with interface num %d",
	    interface->altsetting->desc.bInterfaceNumber);

	if (interface->altsetting->desc.bInterfaceNumber != 0) {
		return 0;
	}

	/* Check if we found an Option 3G UMS device */
	D1("Check interface num 0");
	if (check_ums_and_send_rezero(usb_ifnum_to_if(usb,0), usb)) {
		return 0;
	}


	/*
	 * If we get a kernel Ethernet interface, we can get our
	 * private data storage allocated using it
	 */
	net = alloc_netdev(sizeof(struct hso_priv), "hso%d", hso_net_init);
	if (!net) {
		ERR("Unable to create ethernet device");
		return -ENOMEM;
	}
	odev = netdev_priv(net);

	res = register_netdev(net);
	if (res) {
		ERR("Unable to register ethernet device");
		goto exit;
	}

	if ((num_if = check_and_claim_interfaces(usb, odev)) < 0 ) {
		D1("Could not find valid interface configuration");
		goto exit_unregister;
	}

	if ((res = hso_alloc_memory(odev)) ){
		goto exit_release_interfaces;
	}

	usb_set_intfdata(interface, odev); /* save our data pointer in this device */
	/* FIXME - should do usb_set_intfdata(interface, odev); for 1 also? */
	/* FIXME - should do usb_set_intfdata(interface, odev); for 2 also? */

	/* We'll keep track of this information for later... */
	odev->usb = usb;
	odev->net = net;

	/* and don't forget the MAC address. */
	set_ethernet_addr(odev);

	/* Send a message to syslog about what we are handling */
	log_device_info(odev);

	/* Muxed ports */
	for (i = 1,mux = 0; i < 0x100; i = i << 1,mux++) {
		if (odev->option_info.enabled_ports & i) {
			if((minor = get_free_serial_index()) < 0)
				break;
			hso_serial_start(odev, minor, MUX_INTERFACE, mux);
		}
	}

	/* Diagnostics port */
	if((minor = get_free_serial_index()) >= 0) {
		hso_serial_start(odev, minor, QXDM_INTERFACE, 1); /* interface num */
	}

	/* C/S port */
	if( ( num_if > 2 ) && ((minor = get_free_serial_index()) >= 0) ) {
		hso_serial_start(odev, minor, CS_INTERFACE, 2);   /* interface num */
	}

	usb_get_dev(usb);

	/* Arm and submit the interrupt URB */
	if(hso_mux_submit_intr_urb(odev,GFP_KERNEL) != 0) {
		goto exit_release_interfaces;
	}

	odev->ourproc = proc_mkdir(odev->net->name, hso_proc_dir_devices);
	create_proc_read_entry("ttys", 0, odev->ourproc, hso_proc_device_ttys, odev);

	D1("** probing done **");

	return 0;

exit_release_interfaces:
        /* FIXME AJB, release the usb interfaces */

exit_unregister:
	unregister_netdev(net);

exit:
	free_netdev(net);

	return -EIO;
}

/*
 * Module's disconnect routine
 * Called when the driver is unloaded or the device is unplugged
 * (Whichever happens first assuming the driver suceeded at its probe)
 */
static void hso_disconnect(struct usb_interface *interface)
{
	struct usb_device *usb = interface_to_usbdev(interface);
	struct hso_priv *odev;

	D1("called with interface num %d", interface->altsetting->desc.bInterfaceNumber);

	if (interface->altsetting->desc.bInterfaceNumber == 0) {

		odev = usb_get_intfdata(interface);
		if (!odev || !odev->usb || !odev->net) {
			WARN("odev, or odev->usb, or odev->net null");
			return;
		}

		/* let's cleanup our per device info */
		remove_proc_entry("ttys", odev->ourproc);

		remove_proc_entry(odev->net->name, hso_proc_dir_devices);

		usb_set_intfdata(interface,NULL);

		/* stop the interrupts */
		usb_kill_urb(odev->mux_intr_urb);

		/*
		   It is possible that this function is called before
		   the "close" function. This tells the close function
		   we are already disconnected
		 */
		set_bit(HSO_CARD_UNPLUG, &odev->flags);

		usb_put_dev(usb);

		hso_serial_disconnect(usb, odev);

		/* release the device memory under odev */
		hso_free_memory(odev);

		/* We don't need the network device any more */
		unregister_netdev(odev->net);

		/* Free network device including odev memeory */
		free_netdev(odev->net);

	}

	usb_driver_release_interface(&hso_driver, interface);
}

static struct usb_driver hso_driver = {
      .name =		driver_name,
      .probe =		hso_probe,
      .disconnect =	hso_disconnect,
      .id_table =	hso_ids,
};

int __init hso_init(void)
{
	int i,rc = 0;

	D1("%s", version);

#ifdef HSO_PROC
	hso_proc_dir = proc_mkdir(driver_name, proc_root_driver);
	hso_proc_dir_devices = proc_mkdir("devices", hso_proc_dir);
#endif
        /* Initialise our global data */
        spin_lock_init(&serial_table_lock);
	for (i = 0; i < HSO_SERIAL_TTY_MINORS; i++) {
		serial_table[i] = NULL;
	}

	hso_serial_init();

	if ((rc = usb_register(&hso_driver))) {
		ERR("Could not register hso driver? error: %d", rc);
		return rc;
	}

#ifdef HSO_PROC
	create_proc_read_entry("options", 0, hso_proc_dir, hso_proc_options,NULL);
#endif
	return 0;
}

/* usb_deregister calls disconnect of registered drivers. */
void __exit hso_exit(void)
{
	hso_serial_exit();

	usb_deregister(&hso_driver);

#ifdef HSO_PROC
	remove_proc_entry("options", hso_proc_dir);
	remove_proc_entry("devices", hso_proc_dir);
	remove_proc_entry(driver_name, proc_root_driver);
#endif
}

/*
   TTY Layer Type definitions
 */
static int hso_serial_open(struct tty_struct *tty, struct file *filp);
static void hso_serial_close(struct tty_struct *tty, struct file *filp);
static void hso_serial_hangup(struct tty_struct *tty);

static int hso_serial_write(struct tty_struct *tty, const unsigned char *buf,
			    int count);

static int hso_serial_write_room(struct tty_struct *tty);
static int hso_serial_chars_in_buffer(struct tty_struct *tty);
static void hso_serial_throttle(struct tty_struct *tty);
static void hso_serial_unthrottle(struct tty_struct *tty);
static int hso_serial_ioctl(struct tty_struct *tty, struct file *file,
			    unsigned int cmd, unsigned long arg);

#if ( LINUX_VERSION_CODE < KERNEL_VERSION(2,6,20) )
static void hso_serial_set_termios(struct tty_struct *tty, struct termios *old);
#else
static void hso_serial_set_termios(struct tty_struct *tty,
				   struct ktermios *old);
#endif

static void hso_serial_break(struct tty_struct *tty, int break_state);
static int hso_serial_read_proc(char *page, char **start, off_t off, int count,
				int *eof, void *data);

static int hso_serial_open(struct tty_struct *tty, struct file *filp)
{
	struct hso_serial *serial = get_serial_by_index(tty->index);
	struct hso_priv *odev;

	if (serial == NULL || serial->magic != HSO_SERIAL_MAGIC) {
		tty->driver_data = NULL;
		ERR("Failed to open port");
		return -ENODEV;
	}

	down(&serial->sem);

	D1("Opening %d", serial->minor);

	tty->driver_data = serial;

	serial->tty = tty;

	odev = serial->odev;

	serial->open_count++;
	if (serial->open_count == 1) {
		tty->low_latency = 1;

		serial->flags = 0;

		/* Force default termio settings */
		_hso_serial_set_termios(tty, NULL);

	} else {
		D1("Port was already open");
	}

	/* If it is not the MUX port fill in and submit a bulk urb.
	   (already allocated in hso_serial_start) */
	if (serial->type != MUX_INTERFACE) {
		int res;
		usb_fill_bulk_urb(serial->rx_urb, serial->odev->usb,
				  usb_rcvbulkpipe(serial->odev->usb,
						  serial->ep_bulk_in),
				  serial->rx_data, serial->rx_data_length,
				  hso_std_serial_read_bulk_callback, serial);

		if ((res = usb_submit_urb(serial->rx_urb, GFP_KERNEL)))
			D("Failed to submit urb - res %d", res);
	}

	up(&serial->sem);

	return 0;
}

static void __hso_serial_close(struct hso_serial *serial)
{
	D1(" ");

	if (!serial->open_count) {
		err("%s - port %d: not open", __FUNCTION__, serial->minor);
		return;
	}

	serial->open_count--;

	D1("serial->open_count == %d",serial->open_count);

	if (serial->open_count <= 0) {
		if (serial->type != MUX_INTERFACE) {
			D1("Unlink any pending bulk URBS");
			/* Stop pending urbs. Do not deallocate them,
			   it will be done in hso_serial_stop. */
			/* Is this in interrupt state? */
			usb_unlink_urb(serial->rx_urb);
			usb_unlink_urb(serial->tx_urb);
		}
		/*close ? */
		serial->open_count = 0;
		if (serial->tty) {
			serial->tty->driver_data = NULL;
			serial->tty = NULL;
		}
		return;
	}
}

static void hso_serial_close(struct tty_struct *tty, struct file *filp)
{
	struct hso_serial *serial = NULL;
	D1(" ");

	if (tty == NULL || tty->driver_data == NULL) {
		D1("(tty == NULL || tty->driver_data == NULL)");
		return;
	}

	serial = tty->driver_data;
	down(&serial->sem);
	if (tty->driver_data) {
		__hso_serial_close(serial);
	}
	up(&serial->sem);
}

static void hso_serial_hangup(struct tty_struct *tty)
{
	D1("hang up");
}

/* Called once per device shutdown */
/* usb unused */
static void hso_serial_disconnect(struct usb_device *usb, struct hso_priv *odev)
{
	struct hso_serial *serial;
	int i;

	for (i = 0; i < HSO_SERIAL_TTY_MINORS; i++) {
		serial = get_serial_by_index(i);
		if (serial != NULL && serial->odev == odev) {  /* only close the
								  ports belonging to odev */
			down(&serial->sem);
			while (serial->open_count > 0) {
				__hso_serial_close(serial);
			}
			hso_serial_stop(usb, i, odev);
			up(&serial->sem);
		}
		
	}
}

static int hso_serial_write(struct tty_struct *tty, const unsigned char *buf,
			    int count)
{
	struct hso_serial *serial = get_serial_by_tty(tty);
	int retval = -EINVAL;

	if (serial == NULL) {
		ERR("%s(%d) tty or tty->driver_data is NULL", __FUNCTION__,
		    __LINE__);
		return -ENODEV;
	}

	down(&serial->sem);

	while (test_bit(HSO_SERIAL_FLAG_TX_SENT, &serial->flags)) {
		/* wait for previous write to complete
		 * how else to ensure we are always able
		 * to write at least one byte for putchar()? */
		D2("HSO_SERIAL_FLAG_TX_SENT says already in progress");
		udelay(500);
	}

	set_bit(HSO_SERIAL_FLAG_TX_SENT, &serial->flags);

	D2(" hso_port = %d count %d", serial->minor, count);
	count = min((u16) count, serial->tx_data_length);

	memcpy(serial->tx_data, buf, count);

	serial->tx_data_count = count;

	if (serial->write_data) {
		retval = serial->write_data(serial);
	} else {
		clear_bit(HSO_SERIAL_FLAG_TX_SENT, &serial->flags);
		retval = -ENODEV;
	}

	up(&serial->sem);
	return retval;
}

static int hso_mux_serial_write_data(struct hso_serial *serial)
{
	return mux_device_request(serial,
			SEND_ENCAPSULATED_COMMAND,
			serial->mux,
			serial->tx_urb,
			&serial->ctrl_req_tx,
			serial->tx_data, serial->tx_data_count);
}

static int hso_std_serial_write_data(struct hso_serial *serial)
{
	int count = serial->tx_data_count;
	int res;

	usb_fill_bulk_urb(serial->tx_urb,
			serial->odev->usb,
			  usb_sndbulkpipe(serial->odev->usb,
					  serial->ep_bulk_out),
			serial->tx_data,
			serial->tx_data_count,
			hso_std_serial_write_bulk_callback,
			serial);

	if ((res = usb_submit_urb(serial->tx_urb, GFP_KERNEL)))
	{
		D("Failed to submit urb - res %d ", res);
		clear_bit(HSO_SERIAL_FLAG_TX_SENT, &serial->flags);
		return res;
	}

	return count;
}

static int hso_serial_write_room(struct tty_struct *tty)
{
	struct hso_serial *serial = get_serial_by_tty(tty);
	int room = 0;

	if (serial == NULL)
		return 0;

	down(&serial->sem);

	if(!test_bit(HSO_SERIAL_FLAG_TX_SENT, &serial->flags))
		room = serial->tx_data_length;
	
	up(&serial->sem);

	D4("room = %d ", room);

	return room;
}

static int hso_serial_chars_in_buffer(struct tty_struct *tty)
{
	struct hso_serial *serial = get_serial_by_tty(tty);
	int chars = 0;

	if (serial == NULL) {
		return 0;
	}

	down(&serial->sem);

	if(test_bit(HSO_SERIAL_FLAG_TX_SENT, &serial->flags))
		chars = serial->tx_urb->transfer_buffer_length;

	up(&serial->sem);

	D4("chars = %d ", chars);

	return chars;
}

static void put_rxbuf_data(struct urb *urb, struct hso_serial *serial)
{
        struct tty_struct *tty = serial->tty;

	if (urb == NULL || serial == NULL) {
		D1("serial = NULL");
		return;
	}

       /* Push data to tty */
        if (tty && urb->actual_length) {


	D("data to push to tty");

#if ( LINUX_VERSION_CODE < KERNEL_VERSION(2,6,16) )
	{
		int i;
		unsigned char *data = urb->transfer_buffer;

		for (i = 0; i < urb->actual_length ; ++i) {
			if (tty->flip.count >= TTY_FLIPBUF_SIZE)
				tty_flip_buffer_push(tty);
			tty_insert_flip_char(tty, data[i], 0);
		}
		tty_flip_buffer_push(tty);
	}
#else
                tty_buffer_request_room(tty, urb->actual_length);
                tty_insert_flip_string(tty, urb->transfer_buffer, urb->actual_length);
                tty_flip_buffer_push(tty);
#endif
        }

#if 0
	/* FIXME - AJB, maybe we need to throttle? */

	if (rx_fifo->cnt >= HSO_THRESHOLD_THROTTLE
	    && !test_and_set_bit(HSO_SERIAL_FLAG_THROTTLED, &serial->flags)) {
		D1("Throttle: rx_fifo->cnt[%d] >= HSO_THRESHOLD_THROTTLE[%d]",
		   rx_fifo->cnt, HSO_THRESHOLD_THROTTLE);
/*              hss_throttle(port); */
	}
#endif

}

static void hso_serial_throttle(struct tty_struct *tty)
{
	D1(" ");
}

static void hso_serial_unthrottle(struct tty_struct *tty)
{
	D1(" ");
}

static int hso_serial_ioctl(struct tty_struct *tty, struct file *file,
			    unsigned int cmd, unsigned long arg)
{
	D4(" ");
	return -ENOIOCTLCMD;
}

#if ( LINUX_VERSION_CODE < KERNEL_VERSION(2,6,20) )
static void _hso_serial_set_termios(struct tty_struct *tty,
				    struct termios *old)
#else
static void _hso_serial_set_termios(struct tty_struct *tty,
				    struct ktermios *old)
#endif
{
	struct hso_serial *serial = get_serial_by_tty(tty);

	if ((!tty) || (!tty->termios) || (!serial)) {
		ERR("no tty structures");
		return;
	}

	D4("port %d", serial->minor);

	/*
	 * The default requirements for this device are:
	 */
	serial->tty->termios->c_iflag &= ~(IGNBRK	/* disable ignore break */
					   | BRKINT	/* disable break causes interrupt */
					   | PARMRK	/* disable mark parity errors */
					   | ISTRIP	/* disable clear high bit of input characters */
					   | INLCR	/* disable translate NL to CR */
					   | IGNCR	/* disable ignore CR */
					   | ICRNL	/* disable translate CR to NL */
					   | IXON);	/* disable enable XON/XOFF flow control */

	serial->tty->termios->c_oflag &= ~OPOST;	/* disable postprocess output characters */

	serial->tty->termios->c_lflag &= ~(ECHO	/* disable echo input characters */
					   | ECHONL	/* disable echo new line */
					   | ICANON	/* disable erase, kill, werase, and rprnt special characters */
					   | ISIG	/* disable interrupt, quit, and suspend special characters */
					   | IEXTEN);	/* disable non-POSIX special characters */

	serial->tty->termios->c_cflag &= ~(CSIZE	/* no size */
					   | PARENB	/* disable parity bit */
					   | CBAUD);	/* clear current baud rate */

	serial->tty->termios->c_cflag |= (CS8	/* character size 8 bits */
					  | B115200);	/* baud rate 115200 */

	/*
	 * Force low_latency on; otherwise the pushes are scheduled;
	 * this is bad as it opens up the possibility of dropping bytes
	 * on the floor.  We don't want to drop bytes on the floor. :)
	 */
	serial->tty->low_latency = 1;

	/* Notify the tty driver that the termios have changed. */
	serial->tty->ldisc.set_termios(serial->tty, NULL);
	return;
}

/*
 * TODO - need to be down/up protected, but currently it is also used in serial_open
 * also locking it...
 */

#if ( LINUX_VERSION_CODE < KERNEL_VERSION(2,6,20) )
static void hso_serial_set_termios(struct tty_struct *tty, struct termios *old)
#else
static void hso_serial_set_termios(struct tty_struct *tty, struct ktermios *old)
#endif
{
	struct hso_serial *serial = get_serial_by_tty(tty);

	if ((!tty) || (!tty->termios) || (!serial)) {
		D1("no tty structures");
		return;
	}

	D1("port %d", serial->minor);

	down(&serial->sem);
	if (serial->open_count)
		_hso_serial_set_termios(tty, old);
	up(&serial->sem);

	return;
}

static void hso_serial_break(struct tty_struct *tty, int break_state)
{
	D1(" ");
}

static int hso_serial_read_proc(char *page, char **start, off_t off, int count,
				int *eof, void *data)
{
	return 0;
}

#if 0
static void port_softint(void *private)
{
	struct hso_serial *serial = (struct hso_serial *) private;
	struct tty_struct *tty;

	D1(" - port %d", serial->minor);

	if (!serial)
		return;

	tty = serial->tty;
	if (!tty)
		return;

	if ((tty->flags & (1 << TTY_DO_WRITE_WAKEUP))
	    && tty->ldisc.write_wakeup) {
		D1(" - write wakeup call.");
		(tty->ldisc.write_wakeup) (tty);
	}

	wake_up_interruptible(&tty->write_wait);
}
#endif

static struct hso_serial *hso_serial_start(struct hso_priv *odev, u8 minor, enum type_intf type, u8 num)
{
	struct hso_serial *serial = NULL;

	if (!(serial = kmalloc(sizeof(*serial), GFP_KERNEL))) {
		ERR("%s - Out of memory", __FUNCTION__);
		return NULL;
	}

	D1("tty_register_device %d", minor);
	tty_register_device(tty_drv, minor, NULL);

	serial->odev = odev;
	serial->minor = minor;
	serial->type = type;
	serial->mux = (type == MUX_INTERFACE) ? num : 0;
	serial->magic = HSO_SERIAL_MAGIC;

	serial->rx_urb = NULL;
	serial->rx_data = NULL;
	serial->rx_data_length = 0;

	switch(type) {
	case MUX_INTERFACE:
		switch(num) {
		case 0:
			QFO("Multiplexed Control channel present");
			break;
		case 1:
			QFO("Multiplexed Application channel present");
			break;
		case 2:
			QFO("Multiplexed PC/SC channel present");
			break;
		case 3:
			QFO("Multiplexed GPS channel present");
			break;
		case 4:
			QFO("Multiplexed Application 2 channel present");
			break;
		case 5:
		case 6:
		case 7:
			QFO("Multiplexed Reserved(%d) channel present",num);
			break;
		}
		break;
	case QXDM_INTERFACE:
		QFO("QXDM port present");
		break;
	case CS_INTERFACE:
		QFO("Circuit Switched port present");
		break;
	}

	if (type != MUX_INTERFACE) {
		if (get_std_serial_endpoints(usb_ifnum_to_if(odev->usb,num), serial)) {
			D1("Interface endpoints did not suit us");
			goto exit;
		}
	}

	if (!(serial->rx_urb = usb_alloc_urb(0, GFP_KERNEL))) {
		ERR("Could not allocate urb?");
		goto exit;
	}
	serial->rx_urb->transfer_buffer = NULL;
	serial->rx_urb->transfer_buffer_length = 0;

	serial->rx_data_length =
		(type == MUX_INTERFACE) ? CTRL_URB_RX_SIZE : BULK_URB_RX_SIZE;

	if (!(serial->rx_data = kzalloc(serial->rx_data_length, GFP_KERNEL))) {
		ERR("%s - Out of memory", __FUNCTION__);
		goto exit;
	}

	serial->tx_data_length = 0;
	serial->tx_data_count = 0;
	serial->tx_data = NULL;
	serial->tx_urb = NULL;

	if (!(serial->tx_urb = usb_alloc_urb(0, GFP_KERNEL))) {
		ERR("Could not allocate urb?");
		goto exit;
	}
	serial->tx_urb->transfer_buffer = NULL;
	serial->tx_urb->transfer_buffer_length = 0;

	serial->tx_data_length = 
		(type == MUX_INTERFACE) ? CTRL_URB_TX_SIZE : BULK_URB_TX_SIZE;

	if (!(serial->tx_data = kzalloc(serial->tx_data_length, GFP_KERNEL))) {
		ERR("%s - Out of memory", __FUNCTION__);
		goto exit;
	}

	/* The muxed ports can not use full 64 bytes and the diagnostic must use 
	 * the full 64 bytes if sending larger packets. */
	if (type == MUX_INTERFACE)
		serial->tx_data_length--;

	if (type == MUX_INTERFACE) {
		serial->write_data = hso_mux_serial_write_data;
	} else {
		serial->write_data = hso_std_serial_write_data;
	}

	serial->open_count = 0;
	serial->private = NULL;
	init_MUTEX(&serial->sem);

	set_serial_by_index(minor,serial);

	return serial;

      exit:
	if (serial->rx_urb) {
		usb_free_urb(serial->rx_urb);
	}
	if (serial->rx_data) {
		kfree(serial->rx_data);
	}
	if (serial->tx_data) {
		kfree(serial->tx_data);
	}
	if (serial->tx_urb) {
		usb_free_urb(serial->tx_urb);
	}

	if (serial) {
		kfree(serial);
	}
	return NULL;
}

/* release the serial port */
static void hso_serial_stop(struct usb_device *dev, u8 minor,
			    struct hso_priv *odev)
{
	struct hso_serial *serial;

	D1("Deregistering serial %d", minor);

	serial = get_serial_by_index(minor);
	if (serial == NULL || serial->magic != HSO_SERIAL_MAGIC) {
		ERR("Trying to deregister an unused serial port");
		return;
	}

	if (serial->odev != odev) {
		ERR("Trying to deregister a serial port belonging to a different device");
		return;
	}

	serial->odev = NULL;
	serial->minor = 0;
	serial->mux = 0;
	serial->magic = 0;

	serial->write_data = NULL;

	if (serial->rx_urb != NULL) {
		usb_unlink_urb(serial->rx_urb);
		usb_free_urb(serial->rx_urb);
	}
	serial->rx_urb = NULL;

	if (serial->rx_data != NULL)
		kfree(serial->rx_data);
	serial->rx_data = NULL;

	if (serial->tx_urb != NULL) {
		usb_unlink_urb(serial->tx_urb);
		usb_free_urb(serial->tx_urb);
	}
	serial->tx_urb = NULL;

	if (serial->tx_data != NULL)
		kfree(serial->tx_data);
	serial->tx_data = NULL;

	kfree(serial);
	serial = NULL;

	tty_unregister_device(tty_drv,minor);

	set_serial_by_index(minor,NULL);
}

static struct tty_operations hso_serial_ops = {
	.open =                 hso_serial_open,
	.close =                hso_serial_close,
	.write =                hso_serial_write,
	.write_room =           hso_serial_write_room,
	.ioctl =                hso_serial_ioctl,
	.set_termios =          hso_serial_set_termios,
	.throttle =             hso_serial_throttle,
	.unthrottle =           hso_serial_unthrottle,
	.break_ctl =            hso_serial_break,
	.chars_in_buffer =      hso_serial_chars_in_buffer,
	.read_proc =            hso_serial_read_proc,
	/*        .tiocmget =             serial_tiocmget, */
	/*        .tiocmset =             serial_tiocmset, */
	.hangup =		hso_serial_hangup,
};


/* AJB, called by hso_init() once per load */
static int hso_serial_init(void)
{
	int result;
	D1("Starting hso_serial");

        tty_drv = alloc_tty_driver(HSO_SERIAL_TTY_MINORS);
        if (!tty_drv)
                return -ENOMEM;

	/* register the tty driver */
        tty_drv->magic = TTY_DRIVER_MAGIC;
        tty_drv->owner = THIS_MODULE;
        tty_drv->driver_name = "hso";
        tty_drv->name = "ttyHS";

#ifdef HSO_SERIAL_TTY_MAJOR
	tty_drv->major = HSO_SERIAL_TTY_MAJOR;
#endif
        tty_drv->minor_start = 0;
        tty_drv->num = HSO_SERIAL_TTY_MINORS;  /* FIXME - can we get away without this here? */

        tty_drv->type = TTY_DRIVER_TYPE_SERIAL;
        tty_drv->subtype = SERIAL_TYPE_NORMAL;
#if ( LINUX_VERSION_CODE < KERNEL_VERSION(2,6,20) )
        tty_drv->flags = TTY_DRIVER_REAL_RAW | TTY_DRIVER_NO_DEVFS;
#else
        tty_drv->flags = TTY_DRIVER_REAL_RAW | TTY_DRIVER_DYNAMIC_DEV;
#endif

        tty_drv->init_termios = tty_std_termios;
        tty_drv->init_termios.c_cflag = B9600 | CS8 | CREAD | HUPCL | CLOCAL;

	tty_drv->termios = hso_serial_termios;
	tty_drv->termios_locked = hso_serial_termios_locked;

        tty_set_operations(tty_drv, &hso_serial_ops);

        result = tty_register_driver(tty_drv);
        if (result) {
                err("%s - tty_register_driver failed(%d)", __FUNCTION__,result);
		return result;
        }

	D1("end hso_serial");

	return 0;
}

/* AJB, called by hso_exit() once per unload */
static void hso_serial_exit(void)
{
	struct hso_serial *serial = NULL;
	int i;
	int result;

	D1("Stopping hso_serial");

	for (i = 0; i < HSO_SERIAL_TTY_MINORS; i++) {
		serial = get_serial_by_index(i);
		if (serial != NULL) {
			down(&serial->sem);
			while (serial->open_count > 0) {
				__hso_serial_close(serial);
			}
			hso_serial_stop(serial->odev->usb, i, serial->odev);
			up(&serial->sem);
		}
	}

	result = tty_unregister_driver(tty_drv);
        if (result) {
                err("%s - tty_unregister_driver failed(%d)", __FUNCTION__,result);
        }
}

/*
 * Module definitions
 */
module_init(hso_init);
module_exit(hso_exit);

MODULE_AUTHOR(MOD_AUTHOR);
MODULE_DESCRIPTION("USB High Speed Option driver");
MODULE_LICENSE("GPL");

MODULE_DEVICE_TABLE(usb, hso_ids);

MODULE_PARM_DESC(debug, "Level of debug [0x01 | 0x02 | 0x04 | 0x08]");
module_param(debug, int, S_IRUGO | S_IWUSR);

