#ifndef _H_MXHW_CRYPTO_DRIVER
#define _H_MXHW_CRYPTO_DRIVER

#include "mxhw_crypto_io.h"

/*
	We have some terms defined to make the code readable

	user process:	the process that makes cipher requests in the user space
	io caller:		it represents a user process in the kernel space
	dispatcher:		a daemon thread that repeatedly pulls out requests from a queue
					exclusively appended to the dispatcher and pass these requests to 
					the underlying engine one-by-one
*/
#define	CRYPTO_MAJOR		11		/* major number of the driver */
#define	CRYPTO_MINOR		131		/* minor number of the driver */
#define CRYPTO_DEVNAME		"mxcrypto"

typedef struct _QMBPTR QMBPTR;

/* an io caller at the kernel space */
typedef struct _IOCTRL
{
	u32		cpid;		/* [index][pid] the index to the array of callers + the pid of the caller */
	u32		cfid;		/* an id simplifies the cipher information */
	CIPHER	info;		/* the payload information of a cipher request */
	QMBPTR	*outq;		/* a list of processed packets to be uploaded to the user process */
	u32		pkt_num;	/* totoal processed packets */
} IOCTRL;

/* a wrapper of data to be processed */
typedef struct _IOMBUF
{
	u8		*virt;		/* pointer to an allocated space where data would be stored */
	u32		phys;		/* physical address the engine would access */
	u32		size;		/* the size of allocated space */
	u32		dlen;		/* the length of data to be encrypted/decrypted */
	u32		offs;		/* offset the actual data being stored, within the offset, 
								some info might be utilized */
} IOMBUF;

/* a request unit (context buffer) the dispatcher handles */
typedef struct _QMBCTX
{
	u32		status;			/* the status the dispatch reports */
	IOCTRL	ictx;			/* a new copy of the cipher control upon any request. in case,
								any change is made for the next request */
	IOMBUF	imbuf;			/* a wrapper of input buffer */
	IOMBUF	ombuf;			/* a wrapper of output buffer */
	struct _QMBCTX	*next;  /* link to the next request to be handled */
} QMBCTX;

/* 	an aggregate data structure maintaining a queue of context buffers with
	a spin lock and a waiting queue */
struct _QMBPTR
{
	QMBCTX				*head;		/* where to dequeue */
	QMBCTX				*tail;		/* where to enqueue */
	spinlock_t			lock;		/* lock upon any operation on this queue */
	wait_queue_head_t	quew;		/* apply sleeping before dequeue and waking up after enqueue */
};

#define IOMBUF_OFFS(m) m->offs
#define IOMBUF_VIRT(m) m->virt
#define IOMBUF_DATA(m) (m->virt+m->offs)
#define IOMBUF_SIZE(m) m->size
#define IOMBUF_PHYS(m) m->phys
#define IOMBUF_DLEN(m) m->dlen

#define QUEUE_LENGTH(s,p,n) \
if(1) { \
	n = 0; \
	QMBCTX *i; \
	spin_lock(&p->lock); \
	i = p->head; \
	while (i!=NULL) { i=i->next; n++; } \
	spin_unlock(&p->lock); \
}

#define WAKE_UP_QUEUE(p) wake_up_interruptible(&p->quew)

#define ENQUEUE_CONTEXT(p,i) \
do { \
	if(i!=NULL) \
	{ \
		spin_lock(&p->lock); \
		if (p->head==NULL) \
			p->head=i; \
		else \
			p->tail->next=i; \
		p->tail=i; \
		while (p->tail->next!=NULL) p->tail=p->tail->next; \
		spin_unlock(&p->lock); \
		WAKE_UP_QUEUE(p); \
	} \
} while(0)

#define DEQUEUE_CONTEXT(q,i,j,c) \
do { \
	i=NULL; \
	if (j==0 && wait_event_interruptible(q->quew, q->head!=NULL||(c)) !=0) \
		break; \
	spin_lock(&q->lock); \
	if ((i=q->head)!=NULL) \
	{ \
		if (i==q->tail) \
			q->tail=NULL; \
		q->head=q->head->next; \
		i->next=NULL; \
	} \
	spin_unlock(&q->lock); \
} while(0)

/* all global variables */
typedef struct _global_p
{
	int		major_num;
	int		dspt_thrd;			/* thread id of the dispatcher */
	u32		dspt_exit;			/* force the thread to die */
	u32		pkt_num;			/*  */
	QMBPTR	*free_ctxq;			/* 	1. a list of free contexts that io owners compete with.
									2. a waiting queue embedded with io owners that are ready 
						   				to be awaken up by any other owner when it releases a
						   				processed packet and by the dispatcher when a packet
						   				goes nowhere. */
	QMBPTR	*dspt_ctxq;			/*	1. a list of io packets that the dispatcher would handle. 
									2. a waiting queue embedded with io owners that are ready 
						   				to be awaken up by the dispatcher when a packet is
						   				completed with encryption.
									3. a waiting queue embedded with the dispatcher only that
						   				is ready to be awaken up by any of the io owners when
						   				a request arrives */
	IOCTRL	pool[MAX_CIPHER_CLIENTS];	/* buffers for the callers */
} global_p;

extern global_p global;

/*	macros defining operations on a list of all current io callers, 
	share with a spin lock, the function that these macros reference to 
	makes sure that a processed packet associated with a caller can be 
	returned back to the free pool if the caller was gone already. */
#define IOCTRL_OP_CHKIN	1
#define IOCTRL_OP_CLOSE	2
#define IOCTRL_OP_ALIVE	3
#define IOCTRL_OP_FREEQ	4
#define mxhw_crypto_iocall_chkin(i) mxhw_crypto_iocall_op(i,IOCTRL_OP_CHKIN)
#define mxhw_crypto_iocall_close(i) mxhw_crypto_iocall_op(i,IOCTRL_OP_CLOSE)
#define mxhw_crypto_iocall_alive(i) mxhw_crypto_iocall_op(i,IOCTRL_OP_ALIVE)
#define mxhw_crypto_iocall_freeq(i) mxhw_crypto_iocall_op(i,IOCTRL_OP_FREEQ)

IOCTRL* mxhw_crypto_iocall_op(u32 cpid, int type);

/* this function takes the cipher info (type, algo, mode) as the input, tells the caller the key length, 
	the block length, and outputs a control code (could be an id) 
		return 0 on success, otherwise on failure
*/
int	mxhw_crypto_engine_register(CIPHER *info, u32 *ctrl);

/* the inverse operation of the above function, take the ctrol code as the input if you want to close it */
void mxhw_crypto_engine_unregister(u32 ctrl);

/* this function may have a loop, please check the exited code in the loop, if the code is up, then break 
	the loop and return an positive integer 
		return 0 on success, otherwise on failure
*/
int mxhw_crypto_engine_perform(u32 ctrl, CIPHER *info, IOMBUF *imbuf, IOMBUF *ombuf, u32 *exited);

/* any stuff you need to start an engine
		return 0 on success, otherwise on failure
*/
int	mxhw_crypto_engine_up(void);

/* any stuff you need to cleanup for the engine */
void mxhw_crypto_engine_down(void);

/* this function takes a body of IOMBUF as input and allocates memory (virtual and physical) with the size
	set offset if necessary.
		return 0 on success, otherwise on failure
*/
int	mxhw_crypto_engine_dma_malloc(IOMBUF *mbuf, u32 size);
void mxhw_crypto_engine_dma_mfree(IOMBUF *mbuf);

#ifdef DEBUG
void	hexit(char *msg, u_char *d, int len);
#else
#define hexit(a,b,c) 
#endif

#endif
