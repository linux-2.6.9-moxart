/*
 *	History:
 *	Date		Author			Comment
 *	11-15-2005	Victor Yu.		Create it.
 */

#include <linux/config.h>
#include <linux/module.h>
#include <linux/types.h>
#include <linux/kernel.h>

#include <linux/errno.h>
#include <linux/init.h>

#include <linux/mtd/mtd.h>
#include <linux/mtd/map.h>
#include <linux/mtd/partitions.h>
#include <linux/reboot.h>

#include <asm/hardware.h>
#include <asm/io.h>

static struct map_info mcpu_map_flash1 = {
	.name		= "IA241-32128-1",
	.bankwidth	= 2,
	.phys		= CPE_FLASH_BASE,
	.size		= CPE_FLASH_SZ,
	.virt		= CPE_FLASH_VA_BASE,
};

static struct map_info mcpu_map_flash2 = {
	.name		= "IA241-32128-2",
	.bankwidth	= 2,
	.phys		= CPE_FLASH_BASE2,
	.size		= CPE_FLASH_SZ,
	.virt		= CPE_FLASH_VA_BASE2,
};

#define BOOT_LOADER_SIZE	0x40000
#define KERNEL_BOOT_LOADER_SIZE	0x300000
#define KERNEL_SIZE		(KERNEL_BOOT_LOADER_SIZE-BOOT_LOADER_SIZE)
#define ROOT_DISK_SIZE		0xd00000
#define USER_DISK_SIZE		CPE_FLASH_SZ
static struct mtd_partition mcpu_flash_partitions1[] = {
	{
		.name =		"BootLoader",
		.size =		BOOT_LOADER_SIZE,/* hopefully Moxa boot will stay 128k + 128*/
		.offset =	0,
		//.mask_flags =	MTD_WRITEABLE,  /* force read-only */
	}, {
		.name =		"Kernel",
		.size =		KERNEL_SIZE,
		.offset =	MTDPART_OFS_APPEND,
		//.mask_flags =	MTD_WRITEABLE,  /* force read-only */
	}, {
		.name =		"RootDisk",
		.size =		ROOT_DISK_SIZE,
		.offset =	MTDPART_OFS_APPEND,
		//.mask_flags =	MTD_WRITEABLE,  /* force read-only */
	}
};
static struct mtd_partition mcpu_flash_partitions2[] = {
	{
		.name =		"UserDisk",
		.size =		USER_DISK_SIZE,
		.offset = 	MTDPART_OFS_APPEND,
		//.mask_flags =	MTD_WRITEABLE,
	}
};

static int mtd_reboot(struct notifier_block *n, unsigned long code, void *p)
{
        if(code != SYS_RESTART)
                return NOTIFY_DONE;

	*( u16 *)(CPE_FLASH_VA_BASE + (0x55 * 2)) = 0xff;
        return NOTIFY_DONE;
}

static struct notifier_block mtd_notifier = {
	notifier_call:  mtd_reboot,
	next:           NULL,
	priority:       0
};

static struct mtd_info *flash_mtd1;
static struct mtd_info *flash_mtd2;
 
static int __init init_flash (void)   
{

	/*
	 * Static partition definition selection
	 */
	simple_map_init(&mcpu_map_flash1);
	simple_map_init(&mcpu_map_flash2);

	/*
	 * Now let's probe for the actual flash.  Do it here since
	 * specific machine settings might have been set above.
	 */
	printk(KERN_NOTICE "Moxa CPU flash: probing %d-bit flash bus\n",
		mcpu_map_flash1.bankwidth*8);
	flash_mtd1 = do_map_probe("cfi_probe", &mcpu_map_flash1);
	if (!flash_mtd1)
		return -ENXIO;
	flash_mtd2 = do_map_probe("cfi_probe", &mcpu_map_flash2);
	if (!flash_mtd2)
		return -ENXIO;
 
	printk(KERN_NOTICE "Using static partition definition\n");
	add_mtd_partitions(flash_mtd1, mcpu_flash_partitions1, ARRAY_SIZE(mcpu_flash_partitions1));
	return add_mtd_partitions(flash_mtd2, mcpu_flash_partitions2, ARRAY_SIZE(mcpu_flash_partitions2));
}
 
int __init mcpu_mtd_init(void)  
{
	int status;

 	if (status == init_flash()) {
		printk(KERN_ERR "Flash: unable to init map for flash\n");
	} else {
		register_reboot_notifier(&mtd_notifier);
	}
	return status;
}

static void  __exit mcpu_mtd_cleanup(void)  
{
	if (flash_mtd1) {
		unregister_reboot_notifier(&mtd_notifier);
		del_mtd_partitions(flash_mtd1);
		map_destroy(flash_mtd1);
		del_mtd_partitions(flash_mtd2);
		map_destroy(flash_mtd2);
	}
}

module_init(mcpu_mtd_init);
module_exit(mcpu_mtd_cleanup);

MODULE_AUTHOR("Victor Yu.");
MODULE_DESCRIPTION("Moxa CPU board MTD device driver");
MODULE_LICENSE("GPL");
