
/*
 * This program is the Moxa CPU ethernet device driver.
 *
 * History:
 * Date		Author			Comment
 * 06-15-2005	Victor Yu.		Create it. Make it for Faraday demo board.
 * 11-04-2005	Victor Yu.		Modify it to support Moxa CPU demo board.
 */

#include <linux/config.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/types.h>
#include <linux/fcntl.h>
#include <linux/interrupt.h>
#include <linux/ioport.h>
#include <linux/in.h>
#include <linux/slab.h>
#include <linux/string.h>
#include <linux/errno.h>
#include <linux/init.h>
#include <linux/netdevice.h>
#include <linux/etherdevice.h>
#include <linux/spinlock.h>
#include <linux/skbuff.h>
#include <linux/device.h>
#include <linux/dma-mapping.h>
#include <linux/delay.h>
#include <linux/workqueue.h>
#include <asm/uaccess.h>
#include <asm/system.h>
#include <asm/bitops.h>
#include <asm/io.h>
#include <asm/irq.h>
#include <asm/dma.h>
#include "moxacpu_mac.h"

//#define MCPU_MAC_DEBUG
//#define USE_SCHEDULE_WORK

#if (defined CONFIG_ARCH_IA241_32128)||(defined CONFIG_ARCH_IA241_16128)// add by Victor Yu. 05-22-2007
#define CONFIG_ARCH_IA241
#endif

#if (defined CONFIG_ARCH_UC_7112_LX_PLUS_LITON)
#define CONFIG_ARCH_UC_7112_LX_PLUS
#endif

#ifdef MCPU_MAC_DEBUG
#define dbg_printk(x...)	printk(x)
#else	// MCPU_MAC_DEBUG
#define dbg_printk(x...)
#endif	// MCPU_MAC_DEBUG

#define TX_DESC_NUM		64
#define TX_DESC_NUM_MASK	(TX_DESC_NUM-1)
#define RX_DESC_NUM		64
#define RX_DESC_NUM_MASK	(RX_DESC_NUM-1)
#define TX_BUF_SIZE		1600
#define RX_BUF_SIZE		1600
#if TX_BUF_SIZE >= TXBUF_SIZE_MAX
#error Moxa CPU ethernet device driver Tx buffer size too large !
#endif
#if RX_BUF_SIZE >= RXBUF_SIZE_MAX
#error Moxa CPU ethernet device driver Rx buffer size too large !
#endif

static mcpu_mac_priv_t	mcpu_mac_priv;
#ifdef CONFIG_ARCH_MOXACPU

#if ( defined CONFIG_ARCH_IA240 ) || ( defined CONFIG_ARCH_IA241 ) || ( defined CONFIG_ARCH_UC_7112_LX_PLUS )
static mcpu_mac_priv_t	mcpu_mac_priv2;
#endif

#endif	// CONFIG_ARCH_MOXACPU


#ifdef HAVE_MULTICAST
static int crc32( char * s, int length )
{
        /* indices */
        int			perByte;
        int			perBit;
        /* crc polynomial for Ethernet */
        const unsigned long	poly=0xedb88320;
        /* crc value - preinitialized to all 1's */
        unsigned long		crc_value=0xffffffff;

        for ( perByte = 0; perByte < length; perByte ++ ) {
                unsigned char   c;

                c = *(s++);
                for ( perBit = 0; perBit < 8; perBit++ ) {
                        crc_value = (crc_value>>1)^
                                (((crc_value^c)&0x01)?poly:0);
                        c >>= 1;
                }
        }
        return  crc_value;
}

static void mcpu_mac_setmulticast(unsigned int ioaddr, int count, struct dev_mc_list * addrs )
{
	struct dev_mc_list	*cur_addr;
	int			crc_val;

	for (cur_addr = addrs ; cur_addr!=NULL ; cur_addr = cur_addr->next ) {
		if ( !( *cur_addr->dmi_addr & 1 ) )
			continue;
		crc_val = crc32( cur_addr->dmi_addr, 6 );
		crc_val = (crc_val>>26)&0x3f;                   // ¨ú MSB 6 bit
		if (crc_val >= 32)
			outl(inl(ioaddr+MATH1_REG_OFFSET) | (1UL<<(crc_val-32)), ioaddr+MATH1_REG_OFFSET);
		else
			outl(inl(ioaddr+MATH0_REG_OFFSET) | (1UL<<crc_val), ioaddr+MATH0_REG_OFFSET);
	}
}

static void mcpu_mac_set_multicast_list(struct net_device *dev)
{
	mcpu_mac_priv_t		*priv=(mcpu_mac_priv_t *)dev->priv;
	unsigned long		flags;

	save_flags(flags);
	cli();
#ifdef USE_SCHEDULE_WORK
	spin_lock(&priv->rxlock);
#endif
	spin_lock(&priv->txlock);

	if ( dev->flags & IFF_PROMISC )
		priv->maccr |= RCV_ALL;
	else
		priv->maccr &= ~RCV_ALL;

	if ( dev->flags & IFF_ALLMULTI )
		priv->maccr |= RX_MULTIPKT;
	else
		priv->maccr &= ~RX_MULTIPKT;

	if ( dev->mc_count ) {
		priv->maccr |= RX_MULTIPKT; //add by Johnson.Liu 2007/07/06
		priv->maccr |= HT_MULTI_EN;
		mcpu_mac_setmulticast(dev->base_addr, dev->mc_count, dev->mc_list);
	} else {
                priv->maccr &= ~RX_MULTIPKT; //add by Johnson.Liu 2007/07/06
		priv->maccr &= ~HT_MULTI_EN;
	}		

	outl(priv->maccr, dev->base_addr+MACCR_REG_OFFSET);

	spin_unlock(&priv->txlock);
#ifdef USE_SCHEDULE_WORK
	spin_unlock(&priv->rxlock);
#endif
	restore_flags(flags);
}
#endif	// HAVE_MULTICAST

#if 1	// add by Victor Yu. 07-04-2005
static void mywordcopy(void *dest, void *source, int len)
{
	unsigned short	*pd=(unsigned short *)dest;
	unsigned short	*ps=(unsigned short *)source;
	int		wlen=len>>1;

	while ( wlen > 0 ) {
		*pd++=*ps++;
		wlen--;
	}
	if ( len & 1 )
		*(unsigned char *)pd = *(unsigned char *)ps;
}
#endif	// 07-04-2005

static void mcpu_mac_recv(void *ptr)
{
	struct net_device	*dev=(struct net_device *)ptr;
	mcpu_mac_priv_t		*priv=(mcpu_mac_priv_t *)dev->priv;
	rx_desc_t		*rxdesc;
	int			len;
	struct sk_buff		*skb;

	unsigned char		*data;
#if 1	// add by Victor Yu. 07-04-2005
	unsigned int		ui;
	int			rxnow=priv->RxDescNow;
#endif	// 07-04-2005
#ifndef USE_SCHEDULE_WORK
	int			loops=RX_DESC_NUM;
#endif	// USE_SCHEDULE_WORK

	dbg_printk("mcpu_mac_recv test01\n");


#if 0
	dbg_printk("RxDescNow=%d, desc phy=0x%x, virt=0x%x, buf phy=0x%x, virt=0x%x\n", priv->RxDescNow, priv->phyRxDescBaseAddr+(priv->RxDescNow*sizeof(rx_desc_t)), (unsigned int)&priv->virtRxDescBaseAddr[priv->RxDescNow], priv->virtRxDescBaseAddr[priv->RxDescNow].rxdes2.phyRxBufBaseAddr, (unsigned int)priv->virtRxDescBaseAddr[priv->RxDescNow].rxdes2.virtRxBufBaseAddr);
	dbg_printk("Now Rx desc des0=0x%x, des1=0x%x\n", priv->virtRxDescBaseAddr[priv->RxDescNow].rxdes0.ui, priv->virtRxDescBaseAddr[priv->RxDescNow].rxdes1.ui);
#endif


	
#ifdef USE_SCHEDULE_WORK
	spin_lock(&priv->rxlock);
#endif	// USE_SCHEDULE_WORK
repeat_recv:
	rxdesc = &priv->virtRxDescBaseAddr[rxnow];
#if 0	// mask by Victor Yu. 07-04-2005
	if ( rxdesc->rxdes0.ubit.RxDMAOwn ) {
#else	// add by Victor Yu. 07-04-2005
	ui = rxdesc->rxdes0.ui;
	if ( ui & RXDMA_OWN ) {
#endif	// 07-04-2005
#ifdef USE_SCHEDULE_WORK
		spin_unlock(&priv->rxlock);
#else
#ifdef MCPU_MAC_DEBUG
		if ( loops == RX_DESC_NUM )
			printk("Bad receive packet !\n");
#endif	// MCPU_MAC_DEBUG
#endif	// USE_SCHEDULE_WORK
		return;
	}
#if 0	// mask by Victor Yu. 07-04-2005
	if ( rxdesc->rxdes0.ubit.RxErr ||
	     rxdesc->rxdes0.ubit.CRCErr ||
	     rxdesc->rxdes0.ubit.Ftl ||
	     rxdesc->rxdes0.ubit.Runt ||
	     rxdesc->rxdes0.ubit.RxOddNb ) {
#else	// add by Victor Yu. 07-04-2005
#if defined(CONFIG_VLAN_8021Q) || defined(CONFIG_VLAN_8021Q_MODULE)
	if ( ui & (RX_ERR|CRC_ERR|RUNT|RX_ODD_NB) ) {
#else
	if ( ui & (RX_ERR|CRC_ERR|FTL|RUNT|RX_ODD_NB) ) {
#endif
#endif	// 07-04-2005
		dbg_printk("Ethernet receive packet error !\n");
		priv->stats.rx_dropped++;
		priv->stats.rx_errors++;
		goto recv_finish;
	}
#if 0	// mask by Victor Yu. 07-04-2005
	len = rxdesc->rxdes0.ubit.RecvFrameLen > RX_BUF_SIZE ? RX_BUF_SIZE : rxdesc->rxdes0.ubit.RecvFrameLen;
#else	// add by Victor Yu. 07-04-2005
	len = ui & RFL_MASK;
	if ( len > RX_BUF_SIZE )
		len = RX_BUF_SIZE;
#endif	// 07-04-2005
	skb = dev_alloc_skb(len+2);
	if ( skb == NULL ) {
		dbg_printk("Allocate memory fail !\n");
		priv->stats.rx_dropped++;
		goto recv_finish;
	}
	skb_reserve(skb, 2);
	skb->dev = dev;
	data = skb_put(skb, len);
	dbg_printk("receive data pointer = 0x%x\n", (unsigned long)data);
#if 0	// mask by Victor Yu. 07-04-2005
	memcpy(data, rxdesc->rxdes2.virtRxBufBaseAddr, len);
#else	// add by Victor Yu. 07-04-2005
	mywordcopy((void *)data, (void *)rxdesc->rxdes2.virtRxBufBaseAddr, len);
#endif	// 07-04-2005
	skb->protocol = eth_type_trans(skb, dev);
	netif_rx(skb);
	priv->stats.rx_packets++;
	priv->stats.rx_bytes += len;
	if ( ui & MULTICAST_RXDES0 )
		priv->stats.multicast++;
	dbg_printk("Receive a good packet.\n");



recv_finish:
#if 0	// mask by Victor Yu. 07-04-2005
	rxdesc->rxdes0.ui = 0;
	rxdesc->rxdes0.ubit.RxDMAOwn = 1;
	rxdesc->rxdes1.ubit.RxBufSize = RX_BUF_SIZE;
#else	// add by Victor Yu. 07-04-2005
	rxdesc->rxdes0.ui = RXDMA_OWN;
#endif	// 07-04-2005
	rxnow++;
	rxnow &= RX_DESC_NUM_MASK;
	priv->RxDescNow = rxnow;

#ifdef USE_SCHEDULE_WORK
	goto repeat_recv;
#else	// USE_SCHEDULE_WORK
	if ( loops-- > 0 )
		goto repeat_recv;
#endif	// USE_SCHEDULE_WORK

#ifdef USE_SCHEDULE_WORK
	spin_unlock(&priv->rxlock);
#endif	// USE_SCHEDULE_WORK


}


static void mcpu_mac_free_memory(struct net_device *dev)
{
	mcpu_mac_priv_t		*priv=(mcpu_mac_priv_t *)dev->priv;

	if ( priv->virtTxDescBaseAddr )
		dma_free_coherent(NULL, sizeof(tx_desc_t)*TX_DESC_NUM, priv->virtTxDescBaseAddr, priv->phyTxDescBaseAddr);
	if ( priv->virtRxDescBaseAddr )
		dma_free_coherent(NULL, sizeof(rx_desc_t)*RX_DESC_NUM, priv->virtRxDescBaseAddr, priv->phyRxDescBaseAddr);
	if ( priv->virtTxBufBaseAddr )
		dma_free_coherent(NULL, TX_BUF_SIZE*TX_DESC_NUM, priv->virtTxBufBaseAddr, priv->phyTxBufBaseAddr);
	if ( priv->virtRxBufBaseAddr )
		dma_free_coherent(NULL, RX_BUF_SIZE*RX_DESC_NUM, priv->virtRxBufBaseAddr, priv->phyRxBufBaseAddr);
}

static void mcpu_mac_setup_desc_ring(struct net_device *dev)
{
	mcpu_mac_priv_t		*priv=(mcpu_mac_priv_t *)dev->priv;
	int			i;
	tx_desc_t		*txdesc;
	rx_desc_t		*rxdesc;
	unsigned char		*virtbuf;
	unsigned int		phybuf;

	virtbuf = priv->virtTxBufBaseAddr;
	phybuf = priv->phyTxBufBaseAddr;
	for ( i=0; i<TX_DESC_NUM; i++, virtbuf+=TX_BUF_SIZE, phybuf+=TX_BUF_SIZE ) {
		txdesc = &priv->virtTxDescBaseAddr[i];
		memset(txdesc, 0, sizeof(tx_desc_t));
		txdesc->txdes2.phyTxBufBaseAddr = phybuf;
		txdesc->txdes2.virtTxBufBaseAddr = virtbuf;
	}
	priv->virtTxDescBaseAddr[TX_DESC_NUM-1].txdes1.ubit.Edotr = 1;
	
	virtbuf = priv->virtRxBufBaseAddr;
	phybuf = priv->phyRxBufBaseAddr;
	for ( i=0; i<RX_DESC_NUM; i++, virtbuf+=RX_BUF_SIZE, phybuf+=RX_BUF_SIZE ) {
		rxdesc = &priv->virtRxDescBaseAddr[i];
		memset(rxdesc, 0, sizeof(rx_desc_t));
		rxdesc->rxdes0.ubit.RxDMAOwn = 1;
		rxdesc->rxdes1.ubit.RxBufSize = RX_BUF_SIZE;
		rxdesc->rxdes2.phyRxBufBaseAddr = phybuf;
		rxdesc->rxdes2.virtRxBufBaseAddr = virtbuf;
	}
	priv->virtRxDescBaseAddr[RX_DESC_NUM-1].rxdes1.ubit.Edorr = 1;
	//dbg_printk("First Rx desc des0=0x%x, des1=%x\n", priv->virtRxDescBaseAddr[0].rxdes0.ui, priv->virtRxDescBaseAddr[0].rxdes1.ui);

	priv->TxDescNow = priv->RxDescNow = 0;
	
	// reset the MAC controler Tx/Rx desciptor base address
	outl(priv->phyTxDescBaseAddr, dev->base_addr+TXR_BADR_REG_OFFSET);
	outl(priv->phyRxDescBaseAddr, dev->base_addr+RXR_BADR_REG_OFFSET);
#if 0
	dbg_printk("Tx/Rx desc phy=0x%x,0x%x, virt=0x%x,0x%x\n", priv->phyTxDescBaseAddr, priv->phyRxDescBaseAddr, (unsigned int)priv->virtTxDescBaseAddr, (unsigned int)priv->virtRxDescBaseAddr);
	dbg_printk("set Tx desc base address=0x%x, Rx=0x%x\n", inl(dev->base_addr+TXR_BADR_REG_OFFSET), inl(dev->base_addr+RXR_BADR_REG_OFFSET));
#endif
}

static void mcpu_mac_reset(struct net_device *dev)
{
	unsigned int	reg=dev->base_addr+MACCR_REG_OFFSET;

	outl(SW_RST, reg);	// software reset
	while ( inl(reg) & SW_RST ) mdelay(10);
	// maybe we need to disable the all interrupt
	outl(0, dev->base_addr+IMR_REG_OFFSET);
//	((mcpu_mac_priv_t *)dev->priv)->maccr = RX_BROADPKT | FULLDUP | CRC_APD;
#if defined(CONFIG_VLAN_8021Q) || defined(CONFIG_VLAN_8021Q_MODULE) //johnson modify for 802.1Q support 2008-11-14
	((mcpu_mac_priv_t *)dev->priv)->maccr = RX_BROADPKT | ENRX_IN_HALFTX | CRC_APD | RX_FTL;
#else
	((mcpu_mac_priv_t *)dev->priv)->maccr = RX_BROADPKT | ENRX_IN_HALFTX | CRC_APD;
#endif
}

static void mcpu_mac_set_mac_address(unsigned int base, unsigned char *macaddr)
{
	unsigned int	val;

	val = (((u32)macaddr[0] << 8) &0xff00) | ((u32)macaddr[1] & 0xff);
	outl(val, base);
	val = (((u32)macaddr[2]<<24) & 0xff000000) |
	      (((u32)macaddr[3]<<16) & 0x00ff0000) |
	      (((u32)macaddr[4]<<8)  & 0x0000ff00) |
	      (((u32)macaddr[5]) & 0x000000ff);
	outl(val, base+4);
}

#ifdef MCPU_MAC_DEBUG	// add by Victor Yu. 03-14-2006
static void mcpu_mac_get_mac_address(unsigned int base, unsigned char *macaddr)
{
	unsigned int	val;

	val = inl(base);
	macaddr[0] = (val >> 8) & 0xff;
	macaddr[1] = val & 0xff;
	val = inl(base+4);
	macaddr[2] = (val >> 24) & 0xff;
	macaddr[3] = (val >> 16) & 0xff;
	macaddr[4] = (val >> 8) & 0xff;
	macaddr[5] = val & 0xff;
}
#endif

static void mcpu_mac_enable(struct net_device *dev)
{
	mcpu_mac_priv_t		*priv=(mcpu_mac_priv_t *)dev->priv;
	unsigned int		base=dev->base_addr;

	outl(0x00001010, base+ITC_REG_OFFSET);
	outl(0x00000001, base+APTC_REG_OFFSET);
	outl(0x00000390, base+DBLAC_REG_OFFSET);
#ifdef MCPU_MAC_DEBUG
	outl(RPKT_FINISH_M|NORXBUF_M|AHB_ERR_M, base+IMR_REG_OFFSET);
#else
	outl(RPKT_FINISH_M, base+IMR_REG_OFFSET);
#endif
	priv->maccr |= (RCV_EN | XMT_EN | RDMA_EN | XDMA_EN);
	outl(priv->maccr, base+MACCR_REG_OFFSET);
}

static void mcpu_mac_tx_timeout(struct net_device *dev)
{
	mcpu_mac_priv_t	*priv=(mcpu_mac_priv_t *)dev->priv;
	unsigned long	flags;

	dbg_printk("mcpu_mac_tx_timeout test01\n");
	save_flags(flags);
	cli();
#ifdef USE_SCHEDULE_WORK
	spin_lock(&priv->rxlock);
#endif
	spin_lock(&priv->txlock);
	mcpu_mac_reset(dev);
	mcpu_mac_set_mac_address(dev->base_addr+MAC_MADR_REG_OFFSET, dev->dev_addr);
	mcpu_mac_setup_desc_ring(dev);
	mcpu_mac_enable(dev);
	spin_unlock(&priv->txlock);
#ifdef USE_SCHEDULE_WORK
	spin_unlock(&priv->rxlock);
#endif
	restore_flags(flags);
	netif_wake_queue(dev);
	dev->trans_start = jiffies;
}

static int mcpu_mac_hard_start_xmit(struct sk_buff *skb, struct net_device *dev)
{
	mcpu_mac_priv_t		*priv=(mcpu_mac_priv_t *)dev->priv;
	tx_desc_t		*txdesc;
	int			len;
	int			txnow=priv->TxDescNow;

	dbg_printk("mcpu_mac_hard_start_xmit test01\n");
	spin_lock(&priv->txlock);

	// first check the Tx buffer is enough or not
	txdesc = &priv->virtTxDescBaseAddr[txnow];
	if ( txdesc->txdes0.ubit.TxDMAOwn ) {
		dbg_printk("No Tx space to transmit the packet !\n");
		priv->stats.tx_dropped++;
		goto xmit_final;
	}

	// fill the data
#if 0	// mask by Victor Yu. 07-04-2005
	len = skb->len < ETH_ZLEN ? ETH_ZLEN : skb->len;
	len = len > TX_BUF_SIZE ? TX_BUF_SIZE : len;
#else	// add by Victor Yu. 07-04-2005
	len = skb->len > TX_BUF_SIZE ? TX_BUF_SIZE : skb->len;
#endif
#if 0	// mask by Victor Yu. 07-04-2005
	memcpy(txdesc->txdes2.virtTxBufBaseAddr, skb->data, len);
#else	// add by Victor Yu. 07-04-2005
	mywordcopy((void *)txdesc->txdes2.virtTxBufBaseAddr, (void *)skb->data, len);
#endif
	dbg_printk("transmit data pointer = 0x%x\n", (unsigned long)skb->data);
#if 1	// add by Victor Yu. 07-04-2005
	if ( skb->len < ETH_ZLEN ) {
		memset(&txdesc->txdes2.virtTxBufBaseAddr[skb->len], 0, ETH_ZLEN-skb->len);
		len = ETH_ZLEN;
	}
#endif
	txdesc->txdes1.ubit.Lts = 1;
	txdesc->txdes1.ubit.Fts = 1;
	txdesc->txdes1.ubit.Tx2fic = 0;
	txdesc->txdes1.ubit.Txic = 0;
	txdesc->txdes1.ubit.TxBufSize = len;
#if 0	// mask by Victor Yu. 07-04-2005
	txdesc->txdes0.ui = 0;
	txdesc->txdes0.ubit.TxDMAOwn = 1;
#else	// add by Victor Yu. 07-04-2005
	txdesc->txdes0.ui = TXDMA_OWN;
#endif

	outl(0xffffffff, dev->base_addr+TXPD_REG_OFFSET);	// start to send packet
#if 0
	dbg_printk("TxDescNow=%d, address=0x%x, des0=0x%x, des1=0x%x\n", priv->TxDescNow, (unsigned int)&priv->virtTxDescBaseAddr[priv->TxDescNow], txdesc->txdes0.ui, txdesc->txdes1.ui);
	dbg_printk("Buffer phy address=0x%x, virt=0x%x\n", txdesc->txdes2.phyTxBufBaseAddr, (unsigned int)txdesc->txdes2.virtTxBufBaseAddr);
	dbg_printk("TxDescNow-1=%d, address=0x%x, des0=0x%x\n", (priv->TxDescNow-1)&TX_DESC_NUM_MASK, (unsigned int)&priv->virtTxDescBaseAddr[(priv->TxDescNow-1)&TX_DESC_NUM_MASK], priv->virtTxDescBaseAddr[(priv->TxDescNow-1)&TX_DESC_NUM_MASK].txdes0.ui);
#endif
	txnow++;
	txnow &= TX_DESC_NUM_MASK;
	priv->TxDescNow = txnow;
	dev->trans_start = jiffies;
	priv->stats.tx_packets++;
	priv->stats.tx_bytes += len;

xmit_final:
	spin_unlock(&priv->txlock);
	dev_kfree_skb_any(skb);

	return 0;
}

static int mcpu_mac_open(struct net_device *dev)
{
	mcpu_mac_priv_t		*priv=(mcpu_mac_priv_t *)dev->priv;
	unsigned long		flags;

	dbg_printk("mcpu_mac_open test01\n");

	save_flags(flags);
	cli();
#ifdef USE_SCHEDULE_WORK
	spin_lock(&priv->rxlock);
#endif
	spin_lock(&priv->txlock);
	mcpu_mac_reset(dev);
	mcpu_mac_set_mac_address(dev->base_addr+MAC_MADR_REG_OFFSET, dev->dev_addr);
	mcpu_mac_setup_desc_ring(dev);
	mcpu_mac_enable(dev);
	spin_unlock(&priv->txlock);
#ifdef USE_SCHEDULE_WORK
	spin_unlock(&priv->rxlock);
#endif
	restore_flags(flags);
	netif_start_queue(dev);
	dbg_printk("IMR=0x%x, MACCR=0x%x\n", inl(dev->base_addr+IMR_REG_OFFSET), inl(dev->base_addr+MACCR_REG_OFFSET));
#ifdef MCPU_MAC_DEBUG
	{
	unsigned char	macaddr[6];
	int		i;
	mcpu_mac_get_mac_address(dev->base_addr+MAC_MADR_REG_OFFSET, macaddr);
	printk("Get MAC address = ");
	for ( i=0; i<6; i++ )
		printk("%02X ", macaddr[i]);
	printk("\n");
	}
#endif

	return 0;
}

static int mcpu_mac_stop(struct net_device *dev)
{
	mcpu_mac_priv_t		*priv=(mcpu_mac_priv_t *)dev->priv;
	unsigned long		flags;

	dbg_printk("mcpu_mac_stop test01\n");
	netif_stop_queue(dev);
	save_flags(flags);
	cli();
#ifdef USE_SCHEDULE_WORK
	spin_lock(&priv->rxlock);
#endif
	spin_lock(&priv->txlock);
	outl(0, dev->base_addr+IMR_REG_OFFSET);		// disable all interrupt
	outl(0, dev->base_addr+MACCR_REG_OFFSET);	// disable all function
	spin_unlock(&priv->txlock);
#ifdef USE_SCHEDULE_WORK
	spin_unlock(&priv->rxlock);
#endif
	restore_flags(flags);

	return 0;
}

static struct net_device_stats *mcpu_mac_get_stats(struct net_device *dev)
{
	mcpu_mac_priv_t		*priv=(mcpu_mac_priv_t *)dev->priv;

	dbg_printk("mcpu_mac_get_stats test01\n");
#if 0
	{
	unsigned int	base=dev->base_addr;
	int		desc=priv->RxDescNow;
	dbg_printk("RxDescNow=%d, desc phy=0x%x, virt=0x%x, buf phy=0x%x, virt=0x%x\n", desc, priv->phyRxDescBaseAddr+(desc*sizeof(rx_desc_t)), (unsigned int)&priv->virtRxDescBaseAddr[desc], priv->virtRxDescBaseAddr[desc].rxdes2.phyRxBufBaseAddr, (unsigned int)priv->virtRxDescBaseAddr[desc].rxdes2.virtRxBufBaseAddr);
	dbg_printk("Now Rx desc des0=0x%x, des1=0x%x\n", priv->virtRxDescBaseAddr[desc].rxdes0.ui, priv->virtRxDescBaseAddr[desc].rxdes1.ui);
	desc++;
	desc &= RX_DESC_NUM_MASK;
	dbg_printk("Next RxDescNow=%d, desc phy=0x%x, virt=0x%x, buf phy=0x%x, virt=0x%x\n", desc, priv->phyRxDescBaseAddr+(desc*sizeof(rx_desc_t)), (unsigned int)&priv->virtRxDescBaseAddr[desc], priv->virtRxDescBaseAddr[desc].rxdes2.phyRxBufBaseAddr, (unsigned int)priv->virtRxDescBaseAddr[desc].rxdes2.virtRxBufBaseAddr);
	dbg_printk("Next Now Rx desc des0=0x%x, des1=0x%x\n", priv->virtRxDescBaseAddr[desc].rxdes0.ui, priv->virtRxDescBaseAddr[desc].rxdes1.ui);
	printk("TX_MCOL_TX_SCOL register = 0x%x\n", inl(base+TX_MCOL_TX_SCOL_REG_OFFSET));
	printk("RPF_AEP register = 0x%x\n", inl(base+RPF_AEP_REG_OFFSET));
	printk("XM_PG register = 0x%x\n", inl(base+XM_PG_REG_OFFSET));
	printk("RUNT_CNT_TLCC register = 0x%x\n", inl(base+RUNT_CNT_TLCC_REG_OFFSET));
	printk("CRCER_CNT_FTL_CNT register = 0x%x\n", inl(base+CRCER_CNT_FTL_CNT_REG_OFFSET));
	printk("RLC_RCC register = 0x%x\n", inl(base+RLC_RCC_REG_OFFSET));
	printk("BROC register = 0x%x\n", inl(base+BROC_REG_OFFSET));
	printk("MUCLA register = 0x%x\n", inl(base+MULCA_REG_OFFSET));
	printk("RP register = 0x%x\n", inl(base+RP_REG_OFFSET));
	printk("XP register = 0x%x\n", inl(base+XP_REG_OFFSET));
	}
#endif

	return &priv->stats;
}


static irqreturn_t mcpu_mac_interrupt(int irq, void *dev_id, struct pt_regs *regs)
{
	struct net_device	*dev=(struct net_device *)dev_id;
	unsigned int		ists;
#ifdef USE_SCHEDULE_WORK
	mcpu_mac_priv_t		*priv=(mcpu_mac_priv_t *)dev->priv;
#endif

	
	//dbg_printk("mcpu_mac_interrupt test01\n");
	ists = inl(dev->base_addr+ISR_REG_OFFSET);
	if ( ists & RPKT_FINISH ) {
#ifdef USE_SCHEDULE_WORK
		schedule_work(&priv->rqueue);
#else
		mcpu_mac_recv((void *)dev);
#endif
	} else {
#ifdef MCPU_MAC_DEBUG
		if ( ists & NORXBUF ) {
			printk("Receiver no Rx buffer interrupt\n");
			outl(inl(dev->base_addr+IMR_REG_OFFSET)&~NORXBUF_M, dev->base_addr+IMR_REG_OFFSET);
			//return IRQ_HANDLED;
		}
		if ( ists & AHB_ERR ) {
			printk("Receiver AHB error interrupt.\n");
			//return IRQ_HANDLED;
		}
#endif
		//return IRQ_NONE;
	}

	return IRQ_HANDLED;
}

static int mcpu_mac_init(struct net_device *dev)
{
	mcpu_mac_priv_t		*priv=(mcpu_mac_priv_t *)dev->priv;

	dbg_printk("mcpu_mac_init test01\n");

	// first initialize the private variable to zero 
	memset((void *)priv, 0, sizeof(mcpu_mac_priv_t));
	spin_lock_init(&priv->txlock);

#ifdef USE_SCHEDULE_WORK

//#if 1	// add by Victor Yu. 07-04-2005
	spin_lock_init(&priv->rxlock);
	INIT_WORK(&priv->rqueue, &mcpu_mac_recv, (void *)dev);
#endif

	// allocate the descriptor and buffer memory
	priv->virtTxDescBaseAddr = (tx_desc_t *)dma_alloc_coherent(NULL, sizeof(tx_desc_t)*TX_DESC_NUM, (dma_addr_t *)&priv->phyTxDescBaseAddr, GFP_DMA|GFP_KERNEL);	
	if ( priv->virtTxDescBaseAddr == NULL || (priv->phyTxDescBaseAddr & 0x0f) ) {
		dbg_printk("Allocate the Tx descriptor memory fail !\n");
		goto init_fail;
	}
	priv->virtRxDescBaseAddr = (rx_desc_t *)dma_alloc_coherent(NULL, sizeof(rx_desc_t)*RX_DESC_NUM, (dma_addr_t *)&priv->phyRxDescBaseAddr, GFP_DMA|GFP_KERNEL);
	if ( priv->virtRxDescBaseAddr == NULL || (priv->phyRxDescBaseAddr & 0x0f) ) {
		dbg_printk("Allocate the Rx descriptor memory fail !\n");
		goto init_fail;
	}
	priv->virtTxBufBaseAddr = (unsigned char *)dma_alloc_coherent(NULL, TX_BUF_SIZE*TX_DESC_NUM, (dma_addr_t *)&priv->phyTxBufBaseAddr, GFP_DMA|GFP_KERNEL);
	if ( priv->virtTxBufBaseAddr == NULL || (priv->phyTxBufBaseAddr & 0x03) ) {
		dbg_printk("Allocate the Tx buffer memory fail !\n");
		goto init_fail;
	}
	priv->virtRxBufBaseAddr = (unsigned char *)dma_alloc_coherent(NULL, RX_BUF_SIZE*RX_DESC_NUM, (dma_addr_t *)&priv->phyRxBufBaseAddr, GFP_DMA|GFP_KERNEL);
	if ( priv->virtRxBufBaseAddr == NULL || (priv->phyRxBufBaseAddr & 0x03) ) {
		dbg_printk("Allocate the Rx buffer memory fail !\n");
		goto init_fail;
	}

	// setup the thernet basic
	ether_setup(dev);

	// reset the MAC
	mcpu_mac_reset(dev);
	mcpu_mac_setup_desc_ring(dev);

	// we need to get the MAC address from the hardware and set to the device
#ifdef CONFIG_ARCH_CPE
dev->dev_addr[0] = 0x00;
dev->dev_addr[1] = 0x90;
dev->dev_addr[2] = 0xE8;
dev->dev_addr[3] = 0x72;
dev->dev_addr[4] = 0x20;
dev->dev_addr[5] = 0x01;
#endif	// CONFIG_ARCH_CPE
#ifdef CONFIG_ARCH_MOXACPU
{
#include	<linux/mtd/mtd.h>
	struct mtd_info	*mtd;
	int		len;
	mtd = get_mtd_device(NULL, 0);
	if ( priv == (void *)&mcpu_mac_priv ) {	// LAN 1
		if ( mtd == NULL ) {
#if 0	// mask by Victor Yu. 03-21-2006
			dev->dev_addr[0] = 0x00;
			dev->dev_addr[1] = 0x90; 
			dev->dev_addr[2] = 0xe8; 
			dev->dev_addr[3] = 0x10; 
			dev->dev_addr[4] = 0x02; 
			dev->dev_addr[5] = 0x40; 
#else	// add by Victor Yu. 021-2006
#if ( defined CONFIG_ARCH_W341 ) || ( defined CONFIG_ARCH_W345 ) || ( defined CONFIG_ARCH_W345_IMP1 ) || ( defined CONFIG_ARCH_UC_7112_LX_PLUS ) || ( defined CONFIG_ARCH_W311 ) ||   ( defined CONFIG_ARCH_W321 ) ||  ( defined CONFIG_ARCH_W315 ) ||   ( defined CONFIG_ARCH_W325 ) 	 
			memcpy(dev->dev_addr, (char *)(CPE_FLASH_VA_BASE+0x50), 6);
#elif ( defined CONFIG_ARCH_IA240 ) || ( defined CONFIG_ARCH_IA241 )
			memcpy(dev->dev_addr, (char *)(CPE_FLASH_VA_BASE+0x56), 6);
	
#endif
			
#endif
		} else {
#if ( defined CONFIG_ARCH_W341 ) || ( defined CONFIG_ARCH_W345 ) || ( defined CONFIG_ARCH_W345_IMP1 ) || ( defined CONFIG_ARCH_UC_7112_LX_PLUS ) || ( defined CONFIG_ARCH_W311 ) ||   ( defined CONFIG_ARCH_W321 ) ||  ( defined CONFIG_ARCH_W315 ) ||   ( defined CONFIG_ARCH_W325 ) 	 
 	 	 
			 mtd->read(mtd, 0x50, 6, &len, dev->dev_addr);
#elif ( defined CONFIG_ARCH_IA240 ) || ( defined CONFIG_ARCH_IA241 )
			 mtd->read(mtd, 0x56, 6, &len, dev->dev_addr);

#endif

		}
	} else {	// LAN2
		if ( mtd == NULL ) {
#if 0	// mask by Victor Yu. 03-21-2006
			dev->dev_addr[0] = 0x00;
			dev->dev_addr[1] = 0x90; 
			dev->dev_addr[2] = 0xe8; 
			dev->dev_addr[3] = 0x10; 
			dev->dev_addr[4] = 0x02; 
			dev->dev_addr[5] = 0x41; 
#else	// add by Victor Yu. 03-21-2006

#if ( defined CONFIG_ARCH_UC_7112_LX_PLUS )  	 
			memcpy(dev->dev_addr, (char *)(CPE_FLASH_VA_BASE+0x56), 6);
#elif ( defined CONFIG_ARCH_IA240 ) || ( defined CONFIG_ARCH_IA241 )
			memcpy(dev->dev_addr, (char *)(CPE_FLASH_VA_BASE+0x50), 6);
#endif

#endif

		} else {
#if ( defined CONFIG_ARCH_UC_7112_LX_PLUS )  	 
	 		mtd->read(mtd, 0x56, 6, &len, dev->dev_addr);
#elif ( defined CONFIG_ARCH_IA240 ) || ( defined CONFIG_ARCH_IA241 )
			mtd->read(mtd, 0x50, 6, &len, dev->dev_addr);
#endif
		}
	}
}
#endif	// CONFIG_ARCH_MOXACPU

	// setup the low lever interrupt for Moxa CPU
#include <asm/arch/cpe_int.h>
	cpe_int_set_irq(dev->irq, LEVEL, H_ACTIVE);
	if ( request_irq(dev->irq, &mcpu_mac_interrupt, SA_INTERRUPT, dev->name, dev) ) {
		dbg_printk("Request interrupt service fail !\n");
		goto init_fail;
	}

	return 0;

init_fail:
	mcpu_mac_free_memory(dev);
	return -ENOMEM;
}

#if 1  // Add by Jared 12-04-2008

#include <linux/ethtool.h>
#include <linux/mii.h>

#define	PHY_CNTL_REG 	0x00
#define	PHY_STATUS_REG 	0x01
#define	PHY_ID_REG1    	0x02
#define	PHY_ID_REG2    	0x03
#define	PHY_ANA_REG    	0x04
#define	PHY_ANLPAR_REG 	0x05
#define	PHY_ANE_REG    	0x06
#define	PHY_ECNTL_REG1 	0x10
#define	PHY_QPDS_REG   	0x11
#define	PHY_10BOP_REG  	0x12
#define	PHY_ECNTL_REG2 	0x13
/* PHY Status register */
#define AN_COMPLETE	0x0020
#define Link_Status	0x0004
//FTMAC100 register definition => 90/94: PHY register
#define FTMAC100_REG_PHY_WRITE	0x08000000
#define FTMAC100_REG_PHY_READ	0x04000000

static DECLARE_MUTEX(miiAccessMutex);

int flib_FTMAC100_PHY_Read_Waiting(struct net_device *dev) {
	u32 wResult;
	unsigned long t;

	for(t = jiffies;;) {
		if((jiffies - t) > (HZ/100) ) {
			printk("<1>%s[%d]timeout\n",__FUNCTION__,__LINE__);
			return (-1);
		}
		wResult = inl(dev->base_addr+PHYCR_REG_OFFSET);
		if( (wResult & FTMAC100_REG_PHY_READ) == 0 )
			break;
	}
	return 0;
}

int flib_FTMAC100_PHY_Write_Waiting(struct net_device *dev) {
	u32 wResult;
	unsigned long t;

	for(t = jiffies;;) {
		if((jiffies - t) > (HZ/100) ) {
			printk("<1>%s[%d]\n",__FUNCTION__,__LINE__);
			return (-1);
		}
		wResult = inl(dev->base_addr+PHYCR_REG_OFFSET);
		if( (wResult & FTMAC100_REG_PHY_WRITE) == 0 )
			break;
	}
	return 0;
}

u32 PhyMiiReadRtn (struct net_device *dev, u8 phyAddr, u8 phyReg, u16 *value) {
	u32 dwTemp;

	dwTemp= (phyAddr<<16) | ((phyReg & 0x1f) << 21) | FTMAC100_REG_PHY_READ;
	outl(dwTemp, dev->base_addr + PHYCR_REG_OFFSET);

	if(flib_FTMAC100_PHY_Read_Waiting(dev) != 0){
		printk("<1>%s[%d]\n",__FUNCTION__,__LINE__);
		return (-1);
	}

	*value = inw(dev->base_addr + PHYCR_REG_OFFSET);

	return 0;
}

u32 PhyMiiWriteRtn (struct net_device *dev,  u8 phyAddr, u8 phyReg, u16 value) {
	u32 dwTemp, miiTimeout;

	dwTemp = (phyAddr<<16) | ((phyReg & 0x1f) << 21) | FTMAC100_REG_PHY_WRITE;
	outl(value, dev->base_addr + PHYWDATA_REG_OFFSET);
	udelay(10) ;

	if(flib_FTMAC100_PHY_Write_Waiting(dev) != 0){
		printk("<1>%s[%d]\n",__FUNCTION__,__LINE__);
		return (-1);
	}

	for(miiTimeout=10; miiTimeout>0; miiTimeout-- ) {
		set_current_state(TASK_INTERRUPTIBLE);
		schedule_timeout( (1*HZ)/1000 );
	}

	outl(dwTemp, dev->base_addr + PHYCR_REG_OFFSET);

	if(flib_FTMAC100_PHY_Read_Waiting(dev) != 0){
		printk("<1>%s[%d]\n",__FUNCTION__,__LINE__);
		return (-2);
	}

	for(miiTimeout=10; miiTimeout>0; miiTimeout-- ) {
		set_current_state(TASK_INTERRUPTIBLE);
		schedule_timeout( (1*HZ)/1000 );
	}

	return 0;
}

#include <linux/ethtool.h>
static int netdev_ethtool_ioctl(struct net_device *dev, void *useraddr)
{
    u32 dwTemp;
    int ethcmd, res = 0;

    if (copy_from_user (&ethcmd, useraddr, sizeof (ethcmd)))
      return -EFAULT;

    switch (ethcmd) {
      case ETHTOOL_GLINK: {
        struct ethtool_value edata = {ETHTOOL_GLINK};
	
	dwTemp= (1<<16) | ((PHY_STATUS_REG & 0x1f) << 21) | FTMAC100_REG_PHY_READ;
	outl(dwTemp, dev->base_addr + PHYCR_REG_OFFSET);
	if(flib_FTMAC100_PHY_Read_Waiting(dev) != 0){
		res = -1;
	}
	dwTemp = inw(dev->base_addr + PHYCR_REG_OFFSET);
#if 0
	printk("%s[%d]ETHTOOL_GLINK 0x%x-",__FUNCTION__,__LINE__, dwTemp) ;

	dwTemp= (1<<16) | ((PHY_STATUS_REG & 0x1f) << 21) | FTMAC100_REG_PHY_READ;
	outl(dwTemp, dev->base_addr + PHYCR_REG_OFFSET);
	if(flib_FTMAC100_PHY_Read_Waiting(dev) != 0){
		res = -1;
	}
	dwTemp = inw(dev->base_addr + PHYCR_REG_OFFSET);
	printk("%s[%d]ETHTOOL_GLINK 0x%x-",__FUNCTION__,__LINE__, dwTemp) ;
#endif
	if ( (dwTemp & Link_Status) == Link_Status)
		edata.data= 1;
	else
		edata.data= 0;

	if (copy_to_user( useraddr, &edata, sizeof(edata)))
	  res= -EFAULT;

	return res;
      }

      default:
        return -EOPNOTSUPP;
    }
}

static int mcpu_do_dev_ioctl(struct net_device *dev, struct ifreq *req, int cmd)
{
	struct mii_ioctl_data *data = (struct mii_ioctl_data *) & req->ifr_data;
	int res=0;

	switch (cmd)
	{
		/* Get address of MII PHY in use */
		case SIOCGMIIPHY:
		case SIOCDEVPRIVATE:
#ifdef	ONE_MII_Interface
			if(port){
				data->phy_id = 0x2;
			}else{
				data->phy_id = 0x10;
			}
#else
			data->phy_id = 1;
#endif
			break;
		/* Read MII PHY register */
		case SIOCGMIIREG:		
		case SIOCDEVPRIVATE+1:
			down (&miiAccessMutex);     /* lock the MII register access mutex */
			if ((res = PhyMiiReadRtn (dev, data->phy_id, data->reg_num, &data->val_out)))
	    {
				printk("Error reading MII reg %d on phy %d\n",
					data->reg_num, data->phy_id);
				res = -1;
			}
			up (&miiAccessMutex);	/* release the MII register access mutex */
			break;
		/* Write MII PHY register */
		case SIOCSMIIREG:
		case SIOCDEVPRIVATE+2:
			down (&miiAccessMutex);     /* lock the MII register access mutex */
			if ((res = PhyMiiWriteRtn (dev, data->phy_id, data->reg_num, data->val_in)))
	    {
				printk("Error writing MII reg %d on phy %d\n",
		                        data->reg_num, data->phy_id);
				res = -1;
			}
			up (&miiAccessMutex);	/* release the MII register access mutex */
			break;
		case SIOCETHTOOL:
			return netdev_ethtool_ioctl(dev, req->ifr_data);
		default:
			res = -EOPNOTSUPP;
	}

	return res;
}

#endif // end of add by Jared 12-04-2008

static struct net_device	mcpu_mac_dev = {
#ifdef CONFIG_ARCH_MOXACPU
	
#if ( defined CONFIG_ARCH_W341 ) || ( defined CONFIG_ARCH_W345 ) || ( defined CONFIG_ARCH_W345_IMP1 ) || ( defined CONFIG_ARCH_UC_7112_LX_PLUS ) || ( defined CONFIG_ARCH_W311 ) ||   ( defined CONFIG_ARCH_W321 ) ||  ( defined CONFIG_ARCH_W315 ) ||   ( defined CONFIG_ARCH_W325 ) 	 
	.name		= "eth0",
#elif ( defined CONFIG_ARCH_IA240 ) || ( defined CONFIG_ARCH_IA241 )
 	.name		= "eth1",
#endif

#endif	
	.base_addr	= IO_ADDRESS(CPE_FTMAC_BASE),
	.irq		= IRQ_MAC,
	.init		= &mcpu_mac_init,
	.get_stats	= &mcpu_mac_get_stats,
	.open		= &mcpu_mac_open,
	.stop		= &mcpu_mac_stop,
	.hard_start_xmit= &mcpu_mac_hard_start_xmit,
	.priv		= (void *)&mcpu_mac_priv,
	.tx_timeout	= &mcpu_mac_tx_timeout,
#ifdef HAVE_MULTICAST
	.set_multicast_list = &mcpu_mac_set_multicast_list,
#endif
#if 1  // Add by Jared 12-04-2008
	.do_ioctl	= mcpu_do_dev_ioctl,
#endif // end of add by Jared 12-04-2008
};

#ifdef CONFIG_ARCH_MOXACPU


#if ( defined CONFIG_ARCH_IA240 ) || ( defined CONFIG_ARCH_IA241 ) || ( defined CONFIG_ARCH_UC_7112_LX_PLUS ) 
static struct net_device	mcpu_mac_dev2 = {
#if ( defined CONFIG_ARCH_IA240 ) || ( defined CONFIG_ARCH_IA241 )
	.name		= "eth0",
#elif ( defined CONFIG_ARCH_UC_7112_LX_PLUS )
	.name		= "eth1",
#endif
	.base_addr	= IO_ADDRESS(CPE_FTMAC2_BASE),
	.irq		= IRQ_MAC2,
	.init		= &mcpu_mac_init,
	.get_stats	= &mcpu_mac_get_stats,
	.open		= &mcpu_mac_open,
	.stop		= &mcpu_mac_stop,
	.hard_start_xmit= &mcpu_mac_hard_start_xmit,
	.priv		= (void *)&mcpu_mac_priv2,
	.tx_timeout	= &mcpu_mac_tx_timeout,
#ifdef HAVE_MULTICAST
	.set_multicast_list = &mcpu_mac_set_multicast_list,
#endif
#if 1  // Add by Jared 12-04-2008
	.do_ioctl	= mcpu_do_dev_ioctl,
#endif // end of add by Jared 12-04-2008
};
#endif

#endif	// CONFIG_ARCH_MOXACPU

static int __init mcpu_mac_init_module(void)
{
	int	ret;
	printk("Moxa CPU Ethernet Device Driver Version 1.0 load ");
	ret = register_netdev(&mcpu_mac_dev);
	if ( ret ) {
		printk("fail !\n");
		return ret;
	}

#ifdef CONFIG_ARCH_MOXACPU


#if ( defined CONFIG_ARCH_IA240 ) || ( defined CONFIG_ARCH_IA241 ) || ( defined CONFIG_ARCH_UC_7112_LX_PLUS ) 	 	 

	ret = register_netdev(&mcpu_mac_dev2);
	if ( ret ) {
		mcpu_mac_free_memory(&mcpu_mac_dev);
		free_irq(mcpu_mac_dev.irq, &mcpu_mac_dev);
		unregister_netdev(&mcpu_mac_dev);
		printk("fail !\n");
		return ret;
	}
#endif


#endif	// CONFIG_ARCH_MOXACPU

	printk("OK.\n");

	return 0;
}

static void __exit mcpu_mac_cleanup_module(void)
{
	printk("Moxa CPU Ethernet Device Driver unload.\n");
	mcpu_mac_free_memory(&mcpu_mac_dev);
	free_irq(mcpu_mac_dev.irq, &mcpu_mac_dev);
	unregister_netdev(&mcpu_mac_dev);

#ifdef CONFIG_ARCH_MOXACPU

#if ( defined CONFIG_ARCH_IA240 ) || ( defined CONFIG_ARCH_IA241 )|| ( defined CONFIG_ARCH_UC_7112_LX_PLUS )  	 
	mcpu_mac_free_memory(&mcpu_mac_dev2);
	free_irq(mcpu_mac_dev2.irq, &mcpu_mac_dev2);
	unregister_netdev(&mcpu_mac_dev2);
#endif

#endif	// CONFIG_ARCH_MOXACPU
}

module_init(mcpu_mac_init_module);
module_exit(mcpu_mac_cleanup_module);
