/*
 ***************************************************************************
 * Ralink Tech Inc.
 * 4F, No. 2 Technology 5th Rd.
 * Science-based Industrial Park
 * Hsin-chu, Taiwan, R.O.C.
 *
 * (c) Copyright 2002, Ralink Technology, Inc.
 *
 * All rights reserved. Ralink's source code is an unpublished work and the
 * use of a copyright notice does not imply otherwise. This source code
 * contains confidential trade secret material of Ralink Tech. Any attemp
 * or participation in deciphering, decoding, reverse engineering or in any
 * way altering the source code is stricitly prohibited, unless the prior
 * written consent of Ralink Technology, Inc. is obtained.
 ***************************************************************************

    Module Name:
    countryform.h

    Abstract:
        Implement Country Region Select Dialog.

    Revision History:
    Who            When          What
    --------    ----------      ----------------------------------------------
    Paul Wu     01-22-2003      created

*/


#ifndef _COUNTRYFROM_H
#define _COUNTRYFROM_H

#include <qvariant.h>
#include <qdialog.h>
class QVBoxLayout; 
class QHBoxLayout; 
class QGridLayout; 
class QButtonGroup;
class QLabel;
class QPushButton;
class QRadioButton;

class CountryForm : public QDialog
{ 
    Q_OBJECT

public:
    CountryForm( QWidget* parent = 0, const char* name = 0, bool modal = FALSE, WFlags fl = 0 );
    ~CountryForm();

    QLabel* InfoTextLabel;
    QPushButton* OkPushButton;
    QPushButton* CancelPushButton;
    QButtonGroup* SelectButtonGroup;
    QRadioButton* ISRAELRadioButton;
    QRadioButton* ICRadioButton;
    QRadioButton* ETSIRadioButton;
    QRadioButton* MKKRadioButton;
    QRadioButton* MKK1RadioButton;
    QRadioButton* FRANCERadioButton;
    QRadioButton* SPAINRadioButton;
    QRadioButton* FCCRadioButton;

    int GetRegionID();
};

#endif //_COUNTRYFROM_H

