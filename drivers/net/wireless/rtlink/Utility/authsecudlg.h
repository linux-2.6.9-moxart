#ifndef AUTHSECUDLG_H
#define AUTHSECUDLG_H

#include <qvariant.h>
#include <qdialog.h>
#include "rt_tool.h"
#include "qhexvalidator.h"

class QVBoxLayout; 
class QHBoxLayout; 
class QGridLayout; 
class QButtonGroup;
class QComboBox;
class QLabel;
class QLineEdit;
class QPushButton;
class QRadioButton;
class QHexValidator;

class AuthSecuDlg : public QDialog
{ 
    Q_OBJECT

public:
    AuthSecuDlg( QWidget* parent = 0, const char* name = 0, bool modal = FALSE, WFlags fl = 0 );
    ~AuthSecuDlg();

    QLabel*                     Security_TextLabel1;
    QLabel*                     Security_TextLabel2;
    QLabel*                     Security_TextLabel3;
    QComboBox*                  Security_ComboBox_AuthType;
    QComboBox*                  Security_ComboBox_Encrypt;
    QLineEdit*                  Security_LineEdit_PSK;
    QButtonGroup*               Security_ButtonGroup_Key;
    QRadioButton*               Security_RadioButton_Key1;
    QRadioButton*               Security_RadioButton_Key2;
    QRadioButton*               Security_RadioButton_Key3;
    QRadioButton*               Security_RadioButton_Key4;
    QComboBox*                  Security_ComboBox_KeyType1;
    QComboBox*                  Security_ComboBox_KeyType2;
    QComboBox*                  Security_ComboBox_KeyType3;
    QComboBox*                  Security_ComboBox_KeyType4;
    QLineEdit*                  Security_LineEdit_Key1Hex;
    QLineEdit*                  Security_LineEdit_Key1Ascii;
    QLineEdit*                  Security_LineEdit_Key2Hex;
    QLineEdit*                  Security_LineEdit_Key2Ascii;
    QLineEdit*                  Security_LineEdit_Key3Hex;
    QLineEdit*                  Security_LineEdit_Key3Ascii;
    QLineEdit*                  Security_LineEdit_Key4Hex;
    QLineEdit*                  Security_LineEdit_Key4Ascii;
    QPushButton*                PushButton_OK;
    QPushButton*                PushButton_Cancel;

    QHexValidator*              m_hexValidator;
    bool                        m_isClickOk;

    bool Security_IsClickOk();
    int Security_GetWepKeyType(int index);
    QString Security_GetWepKeyString(int index);
    QString Security_GetPSKString();
    NDIS_802_11_AUTHENTICATION_MODE Security_GetAuthticationMode();
    NDIS_802_11_ENCRYPTION_STATUS Security_GetEncryptType();
    void Security_SetAuthModeAndEncryType(NDIS_802_11_AUTHENTICATION_MODE mode, NDIS_802_11_ENCRYPTION_STATUS type);
    int Security_GetDefaultKeyId();
    void Security_SetDefaultKeyId(int keyId);
    void Security_SetKeyTypeAndKeyString(int keyIndex, int keyType, char *keyString);

public slots:
    virtual void Do_NotThing() { };
    virtual void OnOK();
    virtual void OnCancel();
    virtual void Security_OnSelectAuthenType(int id);
    virtual void Security_OnSelectEncryptType(int id);
    virtual void Security_OnSelectKey1Type(int id);
    virtual void Security_OnSelectKey2Type(int id);
    virtual void Security_OnSelectKey3Type(int id);
    virtual void Security_OnSelectKey4Type(int id);
};

#endif // AUTHSECUDLG_H
