#ifndef RACONFIGUI_H
#define RACONFIGUI_H

#include <qvariant.h>
#include <qwidget.h>
#include "rt_tool.h"

class QVBoxLayout; 
class QHBoxLayout; 
class QGridLayout; 
class QButtonGroup;
class QCheckBox;
class QComboBox;
class QLabel;
class QLineEdit;
class QListView;
class QListViewItem;
class QProgressBar;
class QPushButton;
class QTabWidget;
class QWidget;

class RaConfigForm : public QWidget
{ 
    Q_OBJECT

public:
    RaConfigForm(  int socket_id, const char *device_name, QWidget* parent = 0, const char* name = 0);
    ~RaConfigForm();

    QTabWidget*             TabWidgetRaConfig;
//Profile Page
    QWidget*                ProfilePage_tab;
    QButtonGroup*           Profile_ButtonGroup;
    QPushButton*            Profile_PushButton_Add;
    QPushButton*            Profile_PushButton_Delete;
    QPushButton*            Profile_PushButton_Edit;
    QPushButton*            Profile_PushButton_Activate;
    QListView*              Profile_ListView;
//Link Status Page
    QWidget*                LinkStatusPage_tab;
    QLabel*                 LinkStatus_TextLabel1;
    QLabel*                 LinkStatus_TextLabel2;
    QLabel*                 LinkStatus_TextLabel3;
    QLabel*                 LinkStatus_TextLabel4;
    QLabel*                 LinkStatus_TextLabel5;
    QLabel*                 LinkStatus_TextLabel6;
    QLabel*                 LinkStatus_TextLabel7;
    QLabel*                 LinkStatus_TextLabel8;
    QLabel*                 LinkStatus_TextLabel_Status;
    QLabel*                 LinkStatus_TextLabel_Channel;
    QLabel*                 LinkStatus_TextLabel_TxRate;
    QLabel*                 LinkStatus_TextLabel_TxThrougput;
    QLabel*                 LinkStatus_TextLabel_RxThroughput;
    QLabel*                 LinkStatus_TextLabel_Link;
    QProgressBar*           LinkStatus_ProgressBar_Link;
    QLabel*                 LinkStatus_TextLabel_Signal;
    QCheckBox*              LinkStatus_CheckBox_dbm;
    QProgressBar*           LinkStatus_ProgressBar_Signal;
//Site Survey Page    
    QWidget*                SiteSurveyPage_tab;
    QButtonGroup*           SiteSurvey_ButtonGroup;
    QListView*              SiteSurvey_ListView;
    QLineEdit*              SiteSurvey_LineEdit_Status;
    QPushButton*            SiteSurvey_PushButton_Rescan;
    QPushButton*            SiteSurvey_PushButton_Connect;
    QPushButton*            SiteSurvey_PushButton_AddProfile;    
//Statistics Page
    QWidget*                StatisticsPage_tab;
    QButtonGroup*           Statistics_ButtonGroup_Tx;
    QLabel*                 Statistics_TextLabel1;
    QLabel*                 Statistics_TextLabel2;
    QLabel*                 Statistics_TextLabel3;
    QLabel*                 Statistics_TextLabel4;
    QLabel*                 Statistics_TextLabel5;    
    QLabel*                 Statistics_TextLabel6;
    QLabel*                 Statistics_TextLabel7;
    QLabel*                 Statistics_TextLabel8;
    QLabel*                 Statistics_TextLabel9;
    QLabel*                 Statistics_TextLabel10;
    QLabel*                 Statistics_TextLabel11;
    QLabel*                 Statistics_TextLabel12;
    QLabel*                 Statistics_TextLabel_TxSuccess;
    QLabel*                 Statistics_TextLabel_TxWithoutRetry;
    QLabel*                 Statistics_TextLabel_TxAfterRetry;
    QLabel*                 Statistics_TextLabel_TxFailACK;
    QLabel*                 Statistics_TextLabel_RTSSuccess;
    QLabel*                 Statistics_TextLabel_RTSFail;
    QButtonGroup*           Statistics_ButtonGroup_Rx;
    QLabel*                 Statistics_TextLabel13;
    QLabel*                 Statistics_TextLabel14;
    QLabel*                 Statistics_TextLabel15;
    QLabel*                 Statistics_TextLabel16;
    QLabel*                 Statistics_TextLabel17;
    QLabel*                 Statistics_TextLabel18;
    QLabel*                 Statistics_TextLabel19;
    QLabel*                 Statistics_TextLabel20;
    QLabel*                 Statistics_TextLabel_RxSuccess;
    QLabel*                 Statistics_TextLabel_RxCRC;
    QLabel*                 Statistics_TextLabel_RxDrop;
    QLabel*                 Statistics_TextLabel_Duplicate;
    QPushButton*            Statistics_PushButton_Reset;    
//Advance Page
    QWidget*                AdvancePage_tab;
    QLabel*                 Advance_TextLabel1;
    QLabel*                 Advance_TextLabel2;
    QLabel*                 Advance_TextLabel3;
    QComboBox*              Advance_ComboBox_Mode;
    QCheckBox*              Advance_CheckBox_AdhocOfdm;
    QCheckBox*              Advance_CheckBox_TxBurst;
    QCheckBox*              Advance_CheckBox_ShortSlot;
    QCheckBox*              Advance_CheckBox_TurboRate;
    QComboBox*              Advance_ComboBox_BGProtection;
    QComboBox*              Advance_ComboBox_TxRate;
    QLabel*                 Advance_TextLabel_Radio;
    QPushButton*            Advance_PushButton_Radio;
    QPushButton*            Advance_PushButton_Apply;    
//About Page        
    QWidget*                AboutPage_tab;
    QLabel*                 About_TextLabel_Logo;
    QButtonGroup*           About_ButtonGroupUI;
    QLabel*                 About_TextLabel1;
    QLabel*                 About_TextLabel_UIVersion;
    QLabel*                 About_TextLabel2;
    QLabel*                 About_TextLabel_UIDate;
    QButtonGroup*           About_ButtonGroupNIC;
    QLabel*                 About_TextLabel3;
    QLabel*                 About_TextLabel_NICVersion;
    QLabel*                 About_TextLabel4;
    QLabel*                 About_TextLabel_NICDate;
    QButtonGroup*           About_ButtonGroupMAC;
    QLabel*                 About_TextLabel5;
    QLabel*                 About_TextLabel_PhyAddress;

    bool                    m_bRescan;
    bool                    m_bSetBssid;
    bool                    m_bSetSsid;
    bool                    m_bTXBurst;
    bool                    m_bAdhocOfdm;
    bool                    m_bShortSlot;
    bool                    m_bTurboRate;
    bool                    m_bUpdateSiteSurveyPage;
    bool                    m_bUpdateProfilePage;
    bool                    m_bUpdateLinkSatusPage;

    int                     m_nSocketId;    
    int                     m_nTimerCount;
    int                     m_nBGProtection;
    int                     m_nTXRate;
    int                     m_nWirelessMode;
    
    char                    *m_strDeviceName;
    unsigned int            m_nSigQua;
   	unsigned long           m_lTxCount;
	unsigned long           m_lRxCount;
    unsigned long           m_lChannelQuality;
    
    int                     timerId_Alive;
    int                     timerId_ConnectStatus;
    int                     timerId_LinkStatus;
    int                     timerId_Statistic;
    int                     timerId_UpdateProfileStatus;

    char                    m_strActiveProfileID[32+1];

    PNDIS_802_11_BSSID_LIST_EX      m_pBssidList;
    PRT_PROFILE_SETTING             m_pProfileSetting;
    NDIS_802_11_SSID                m_SsidSet;
    NDIS_802_11_MAC_ADDRESS         m_BssidSet;
    RT_802_11_PHY_MODE              m_WirelessMode;

    void Profile_OnActive();
    void Profile_ReadProfileFromFile();
    void Profile_UpdateProfileStatus();
    void Profile_WriteProfileToFile();
    void Profile_WriteConfigToRegistryAndSetOid();
    void LinkStatus_OnActive();
    void LinkStatus_UpdateStatus();
    void SiteSurvey_OnActive();
    void SiteSurvey_ProbeConnectStatus();
    void SiteSurvey_RescanTimerFunc();
    void SiteSurvey_UpdateConnectStatus(NDIS_MEDIA_STATE ConnectStatus);
    void SiteSurvey_ButtonShowHide();
    NDIS_802_11_AUTHENTICATION_MODE SiteSurvey_GetEncryStatus();
    bool SiteSurvey_ResetOidValue(NDIS_802_11_NETWORK_INFRASTRUCTURE enumInfrastructureMode = Ndis802_11Infrastructure);
    void Statistics_OnActive();
    void Statistics_OnTimer();
    void Advance_OnActive();
    void Advance_ReadConfigFromRegistry();
    void About_OnActive();

public slots:	
    virtual void Do_NotThing() { };
    virtual void OnPageChanged();
    virtual void timerEvent(QTimerEvent* e);
    virtual void Profile_OnAddProfile();
    virtual void Profile_OnDeleteProfile();
    virtual void Profile_OnEditProfile();
    virtual void Profile_OnActiveProfile();
    virtual void SiteSurvey_OnRescan();
    virtual void SiteSurvey_OnConnect();
    virtual void SiteSurvey_OnAddToProfile();
    virtual void Statistics_OnResetCounters();
    virtual void SiteSurvey_OnSelectChange();
    virtual void Advance_OnRadio();
    virtual void Advance_OnApply();
    virtual void Advance_OnSelchangeWirelessMode(int index);


};

#endif // RACONFIGUI_H

