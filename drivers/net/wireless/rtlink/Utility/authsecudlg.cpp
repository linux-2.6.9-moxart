#include "authsecudlg.h"

#include <qvariant.h>
#include <qbuttongroup.h>
#include <qcombobox.h>
#include <qlabel.h>
#include <qlineedit.h>
#include <qpushbutton.h>
#include <qradiobutton.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>
#include <qmessagebox.h>

/* 
 *  Constructs a Security which is a child of 'parent', with the 
 *  name 'name' and widget flags set to 'f'.
 *
 *  The dialog will by default be modeless, unless you set 'modal' to
 *  TRUE to construct a modal dialog.
 */
AuthSecuDlg::AuthSecuDlg( QWidget* parent,  const char* name, bool modal, WFlags fl )
    : QDialog( parent, name, modal, fl )
{
    if ( !name )
        setName( "AuthSecuDlg" );
    resize( 580, 325 ); 
    setMinimumSize( 580, 325 );
    setMaximumSize( 580, 325 );
    setCaption("Authentication & AuthSecuDlg");

    Security_TextLabel1 = new QLabel( this, "Security_TextLabel1" );
    Security_TextLabel1->setGeometry( QRect( 20, 14, 120, 30 ) ); 
    Security_TextLabel1->setText("Authentication Type:");

    Security_ComboBox_AuthType = new QComboBox( FALSE, this, "Security_ComboBox_AuthType" );
    Security_ComboBox_AuthType->insertItem("OPEN");
    Security_ComboBox_AuthType->insertItem("SHARED");
    Security_ComboBox_AuthType->setGeometry( QRect( 150, 14, 120, 30 ) ); 

    Security_TextLabel2 = new QLabel( this, "Security_TextLabel2" );
    Security_TextLabel2->setGeometry( QRect( 313, 14, 120, 30 ) ); 
    Security_TextLabel2->setText("Encryptiion Type:");

    Security_ComboBox_Encrypt = new QComboBox( FALSE, this, "Security_ComboBox_Encrypt" );
    Security_ComboBox_Encrypt->insertItem("None");
    Security_ComboBox_Encrypt->insertItem("WEP");
    Security_ComboBox_Encrypt->setGeometry( QRect( 440, 15, 120, 30 ) ); 

    Security_TextLabel3 = new QLabel( this, "Security_TextLabel3" );
    Security_TextLabel3->setGeometry( QRect( 20, 59, 120, 31 ) ); 
    Security_TextLabel3->setText("WPA Pre-Shared Key:");

    Security_LineEdit_PSK = new QLineEdit( this, "Security_LineEdit_PSK" );
    Security_LineEdit_PSK->setGeometry( QRect( 150, 59, 410, 30 ) );
    Security_LineEdit_PSK->setMaxLength(64);
    Security_LineEdit_PSK->setEnabled( FALSE );

    Security_ButtonGroup_Key = new QButtonGroup( this, "Security_ButtonGroup_Key" );
    Security_ButtonGroup_Key->setGeometry( QRect( 21, 98, 535, 170 ) ); 
    Security_ButtonGroup_Key->setTitle("WEP Key");

    Security_RadioButton_Key1 = new QRadioButton( Security_ButtonGroup_Key, "Security_RadioButton_Key1" );
    Security_RadioButton_Key1->setGeometry( QRect( 10, 20, 70, 30 ) ); 
    Security_RadioButton_Key1->setText("Key#1");
    Security_RadioButton_Key1->setEnabled( FALSE );
    Security_RadioButton_Key1->setChecked( TRUE );
    Security_ButtonGroup_Key->insert( Security_RadioButton_Key1, 0 );

    Security_RadioButton_Key2 = new QRadioButton( Security_ButtonGroup_Key, "Security_RadioButton_Key2" );
    Security_RadioButton_Key2->setGeometry( QRect( 10, 55, 70, 30 ) ); 
    Security_RadioButton_Key2->setText("Key#2");
    Security_RadioButton_Key2->setEnabled( FALSE );
    Security_RadioButton_Key2->setChecked( FALSE );
    Security_ButtonGroup_Key->insert( Security_RadioButton_Key2, 1 );

    Security_RadioButton_Key3 = new QRadioButton( Security_ButtonGroup_Key, "Security_RadioButton_Key3" );
    Security_RadioButton_Key3->setGeometry( QRect( 10, 90, 70, 30 ) ); 
    Security_RadioButton_Key3->setText("Key#3");
    Security_RadioButton_Key3->setEnabled( FALSE );
    Security_RadioButton_Key3->setChecked( FALSE );
    Security_ButtonGroup_Key->insert( Security_RadioButton_Key3, 2 );

    Security_RadioButton_Key4 = new QRadioButton( Security_ButtonGroup_Key, "Security_RadioButton_Key4" );
    Security_RadioButton_Key4->setGeometry( QRect( 10, 125, 70, 30 ) ); 
    Security_RadioButton_Key4->setText("Key#4");
    Security_RadioButton_Key4->setEnabled( FALSE );
    Security_RadioButton_Key4->setChecked( FALSE );
    Security_ButtonGroup_Key->insert( Security_RadioButton_Key4, 3 );

    Security_ComboBox_KeyType1 = new QComboBox( FALSE, Security_ButtonGroup_Key, "Security_ComboBox_KeyType1" );
    Security_ComboBox_KeyType1->insertItem("Hexadecimal");
    Security_ComboBox_KeyType1->insertItem("Ascii");
    Security_ComboBox_KeyType1->setGeometry( QRect( 90, 20, 120, 30 ) );
    Security_ComboBox_KeyType1->setEnabled( FALSE );

    Security_ComboBox_KeyType2 = new QComboBox( FALSE, Security_ButtonGroup_Key, "Security_ComboBox_KeyType2" );
    Security_ComboBox_KeyType2->insertItem("Hexadecimal");
    Security_ComboBox_KeyType2->insertItem("Ascii");
    Security_ComboBox_KeyType2->setGeometry( QRect( 90, 55, 120, 30 ) ); 
    Security_ComboBox_KeyType2->setEnabled( FALSE );

    Security_ComboBox_KeyType3 = new QComboBox( FALSE, Security_ButtonGroup_Key, "Security_ComboBox_KeyType3" );
    Security_ComboBox_KeyType3->insertItem("Hexadecimal");
    Security_ComboBox_KeyType3->insertItem("Ascii");
    Security_ComboBox_KeyType3->setGeometry( QRect( 90, 90, 120, 30 ) );
    Security_ComboBox_KeyType3->setEnabled( FALSE );

    Security_ComboBox_KeyType4 = new QComboBox( FALSE, Security_ButtonGroup_Key, "Security_ComboBox_KeyType4" );
    Security_ComboBox_KeyType4->insertItem("Hexadecimal");
    Security_ComboBox_KeyType4->insertItem("Ascii");
    Security_ComboBox_KeyType4->setGeometry( QRect( 90, 125, 120, 30 ) );
    Security_ComboBox_KeyType4->setEnabled( FALSE );

    m_hexValidator = new QHexValidator(this);

    Security_LineEdit_Key1Hex = new QLineEdit( Security_ButtonGroup_Key, "Security_LineEdit_Key1Hex" );
    Security_LineEdit_Key1Hex->setGeometry( QRect( 225, 20, 290, 30 ) ); 
    Security_LineEdit_Key1Hex->setMaxLength(26);
    Security_LineEdit_Key1Hex->setEnabled( FALSE );
    Security_LineEdit_Key1Hex->setValidator(m_hexValidator);

    Security_LineEdit_Key1Ascii = new QLineEdit( Security_ButtonGroup_Key, "Security_LineEdit_Key1Ascii" );
    Security_LineEdit_Key1Ascii->setGeometry( QRect( 225, 20, 290, 30 ) ); 
    Security_LineEdit_Key1Ascii->setMaxLength(13);
    Security_LineEdit_Key1Ascii->hide();
    Security_LineEdit_Key1Ascii->setEnabled( FALSE );

    Security_LineEdit_Key2Hex = new QLineEdit( Security_ButtonGroup_Key, "Security_LineEdit_Key2Hex" );
    Security_LineEdit_Key2Hex->setGeometry( QRect( 225, 55, 290, 30 ) ); 
    Security_LineEdit_Key2Hex->setMaxLength(26);
    Security_LineEdit_Key2Hex->setEnabled( FALSE );
    Security_LineEdit_Key2Hex->setValidator(m_hexValidator);

    Security_LineEdit_Key2Ascii = new QLineEdit( Security_ButtonGroup_Key, "Security_LineEdit_Key2Ascii" );
    Security_LineEdit_Key2Ascii->setGeometry( QRect( 225, 55, 290, 30 ) ); 
    Security_LineEdit_Key2Ascii->setMaxLength(13);
    Security_LineEdit_Key2Ascii->hide();
    Security_LineEdit_Key2Ascii->setEnabled( FALSE );

    Security_LineEdit_Key3Hex = new QLineEdit( Security_ButtonGroup_Key, "Security_LineEdit_Key3Hex" );
    Security_LineEdit_Key3Hex->setGeometry( QRect( 225, 90, 290, 30 ) ); 
    Security_LineEdit_Key3Hex->setMaxLength(26);
    Security_LineEdit_Key3Hex->setEnabled( FALSE );
    Security_LineEdit_Key3Hex->setValidator(m_hexValidator);

    Security_LineEdit_Key3Ascii = new QLineEdit( Security_ButtonGroup_Key, "Security_LineEdit_Key3Ascii" );
    Security_LineEdit_Key3Ascii->setGeometry( QRect( 225, 90, 290, 30 ) ); 
    Security_LineEdit_Key3Ascii->setMaxLength(13);
    Security_LineEdit_Key3Ascii->hide();
    Security_LineEdit_Key3Ascii->setEnabled( FALSE );

    Security_LineEdit_Key4Hex = new QLineEdit( Security_ButtonGroup_Key, "Security_LineEdit_Key4Hex" );
    Security_LineEdit_Key4Hex->setGeometry( QRect( 225, 125, 290, 30 ) ); 
    Security_LineEdit_Key4Hex->setMaxLength(26);
    Security_LineEdit_Key4Hex->setEnabled( FALSE );
    Security_LineEdit_Key4Hex->setValidator(m_hexValidator);

    Security_LineEdit_Key4Ascii = new QLineEdit( Security_ButtonGroup_Key, "Security_LineEdit_Key4Ascii" );
    Security_LineEdit_Key4Ascii->setGeometry( QRect( 225, 125, 290, 30 ) ); 
    Security_LineEdit_Key4Ascii->setMaxLength(13);
    Security_LineEdit_Key4Ascii->hide();
    Security_LineEdit_Key4Ascii->setEnabled( FALSE );

    PushButton_OK = new QPushButton( this, "PushButton_OK" );
    PushButton_OK->setGeometry( QRect( 144, 282, 120, 30 ) ); 
    PushButton_OK->setText("&OK");

    PushButton_Cancel = new QPushButton( this, "PushButton_Cancel" );
    PushButton_Cancel->setGeometry( QRect( 314, 282, 120, 30 ) ); 
    PushButton_Cancel->setText("&CANCEL");

    connect( PushButton_OK, SIGNAL( clicked() ), this, SLOT( OnOK() ) );
    connect( PushButton_Cancel, SIGNAL( clicked() ), this, SLOT( OnCancel() ) );
    connect( Security_ComboBox_AuthType, SIGNAL( activated(int) ), this, SLOT( Security_OnSelectAuthenType(int) ) );
    connect( Security_ComboBox_Encrypt, SIGNAL( activated(int) ), this, SLOT( Security_OnSelectEncryptType(int) ) );
    connect( Security_ComboBox_KeyType1, SIGNAL( activated(int) ), this, SLOT( Security_OnSelectKey1Type(int) ) );
    connect( Security_ComboBox_KeyType2, SIGNAL( activated(int) ), this, SLOT( Security_OnSelectKey2Type(int) ) );
    connect( Security_ComboBox_KeyType3, SIGNAL( activated(int) ), this, SLOT( Security_OnSelectKey3Type(int) ) );
    connect( Security_ComboBox_KeyType4, SIGNAL( activated(int) ), this, SLOT( Security_OnSelectKey4Type(int) ) );

    m_isClickOk = FALSE;
}

/*  
 *  Destroys the object and frees any allocated resources
 */
AuthSecuDlg::~AuthSecuDlg()
{
    // no need to delete child widgets, Qt does it all for us
}

int AuthSecuDlg::Security_GetWepKeyType(int index)
{
    int                                 num;

    switch (index)
    {
    case 1:
        num = Security_ComboBox_KeyType2->currentItem();
        break;
    case 2:
        num = Security_ComboBox_KeyType3->currentItem();
        break;
    case 3:
        num = Security_ComboBox_KeyType4->currentItem();
        break;
    case 0:
    default:
        num = Security_ComboBox_KeyType1->currentItem();
        break;
    }

    return num;
}

QString AuthSecuDlg::Security_GetWepKeyString(int index)
{
    switch (index)
    {
    case 1:
        if ( Security_GetWepKeyType(index) == 0) //Hex
            return (Security_LineEdit_Key2Hex->text());
        else
            return (Security_LineEdit_Key2Ascii->text());
        break;
    case 2:
        if ( Security_GetWepKeyType(index) == 0) //Hex
            return (Security_LineEdit_Key3Hex->text());
        else
            return (Security_LineEdit_Key3Ascii->text());
        break;
    case 3:
        if ( Security_GetWepKeyType(index) == 0) //Hex
            return (Security_LineEdit_Key4Hex->text());
        else
            return (Security_LineEdit_Key4Ascii->text());
        break;
    case 0:
    default:
        if ( Security_GetWepKeyType(index) == 0) //Hex
            return (Security_LineEdit_Key1Hex->text());
        else
            return (Security_LineEdit_Key1Ascii->text());
        break;
    }
}

QString AuthSecuDlg::Security_GetPSKString()
{
    return (Security_LineEdit_PSK->text());
}

NDIS_802_11_AUTHENTICATION_MODE AuthSecuDlg::Security_GetAuthticationMode()
{
    QString                             qstr;
    NDIS_802_11_AUTHENTICATION_MODE     mode = Ndis802_11AuthModeOpen;

    qstr = Security_ComboBox_AuthType->currentText();
    if (qstr.compare("OPEN") == 0)
        mode = Ndis802_11AuthModeOpen;
    else if (qstr.compare("SHARED") == 0)
        mode = Ndis802_11AuthModeShared;
    else if (qstr.compare("WPAPSK") == 0)
        mode = Ndis802_11AuthModeWPAPSK;

    return (mode);
}

NDIS_802_11_ENCRYPTION_STATUS AuthSecuDlg::Security_GetEncryptType()
{
    QString                             qstr;
    NDIS_802_11_ENCRYPTION_STATUS       type = Ndis802_11WEPDisabled;

    qstr = Security_ComboBox_Encrypt->currentText();
    if (qstr.compare("NONE") == 0)
        type = Ndis802_11WEPDisabled;
    else if (qstr.compare("WEP") == 0)
        type = Ndis802_11WEPEnabled;
    else if (qstr.compare("TKIP") == 0)
        type = Ndis802_11Encryption2Enabled;
    else if (qstr.compare("AES") == 0)
        type = Ndis802_11Encryption3Enabled;

    return (type);
}

void AuthSecuDlg::Security_SetAuthModeAndEncryType(NDIS_802_11_AUTHENTICATION_MODE mode, NDIS_802_11_ENCRYPTION_STATUS type)
{
    if ((mode == Ndis802_11AuthModeOpen) || (mode == Ndis802_11AuthModeShared))
    {
        Security_LineEdit_PSK->setEnabled( FALSE );

        Security_ComboBox_AuthType->clear();
        Security_ComboBox_AuthType->insertItem("OPEN");
        Security_ComboBox_AuthType->insertItem("SHARED");

        Security_ComboBox_Encrypt->clear();
        Security_ComboBox_Encrypt->insertItem("NONE");
        Security_ComboBox_Encrypt->insertItem("WEP");
        
        if (type == Ndis802_11WEPEnabled)
        {
            Security_ComboBox_Encrypt->setCurrentItem( 1 );

            Security_RadioButton_Key1->setEnabled( TRUE );
            Security_RadioButton_Key2->setEnabled( TRUE );
            Security_RadioButton_Key3->setEnabled( TRUE );
            Security_RadioButton_Key4->setEnabled( TRUE );

            Security_ComboBox_KeyType1->setEnabled( TRUE );
            Security_ComboBox_KeyType2->setEnabled( TRUE );
            Security_ComboBox_KeyType3->setEnabled( TRUE );
            Security_ComboBox_KeyType4->setEnabled( TRUE );

            Security_LineEdit_Key1Hex->setEnabled( TRUE );
            Security_LineEdit_Key1Ascii->setEnabled( TRUE );
            Security_LineEdit_Key2Hex->setEnabled( TRUE );
            Security_LineEdit_Key2Ascii->setEnabled( TRUE );
            Security_LineEdit_Key3Hex->setEnabled( TRUE );
            Security_LineEdit_Key3Ascii->setEnabled( TRUE );
            Security_LineEdit_Key4Hex->setEnabled( TRUE );
            Security_LineEdit_Key4Ascii->setEnabled( TRUE );
        }
        else
        {
            Security_ComboBox_Encrypt->setCurrentItem( 0 );

            Security_RadioButton_Key1->setEnabled( FALSE );
            Security_RadioButton_Key2->setEnabled( FALSE );
            Security_RadioButton_Key3->setEnabled( FALSE );
            Security_RadioButton_Key4->setEnabled( FALSE );

            Security_ComboBox_KeyType1->setEnabled( FALSE );
            Security_ComboBox_KeyType2->setEnabled( FALSE );
            Security_ComboBox_KeyType3->setEnabled( FALSE );
            Security_ComboBox_KeyType4->setEnabled( FALSE );

            Security_LineEdit_Key1Hex->setEnabled( FALSE );
            Security_LineEdit_Key1Ascii->setEnabled( FALSE );
            Security_LineEdit_Key2Hex->setEnabled( FALSE );
            Security_LineEdit_Key2Ascii->setEnabled( FALSE );
            Security_LineEdit_Key3Hex->setEnabled( FALSE );
            Security_LineEdit_Key3Ascii->setEnabled( FALSE );
            Security_LineEdit_Key4Hex->setEnabled( FALSE );
            Security_LineEdit_Key4Ascii->setEnabled( FALSE );
        }
    }
    else if ((mode == Ndis802_11AuthModeWPAPSK) || (mode == Ndis802_11AuthModeWPANone))
    {
        Security_LineEdit_PSK->setEnabled( TRUE );

        Security_ComboBox_AuthType->clear();
        if (mode == Ndis802_11AuthModeWPAPSK)
            Security_ComboBox_AuthType->insertItem("WPAPSK");
        else if (mode == Ndis802_11AuthModeWPANone)
            Security_ComboBox_AuthType->insertItem("WPA-None");
        else
        {
            Security_ComboBox_AuthType->insertItem("WPAPSK");
            Security_ComboBox_AuthType->insertItem("WPA-None");
        }

        Security_ComboBox_Encrypt->clear();
        Security_ComboBox_Encrypt->insertItem("TKIP");
        Security_ComboBox_Encrypt->insertItem("AES");

        if (type == Ndis802_11Encryption2Enabled )
            Security_ComboBox_Encrypt->setCurrentItem( 0 );
        else if(type == Ndis802_11Encryption3Enabled)
            Security_ComboBox_Encrypt->setCurrentItem( 1 );

        Security_RadioButton_Key1->setEnabled( FALSE );
        Security_RadioButton_Key2->setEnabled( FALSE );
        Security_RadioButton_Key3->setEnabled( FALSE );
        Security_RadioButton_Key4->setEnabled( FALSE );

        Security_ComboBox_KeyType1->setEnabled( FALSE );
        Security_ComboBox_KeyType2->setEnabled( FALSE );
        Security_ComboBox_KeyType3->setEnabled( FALSE );
        Security_ComboBox_KeyType4->setEnabled( FALSE );

        Security_LineEdit_Key1Hex->setEnabled( FALSE );
        Security_LineEdit_Key1Ascii->setEnabled( FALSE );
        Security_LineEdit_Key2Hex->setEnabled( FALSE );
        Security_LineEdit_Key2Ascii->setEnabled( FALSE );
        Security_LineEdit_Key3Hex->setEnabled( FALSE );
        Security_LineEdit_Key3Ascii->setEnabled( FALSE );
        Security_LineEdit_Key4Hex->setEnabled( FALSE );
        Security_LineEdit_Key4Ascii->setEnabled( FALSE );
    }
}

int AuthSecuDlg::Security_GetDefaultKeyId()
{
    int                                 keyId;

    keyId = Security_ButtonGroup_Key->id(Security_ButtonGroup_Key->selected());

    return (keyId);
}

void AuthSecuDlg::Security_SetDefaultKeyId(int keyId)
{
    Security_ButtonGroup_Key->setButton(keyId);
}

void AuthSecuDlg::Security_SetKeyTypeAndKeyString(int keyIndex, int keyType, char *keyString)
{
    switch (keyIndex)
    {
    case 0:
        if (keyType == 0) //Hex
        {
            Security_ComboBox_KeyType1->setCurrentItem( 0 );
            Security_LineEdit_Key1Hex->setText(keyString);
            Security_LineEdit_Key1Hex->show();
            Security_LineEdit_Key1Ascii->hide();
        }
        else
        {
            Security_ComboBox_KeyType1->setCurrentItem( 1 );
            Security_LineEdit_Key1Ascii->setText(keyString);
            Security_LineEdit_Key1Ascii->show();
            Security_LineEdit_Key1Hex->hide();
        }
        break;
    case 1:
        if (keyType == 0) //Hex
        {
            Security_ComboBox_KeyType2->setCurrentItem( 0 );
            Security_LineEdit_Key2Hex->setText(keyString);
            Security_LineEdit_Key2Hex->show();
            Security_LineEdit_Key2Ascii->hide();
        }
        else
        {
            Security_ComboBox_KeyType2->setCurrentItem( 1 );
            Security_LineEdit_Key2Ascii->setText(keyString);
            Security_LineEdit_Key2Ascii->show();
            Security_LineEdit_Key2Hex->hide();
        }
        break;
    case 2:
        if (keyType == 0) //Hex
        {
            Security_ComboBox_KeyType3->setCurrentItem( 0 );
            Security_LineEdit_Key3Hex->setText(keyString);
            Security_LineEdit_Key3Hex->show();
            Security_LineEdit_Key3Ascii->hide();
        }
        else
        {
            Security_ComboBox_KeyType3->setCurrentItem( 1 );
            Security_LineEdit_Key3Ascii->setText(keyString);
            Security_LineEdit_Key3Ascii->show();
            Security_LineEdit_Key3Hex->hide();
        }
        break;
    case 3:
        if (keyType == 0) //Hex
        {
            Security_ComboBox_KeyType4->setCurrentItem( 0 );
            Security_LineEdit_Key4Hex->setText(keyString);
            Security_LineEdit_Key4Hex->show();
            Security_LineEdit_Key4Ascii->hide();
        }
        else
        {
            Security_ComboBox_KeyType4->setCurrentItem( 1 );
            Security_LineEdit_Key4Ascii->setText(keyString);
            Security_LineEdit_Key4Ascii->show();
            Security_LineEdit_Key4Hex->hide();
        }
        break;
    }
}

void AuthSecuDlg::OnOK()
{
    QString                             qstrEncryp;
    QString                             qstrAuthen;
    QString                             qstr;
    int                                 keyid;
    char                                msg[255];

    qstrAuthen = Security_ComboBox_AuthType->currentText();
    qstrEncryp = Security_ComboBox_Encrypt->currentText();
    if (qstrAuthen.compare("WPAPSK") == 0)
    { //Use WPAPSK
        qstr = Security_LineEdit_PSK->text();
        if (qstr.length() < 8)
        {
            QMessageBox::warning(this, "Warning", "Invalid WPA Pre-Shared key. WPAPSK used field should use more than 8 characters.");
            Security_LineEdit_PSK->setFocus();
            return;
        }
    }
    else if (qstrEncryp.compare("WEP") == 0)
    {
        keyid = Security_ButtonGroup_Key->id(Security_ButtonGroup_Key->selected());
        qstr = Security_GetWepKeyString(keyid);
        if (Security_GetWepKeyType(keyid) == 0)
        {//Hex
            if ((qstr.length() != 10) && (qstr.length() != 26))
            {
                sprintf(msg, "Invalid WEP KEY length. Key should be 10 or 26 hex digits!");
                QMessageBox::warning(this, "Warning", msg);
                switch (keyid)
                {
                case 0:
                    Security_LineEdit_Key1Hex->setFocus();
                    break;
                case 1:
                    Security_LineEdit_Key2Hex->setFocus();
                    break;
                case 2:
                    Security_LineEdit_Key3Hex->setFocus();
                    break;
                case 3:
                    Security_LineEdit_Key4Hex->setFocus();
                    break;
                }
                return;
            }
        }
        else
        {//Ascii
            if ((qstr.length() != 5) && (qstr.length() != 13))
            {
                sprintf(msg, "Invalid WEP KEY length. Key should be 5 or 13 ascii characters!");
                QMessageBox::warning(this, "Warning", msg);
                switch (keyid)
                {
                case 0:
                    Security_LineEdit_Key1Ascii->setFocus();
                    break;
                case 1:
                    Security_LineEdit_Key2Ascii->setFocus();
                    break;
                case 2:
                    Security_LineEdit_Key3Ascii->setFocus();
                    break;
                case 3:
                    Security_LineEdit_Key4Ascii->setFocus();
                    break;
                }
                return;
            }
        }
    }

    m_isClickOk = TRUE;
    close();
}

void AuthSecuDlg::OnCancel()
{
    m_isClickOk = FALSE;
    close();
}

void AuthSecuDlg::Security_OnSelectAuthenType(int id)
{
    QString                             textAuthen;

    textAuthen = Security_ComboBox_AuthType->text(id);
    if ((textAuthen.compare("OPEN") == 0) || (textAuthen.compare("SHARED") == 0))
    {
        Security_LineEdit_PSK->setEnabled( FALSE );

        Security_ComboBox_Encrypt->clear();
        Security_ComboBox_Encrypt->insertItem("NONE");
        Security_ComboBox_Encrypt->insertItem("WEP");
        Security_ComboBox_Encrypt->setCurrentItem( 1 );

        Security_RadioButton_Key1->setEnabled( TRUE );
        Security_RadioButton_Key2->setEnabled( TRUE );
        Security_RadioButton_Key3->setEnabled( TRUE );
        Security_RadioButton_Key4->setEnabled( TRUE );

        Security_ComboBox_KeyType1->setEnabled( TRUE );
        Security_ComboBox_KeyType2->setEnabled( TRUE );
        Security_ComboBox_KeyType3->setEnabled( TRUE );
        Security_ComboBox_KeyType4->setEnabled( TRUE );

        Security_LineEdit_Key1Hex->setEnabled( TRUE );
        Security_LineEdit_Key1Ascii->setEnabled( TRUE );
        Security_LineEdit_Key2Hex->setEnabled( TRUE );
        Security_LineEdit_Key2Ascii->setEnabled( TRUE );
        Security_LineEdit_Key3Hex->setEnabled( TRUE );
        Security_LineEdit_Key3Ascii->setEnabled( TRUE );
        Security_LineEdit_Key4Hex->setEnabled( TRUE );
        Security_LineEdit_Key4Ascii->setEnabled( TRUE );

    } 
    else if (textAuthen.compare("WPAPSK") == 0)
    {
        Security_LineEdit_PSK->setEnabled( TRUE );

        Security_ComboBox_Encrypt->clear();
        Security_ComboBox_Encrypt->insertItem("TKIP");
        Security_ComboBox_Encrypt->insertItem("AES");
        Security_ComboBox_Encrypt->setCurrentItem( 0 );

        Security_RadioButton_Key1->setEnabled( FALSE );
        Security_RadioButton_Key2->setEnabled( FALSE );
        Security_RadioButton_Key3->setEnabled( FALSE );
        Security_RadioButton_Key4->setEnabled( FALSE );

        Security_ComboBox_KeyType1->setEnabled( FALSE );
        Security_ComboBox_KeyType2->setEnabled( FALSE );
        Security_ComboBox_KeyType3->setEnabled( FALSE );
        Security_ComboBox_KeyType4->setEnabled( FALSE );

        Security_LineEdit_Key1Hex->setEnabled( FALSE );
        Security_LineEdit_Key1Ascii->setEnabled( FALSE );
        Security_LineEdit_Key2Hex->setEnabled( FALSE );
        Security_LineEdit_Key2Ascii->setEnabled( FALSE );
        Security_LineEdit_Key3Hex->setEnabled( FALSE );
        Security_LineEdit_Key3Ascii->setEnabled( FALSE );
        Security_LineEdit_Key4Hex->setEnabled( FALSE );
        Security_LineEdit_Key4Ascii->setEnabled( FALSE );
    }
}

void AuthSecuDlg::Security_OnSelectEncryptType(int id)
{
    QString                             textAuthen;
    QString                             textEncry;

    textEncry = Security_ComboBox_Encrypt->text(id);
    textAuthen = Security_ComboBox_AuthType->currentText();
    if ((textAuthen.compare("OPEN") == 0) || (textAuthen.compare("SHARED") == 0))
    {
        if (textEncry.compare("NONE") == 0)
        {
            Security_RadioButton_Key1->setEnabled( FALSE );
            Security_RadioButton_Key2->setEnabled( FALSE );
            Security_RadioButton_Key3->setEnabled( FALSE );
            Security_RadioButton_Key4->setEnabled( FALSE );

            Security_ComboBox_KeyType1->setEnabled( FALSE );
            Security_ComboBox_KeyType2->setEnabled( FALSE );
            Security_ComboBox_KeyType3->setEnabled( FALSE );
            Security_ComboBox_KeyType4->setEnabled( FALSE );

            Security_LineEdit_Key1Hex->setEnabled( FALSE );
            Security_LineEdit_Key1Ascii->setEnabled( FALSE );
            Security_LineEdit_Key2Hex->setEnabled( FALSE );
            Security_LineEdit_Key2Ascii->setEnabled( FALSE );
            Security_LineEdit_Key3Hex->setEnabled( FALSE );
            Security_LineEdit_Key3Ascii->setEnabled( FALSE );
            Security_LineEdit_Key4Hex->setEnabled( FALSE );
            Security_LineEdit_Key4Ascii->setEnabled( FALSE );

            Security_ComboBox_AuthType->setCurrentItem(0);
        }
        else
        {
            Security_RadioButton_Key1->setEnabled( TRUE );
            Security_RadioButton_Key2->setEnabled( TRUE );
            Security_RadioButton_Key3->setEnabled( TRUE );
            Security_RadioButton_Key4->setEnabled( TRUE );

            Security_ComboBox_KeyType1->setEnabled( TRUE );
            Security_ComboBox_KeyType2->setEnabled( TRUE );
            Security_ComboBox_KeyType3->setEnabled( TRUE );
            Security_ComboBox_KeyType4->setEnabled( TRUE );

            Security_LineEdit_Key1Hex->setEnabled( TRUE );
            Security_LineEdit_Key1Ascii->setEnabled( TRUE );
            Security_LineEdit_Key2Hex->setEnabled( TRUE );
            Security_LineEdit_Key2Ascii->setEnabled( TRUE );
            Security_LineEdit_Key3Hex->setEnabled( TRUE );
            Security_LineEdit_Key3Ascii->setEnabled( TRUE );
            Security_LineEdit_Key4Hex->setEnabled( TRUE );
            Security_LineEdit_Key4Ascii->setEnabled( TRUE );
        }
    }
}

void AuthSecuDlg::Security_OnSelectKey1Type(int id)
{
    if (id==0)
    {//hex
        Security_LineEdit_Key1Hex->show();
        Security_LineEdit_Key1Ascii->hide();
    }
    else
    {//Ascii
        Security_LineEdit_Key1Hex->hide();
        Security_LineEdit_Key1Ascii->show();
    }
}

void AuthSecuDlg::Security_OnSelectKey2Type(int id)
{
    if (id==0)
    {//hex
        Security_LineEdit_Key2Hex->show();
        Security_LineEdit_Key2Ascii->hide();
    }
    else
    {//Ascii
        Security_LineEdit_Key2Hex->hide();
        Security_LineEdit_Key2Ascii->show();
    }
}

void AuthSecuDlg::Security_OnSelectKey3Type(int id)
{
    if (id==0)
    {//hex
        Security_LineEdit_Key3Hex->show();
        Security_LineEdit_Key3Ascii->hide();
    }
    else
    {//Ascii
        Security_LineEdit_Key3Hex->hide();
        Security_LineEdit_Key3Ascii->show();
    }
}

void AuthSecuDlg::Security_OnSelectKey4Type(int id)
{
    if (id==0)
    {//hex
        Security_LineEdit_Key4Hex->show();
        Security_LineEdit_Key4Ascii->hide();
    }
    else
    {//Ascii
        Security_LineEdit_Key4Hex->hide();
        Security_LineEdit_Key4Ascii->show();
    }
}

bool AuthSecuDlg::Security_IsClickOk()
{
    return (m_isClickOk);
}
