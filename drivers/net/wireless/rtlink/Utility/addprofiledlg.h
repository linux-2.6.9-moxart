#ifndef ADDPROFILEDLG_H
#define ADDPROFILEDLG_H

#include <qvariant.h>
#include <qdialog.h>
#include <qvalidator.h>
#include "rt_tool.h"
#include "qhexvalidator.h"

class QVBoxLayout; 
class QHBoxLayout; 
class QGridLayout; 
class QButtonGroup;
class QCheckBox;
class QComboBox;
class QLabel;
class QLineEdit;
class QListView;
class QListViewItem;
class QPushButton;
class QRadioButton;
class QSlider;
class QSpinBox;
class QTabWidget;
class QWidget;
class QHexValidator;

class AddProfileDlg : public QDialog
{ 
    Q_OBJECT

public:
    AddProfileDlg( int socket_id = 0 , const char *device_name = 0, 
        QWidget* parent = 0, const char* name = 0, 
        int Type = PROFILE_ADD, QListView* pListView = 0, 
        PRT_PROFILE_SETTING *pProfilePtr = 0, NDIS_802_11_MAC_ADDRESS *pBSsid = 0, 
        bool modal = FALSE, WFlags fl = 0 );
    ~AddProfileDlg();

    QLabel*                     TextLabel1;
    QLabel*                     TextLabel2;
    QLineEdit*                  LineEdit_ProfileName;
    QComboBox*                  ComboBox_SSID;
    QPushButton*                PushButton_OK;
    QPushButton*                PushButton_Cancel;

    QTabWidget*                 TabWidget_Config;
//System Configuration Page
    QWidget*                    ConfigPage_tab;
    QLabel*                     Config_TextLabel1;
    QLabel*                     Config_TextLabel2;
    QLabel*                     Config_TextLabel3;
    QLabel*                     Config_TextLabel4;
    QLabel*                     Config_TextLabel5;
    QLabel*                     Config_TextLabel6;
    QButtonGroup*               Config_ButtonGroup_PowerSaveing;
    QRadioButton*               Config_RadioButton_CAM;
    QRadioButton*               Config_RadioButton_PSMode;
    QComboBox*                  Config_ComboBox_NetworkType;
    QComboBox*                  Config_ComboBox_Preamble;
    QCheckBox*                  Config_CheckBox_RTS;
    QSlider*                    Config_Slider_RTS;
    QSpinBox*                   Config_SpinBox_RTS;
    QCheckBox*                  Config_CheckBox_Fragment;
    QSlider*                    Config_Slider_Fragment;
    QSpinBox*                   Config_SpinBox_Fragment;
    QLabel*                     Config_TextLabel_Channel;
    QComboBox*                  Config_ComboBox_Channel;
//Authentication_Security Page
    QWidget*                    SecurityPage_tab;
    QLabel*                     Security_TextLabel1;
    QLabel*                     Security_TextLabel2;
    QLabel*                     Security_TextLabel3;
    QComboBox*                  Security_ComboBox_AuthType;
    QComboBox*                  Security_ComboBox_Encrypt;
    QLineEdit*                  Security_LineEdit_PSK;
    QButtonGroup*               Security_ButtonGroup_Key;
    QRadioButton*               Security_RadioButton_Key1;
    QRadioButton*               Security_RadioButton_Key2;
    QRadioButton*               Security_RadioButton_Key3;
    QRadioButton*               Security_RadioButton_Key4;
    QComboBox*                  Security_ComboBox_KeyType1;
    QComboBox*                  Security_ComboBox_KeyType2;
    QComboBox*                  Security_ComboBox_KeyType3;
    QComboBox*                  Security_ComboBox_KeyType4;
    QLineEdit*                  Security_LineEdit_Key1Hex;
    QLineEdit*                  Security_LineEdit_Key1Ascii;
    QLineEdit*                  Security_LineEdit_Key2Hex;
    QLineEdit*                  Security_LineEdit_Key2Ascii;
    QLineEdit*                  Security_LineEdit_Key3Hex;
    QLineEdit*                  Security_LineEdit_Key3Ascii;
    QLineEdit*                  Security_LineEdit_Key4Hex;
    QLineEdit*                  Security_LineEdit_Key4Ascii;

    QHexValidator*              m_hexValidator;
    QListView*                  m_ProfileListView;

    int                         m_nSocketId;
    int                         m_type;
    char                        *m_strDeviceName;
    bool                        m_bIsProfileNull;
    bool                        m_IsAddToProfile;

    NDIS_802_11_MAC_ADDRESS         m_BSsid;
    PNDIS_802_11_BSSID_LIST_EX      m_pBssidList;
    PRT_PROFILE_SETTING             m_pProfileSetting;
    PRT_PROFILE_SETTING             m_pEditProfileSetting;

    void InitDlg();
    void GetAllSsid();
    void InitChannel();
    bool IsOk();
    void Config_SetCurrentSSID(QString text);
    NDIS_802_11_NETWORK_INFRASTRUCTURE Config_GetNetworkType();
    int Security_GetWepKeyType(int index);
    QString Security_GetWepKeyString(int index);
    QString Security_GetPSKString();
    NDIS_802_11_AUTHENTICATION_MODE Security_GetAuthticationMode();
    NDIS_802_11_ENCRYPTION_STATUS Security_GetEncryptType();
    void Security_SetAuthModeAndEncryType(NDIS_802_11_NETWORK_INFRASTRUCTURE NetworkType, NDIS_802_11_AUTHENTICATION_MODE mode, NDIS_802_11_ENCRYPTION_STATUS type);
    int Security_GetDefaultKeyId();
    void Security_SetDefaultKeyId(int keyId);
    void Security_SetKeyTypeAndKeyString(int keyIndex, int keyType, char *keyString);

public slots:
    virtual void Do_NotThing() { };
    virtual void OnOK();
    virtual void OnCancel();
    virtual void Config_OnSelectSSID(int id);
    virtual void Config_OnSelectNetworkType(int index);
    virtual void Config_OnEnableRTS(bool flag);
    virtual void Config_OnEnableFragment(bool flag);
    virtual void Config_OnRTSSliderChanged(int value);
    virtual void Config_OnFragmentSliderChanged(int value);
    virtual void Config_OnRTSSpinBoxChanged(int value);
    virtual void Config_OnFragmentSpinBoxChanged(int value);
    virtual void Security_OnSelectAuthenType(int id);
    virtual void Security_OnSelectEncryptType(int id);
    virtual void Security_OnSelectKey1Type(int id);
    virtual void Security_OnSelectKey2Type(int id);
    virtual void Security_OnSelectKey3Type(int id);
    virtual void Security_OnSelectKey4Type(int id);
};

#endif // ADDPROFILEDLG_H
