TEMPLATE	=app
CONFIG		+= qt warn_off release
INCLUDEPATH	+= .

HEADERS	+= raconfigui.h \
			cardselect.h \
			countryform.h \
			addprofiledlg.h \
			qhexvalidator.h \
			authsecudlg.h \
			hiddenssiddlg.h \
			configapi.h \
			sha1.h \
			rt_tool.h
					
SOURCES	+= raconfigui.cpp \
			cardselect.cpp \
			countryform.cpp \
			addprofiledlg.cpp \
			qhexvalidator.cpp \
			authsecudlg.cpp \
			hiddenssiddlg.cpp \
			configapi.cpp \
			sha1.cpp \
			rt_tool.cpp \
			raconfig.cpp

IMAGES	=  ico/rtlogo.png ico/adapter.xpm ico/RaConfig2500.xpm \
			ico/check16.xpm ico/uncheck16.xpm ico/radiooff.png ico/radioon.png \
			ico/handshak16.xpm ico/noactive16.xpm 

TARGET	= RaConfig2500