#ifndef RT_TOOL_H
#define RT_TOOL_H

#include <sys/socket.h>
#include <linux/types.h>
#include <linux/wireless.h>

#define CONFIGURATION_UI_VERSION                        "1.2.1.0"
#define CONFIGURATION_UI_DATE                           "2004-08-12"
#define NIC_DEVICE_NAME                                 "RT2500STA"

#define RT2500_SYSTEM_PATH                              "/etc/Wireless/RT2500STA/"
#define RT2500_SYSTEM_DATA                              "RT2500STA.dat"     //for driver init
#define RT2500_UI_PROFILE                               "RT2500STA.ui"     //for UI saving Profile.
#define RT2500_UI_TYPE                                  0x18140201

#if WIRELESS_EXT <= 11
#ifndef SIOCDEVPRIVATE
#define SIOCDEVPRIVATE                                  0x8BE0
#endif
#define SIOCIWFIRSTPRIV                                 SIOCDEVPRIVATE
#endif

#define RT_PRIV_IOCTL                                   SIOCIWFIRSTPRIV + 0x01
#define RTPRIV_IOCTL_SET								SIOCIWFIRSTPRIV + 0x02

//
// IEEE 802.11 OIDs
//
// Ralink defined OIDs
#define OID_GET_SET_TOGGLE                              0x8000

#define OID_802_11_BSSID                                0x0101
#define OID_802_11_SSID                                 0x0102
#define OID_802_11_NETWORK_TYPES_SUPPORTED              0x0103
#define OID_802_11_NETWORK_TYPE_IN_USE                  0x0104
#define OID_802_11_TX_POWER_LEVEL                       0x0105
#define OID_802_11_RSSI                                 0x0106
#define OID_802_11_RSSI_TRIGGER                         0x0107
#define OID_802_11_INFRASTRUCTURE_MODE                  0x0108
#define OID_802_11_FRAGMENTATION_THRESHOLD              0x0109
#define OID_802_11_RTS_THRESHOLD                        0x010A
#define OID_802_11_NUMBER_OF_ANTENNAS                   0x010B
#define OID_802_11_RX_ANTENNA_SELECTED                  0x010C
#define OID_802_11_TX_ANTENNA_SELECTED                  0x010D
#define OID_802_11_SUPPORTED_RATES                      0x010E
#define OID_802_11_DESIRED_RATES                        0x010F
#define OID_802_11_CONFIGURATION                        0x0110
#define OID_802_11_STATISTICS                           0x0111
#define OID_802_11_ADD_WEP                              0x0112
#define OID_802_11_REMOVE_WEP                           0x0113
#define OID_802_11_DISASSOCIATE                         0x0114
#define OID_802_11_POWER_MODE                           0x0115
#define OID_802_11_BSSID_LIST                           0x0116
#define OID_802_11_AUTHENTICATION_MODE                  0x0117
#define OID_802_11_PRIVACY_FILTER                       0x0118
#define OID_802_11_BSSID_LIST_SCAN                      0x0119
#define OID_802_11_WEP_STATUS                           0x011A
// Renamed to reflect better the extended set of encryption status
#define OID_802_11_ENCRYPTION_STATUS                    OID_802_11_WEP_STATUS
#define OID_802_11_RELOAD_DEFAULTS                      0x011B
// Added to allow key mapping and default keys
#define OID_802_11_ADD_KEY                              0x011C
#define OID_802_11_REMOVE_KEY                           0x011D
#define OID_802_11_ASSOCIATION_INFORMATION              0x011E
#define OID_802_11_TEST                                 0x011F

#define OID_802_3_CURRENT_ADDRESS                       0x0120
#define OID_GEN_RCV_OK                                  0x0121
#define OID_GEN_RCV_NO_BUFFER                           0x0122
#define OID_GEN_MEDIA_CONNECT_STATUS                    0x0123

#define RT_OID_DEVICE_NAME                              0x0200
#define RT_OID_802_11_PREAMBLE                          0x0201
#define RT_OID_802_11_LINK_STATUS                       0x0202
#define RT_OID_802_11_RESET_COUNTERS                    0x0203
#define RT_OID_802_11_AC_CAM                            0x0204
#if DBG
#define RT_OID_802_11_HARDWARE_REGISTER                 0x0205
#endif
#define RT_OID_802_11_RACONFIG                          0x0206
#define RT_OID_802_11_COUNTRY_REGION                    0x0207
#define    RT_OID_802_11_RADIO                          0x0208
#define RT_OID_802_11_RX_AGC_VGC_TUNING                 0x0209
#define RT_OID_802_11_EVENT_TABLE                       0x0210
#define RT_OID_802_11_MAC_TABLE                         0x0211
#define RT_OID_802_11_PHY_MODE                          0x0212
#define RT_OID_802_11_TX_PACKET_BURST                   0x0213
#define RT_OID_802_11_TURBO_MODE                        0x0214
#define RT_OID_802_11_AP_CONFIG                         0x0215
#define RT_OID_802_11_ACL                               0x0216
#define RT_OID_802_11_STA_CONFIG                        0x0217
#define RT_OID_VERSION_INFO                             0x0218

#define RT_OID_802_11_WDS                               0x0219
#define RT_OID_802_11_RADIUS_DATA                       0x0220
#define RT_OID_802_11_WPA_REKEY                         0x0221

#define RT_OID_802_11_ADD_WPA                           0x0222

typedef long            LONG;
typedef unsigned char   UCHAR;
typedef unsigned short  USHORT;
typedef unsigned long   ULONG;
typedef unsigned int    UINT;

typedef unsigned char*  PUCHAR;
typedef unsigned short* PUSHORT;
typedef int             BOOLEAN;

typedef unsigned long   DWORD;
typedef unsigned short  WORD;
typedef void            VOID;
typedef __u64           ULONGLONG;
typedef    ULONGLONG    LARGE_INTEGER;

#define REGSTR_COUNTRYREGION_MINIMUM        0   //minimum
#define REGSTR_COUNTRYREGION_FCC            0   // FCC, CH1-11
#define REGSTR_COUNTRYREGION_IC             1   // IC (Canada), CH1-11
#define REGSTR_COUNTRYREGION_ETSI           2   // ETSI, CH1-13
#define REGSTR_COUNTRYREGION_SPAIN          3   // SPAIN, CH10-11
#define REGSTR_COUNTRYREGION_FRANCE         4   // FRANCE, CH10-13
#define REGSTR_COUNTRYREGION_MKK            5   // MKK, CH14
#define REGSTR_COUNTRYREGION_MKK1           6   // MKK1(TELEC), CH1-14
#define REGSTR_COUNTRYREGION_ISRAEL         7   // ISRAEL, CH3-9
#define REGSTR_COUNTRYREGION_MAXIMUM        7   // maximum

typedef struct _PAIR_CHANNEL_FREQ_ENTRY
{
	ULONG	lChannel;	
    ULONG	lFreq;		
} PAIR_CHANNEL_FREQ_ENTRY, *PPAIR_CHANNEL_FREQ_ENTRY;

//
// IEEE 802.11 Structures and definitions
//
// new types for Media Specific Indications

#define NDIS_802_11_LENGTH_SSID         32
#define NDIS_802_11_LENGTH_RATES        8
#define NDIS_802_11_LENGTH_RATES_EX     16

#define ETH_LENGTH_OF_ADDRESS 6
#define MAX_NUM_OF_EVENT      10  // entry # in EVENT table
#define MAX_LEN_OF_MAC_TABLE  32

//
// Defines the state of the LAN media
//
typedef enum _NDIS_MEDIA_STATE
{
    NdisMediaStateConnected,
    NdisMediaStateDisconnected
} NDIS_MEDIA_STATE, *PNDIS_MEDIA_STATE;

typedef enum _NDIS_802_11_STATUS_TYPE
{
    Ndis802_11StatusType_Authentication,
    Ndis802_11StatusTypeMax    // not a real type, defined as an upper bound
} NDIS_802_11_STATUS_TYPE, *PNDIS_802_11_STATUS_TYPE;

typedef UCHAR   NDIS_802_11_MAC_ADDRESS[6];

typedef struct _NDIS_802_11_STATUS_INDICATION
{
    NDIS_802_11_STATUS_TYPE StatusType;
} NDIS_802_11_STATUS_INDICATION, *PNDIS_802_11_STATUS_INDICATION;

// mask for authentication/integrity fields
#define NDIS_802_11_AUTH_REQUEST_AUTH_FIELDS        0x0f

#define NDIS_802_11_AUTH_REQUEST_REAUTH             0x01
#define NDIS_802_11_AUTH_REQUEST_KEYUPDATE          0x02
#define NDIS_802_11_AUTH_REQUEST_PAIRWISE_ERROR     0x06
#define NDIS_802_11_AUTH_REQUEST_GROUP_ERROR        0x0E

typedef struct _NDIS_802_11_AUTHENTICATION_REQUEST
{
    ULONG Length;            // Length of structure
    NDIS_802_11_MAC_ADDRESS Bssid;
    ULONG Flags;
} NDIS_802_11_AUTHENTICATION_REQUEST, *PNDIS_802_11_AUTHENTICATION_REQUEST;

// Added new types for OFDM 5G and 2.4G
typedef enum _NDIS_802_11_NETWORK_TYPE
{
    Ndis802_11FH,
    Ndis802_11DS,
    Ndis802_11OFDM5,
    Ndis802_11OFDM24,
    Ndis802_11NetworkTypeMax    // not a real type, defined as an upper bound
} NDIS_802_11_NETWORK_TYPE, *PNDIS_802_11_NETWORK_TYPE;

typedef struct _NDIS_802_11_NETWORK_TYPE_LIST
{
    ULONG                       NumberOfItems;  // in list below, at least 1
    NDIS_802_11_NETWORK_TYPE    NetworkType [1];
} NDIS_802_11_NETWORK_TYPE_LIST, *PNDIS_802_11_NETWORK_TYPE_LIST;

typedef enum _NDIS_802_11_POWER_MODE
{
    Ndis802_11PowerModeCAM,
    Ndis802_11PowerModeMAX_PSP,
    Ndis802_11PowerModeFast_PSP,
    Ndis802_11PowerModeMax      // not a real mode, defined as an upper bound
} NDIS_802_11_POWER_MODE, *PNDIS_802_11_POWER_MODE;

typedef ULONG   NDIS_802_11_TX_POWER_LEVEL; // in milliwatts

//
// Received Signal Strength Indication
//
typedef LONG   NDIS_802_11_RSSI;           // in dBm

typedef struct _NDIS_802_11_CONFIGURATION_FH
{
    ULONG           Length;             // Length of structure
    ULONG           HopPattern;         // As defined by 802.11, MSB set
    ULONG           HopSet;             // to one if non-802.11
    ULONG           DwellTime;          // units are Kusec
} NDIS_802_11_CONFIGURATION_FH, *PNDIS_802_11_CONFIGURATION_FH;

typedef struct _NDIS_802_11_CONFIGURATION
{
    ULONG           Length;             // Length of structure
    ULONG           BeaconPeriod;       // units are Kusec
    ULONG           ATIMWindow;         // units are Kusec
    ULONG           DSConfig;           // Frequency, units are kHz
    NDIS_802_11_CONFIGURATION_FH    FHConfig;
} NDIS_802_11_CONFIGURATION, *PNDIS_802_11_CONFIGURATION;

typedef struct _NDIS_802_11_STATISTICS
{
    ULONG           Length;             // Length of structure
    LARGE_INTEGER   TransmittedFragmentCount;
    LARGE_INTEGER   MulticastTransmittedFrameCount;
    LARGE_INTEGER   FailedCount;
    LARGE_INTEGER   RetryCount;
    LARGE_INTEGER   MultipleRetryCount;
    LARGE_INTEGER   RTSSuccessCount;
    LARGE_INTEGER   RTSFailureCount;
    LARGE_INTEGER   ACKFailureCount;
    LARGE_INTEGER   FrameDuplicateCount;
    LARGE_INTEGER   ReceivedFragmentCount;
    LARGE_INTEGER   MulticastReceivedFrameCount;
    LARGE_INTEGER   FCSErrorCount;
} NDIS_802_11_STATISTICS, *PNDIS_802_11_STATISTICS;

typedef ULONG  NDIS_802_11_KEY_INDEX;
typedef ULONGLONG   NDIS_802_11_KEY_RSC;

// Key mapping keys require a BSSID
typedef struct _NDIS_802_11_KEY
{
    ULONG           Length;             // Length of this structure
    ULONG           KeyIndex;           
    ULONG           KeyLength;          // length of key in bytes
    NDIS_802_11_MAC_ADDRESS BSSID;
    NDIS_802_11_KEY_RSC KeyRSC;
    UCHAR           KeyMaterial[1];     // variable length depending on above field
} NDIS_802_11_KEY, *PNDIS_802_11_KEY;

typedef struct _NDIS_802_11_REMOVE_KEY
{
    ULONG           Length;             // Length of this structure
    ULONG           KeyIndex;           
    NDIS_802_11_MAC_ADDRESS BSSID;      
} NDIS_802_11_REMOVE_KEY, *PNDIS_802_11_REMOVE_KEY;

typedef struct _NDIS_802_11_WEP
{
    ULONG           Length;             // Length of this structure
    ULONG           KeyIndex;           // 0 is the per-client key, 1-N are the
                                        // global keys
    ULONG           KeyLength;          // length of key in bytes
    UCHAR           KeyMaterial[1];     // variable length depending on above field
} NDIS_802_11_WEP, *PNDIS_802_11_WEP;

typedef enum _NDIS_802_11_NETWORK_INFRASTRUCTURE
{
    Ndis802_11IBSS,
    Ndis802_11Infrastructure,
    Ndis802_11AutoUnknown,
    Ndis802_11InfrastructureMax         // Not a real value, defined as upper bound
} NDIS_802_11_NETWORK_INFRASTRUCTURE, *PNDIS_802_11_NETWORK_INFRASTRUCTURE;

// Add new authentication modes
typedef enum _NDIS_802_11_AUTHENTICATION_MODE
{
    Ndis802_11AuthModeOpen,
    Ndis802_11AuthModeShared,
    Ndis802_11AuthModeAutoSwitch,
    Ndis802_11AuthModeWPA,
    Ndis802_11AuthModeWPAPSK,
    Ndis802_11AuthModeWPANone,
    Ndis802_11AuthModeMax               // Not a real mode, defined as upper bound
} NDIS_802_11_AUTHENTICATION_MODE, *PNDIS_802_11_AUTHENTICATION_MODE;

typedef UCHAR   NDIS_802_11_RATES[NDIS_802_11_LENGTH_RATES];        // Set of 8 data rates
typedef UCHAR   NDIS_802_11_RATES_EX[NDIS_802_11_LENGTH_RATES_EX];  // Set of 16 data rates

typedef struct _NDIS_802_11_SSID
{
    ULONG   SsidLength;         // length of SSID field below, in bytes;
                                // this can be zero.
    UCHAR   Ssid[NDIS_802_11_LENGTH_SSID];           // SSID information field
} NDIS_802_11_SSID, *PNDIS_802_11_SSID;


typedef struct _NDIS_WLAN_BSSID
{
    ULONG                               Length;             // Length of this structure
    NDIS_802_11_MAC_ADDRESS             MacAddress;         // BSSID
    UCHAR                               Reserved[2];
    NDIS_802_11_SSID                    Ssid;               // SSID
    ULONG                               Privacy;            // WEP encryption requirement
    NDIS_802_11_RSSI                    Rssi;               // receive signal
                                                            // strength in dBm
    NDIS_802_11_NETWORK_TYPE            NetworkTypeInUse;
    NDIS_802_11_CONFIGURATION           Configuration;
    NDIS_802_11_NETWORK_INFRASTRUCTURE  InfrastructureMode;
    NDIS_802_11_RATES                   SupportedRates;
} NDIS_WLAN_BSSID, *PNDIS_WLAN_BSSID;

typedef struct _NDIS_802_11_BSSID_LIST
{
    ULONG           NumberOfItems;      // in list below, at least 1
    NDIS_WLAN_BSSID Bssid[1];
} NDIS_802_11_BSSID_LIST, *PNDIS_802_11_BSSID_LIST;

// Added Capabilities, IELength and IEs for each BSSID
typedef struct _NDIS_WLAN_BSSID_EX
{
    ULONG                               Length;             // Length of this structure
    NDIS_802_11_MAC_ADDRESS             MacAddress;         // BSSID
    UCHAR                               Reserved[2];
    NDIS_802_11_SSID                    Ssid;               // SSID
    ULONG                               Privacy;            // WEP encryption requirement
    NDIS_802_11_RSSI                    Rssi;               // receive signal
                                                            // strength in dBm
    NDIS_802_11_NETWORK_TYPE            NetworkTypeInUse;
    NDIS_802_11_CONFIGURATION           Configuration;
    NDIS_802_11_NETWORK_INFRASTRUCTURE  InfrastructureMode;
    NDIS_802_11_RATES_EX                SupportedRates;
    ULONG                               IELength;
    UCHAR                               IEs[1];
} NDIS_WLAN_BSSID_EX, *PNDIS_WLAN_BSSID_EX;

typedef struct _NDIS_802_11_BSSID_LIST_EX
{
    ULONG                   NumberOfItems;      // in list below, at least 1
    NDIS_WLAN_BSSID_EX      Bssid[1];
} NDIS_802_11_BSSID_LIST_EX, *PNDIS_802_11_BSSID_LIST_EX;

typedef struct _NDIS_802_11_FIXED_IEs 
{
    UCHAR Timestamp[8];
    USHORT BeaconInterval;
    USHORT Capabilities;
} NDIS_802_11_FIXED_IEs, *PNDIS_802_11_FIXED_IEs;

typedef struct _NDIS_802_11_VARIABLE_IEs 
{
    UCHAR ElementID;
    UCHAR Length;    // Number of bytes in data field
    UCHAR data[1];
} NDIS_802_11_VARIABLE_IEs, *PNDIS_802_11_VARIABLE_IEs;

typedef  ULONG   NDIS_802_11_FRAGMENTATION_THRESHOLD;

typedef  ULONG   NDIS_802_11_RTS_THRESHOLD;

typedef  ULONG   NDIS_802_11_ANTENNA;

typedef enum _NDIS_802_11_PRIVACY_FILTER
{
    Ndis802_11PrivFilterAcceptAll,
    Ndis802_11PrivFilter8021xWEP
} NDIS_802_11_PRIVACY_FILTER, *PNDIS_802_11_PRIVACY_FILTER;

// Added new encryption types
// Also aliased typedef to new name
typedef enum _NDIS_802_11_WEP_STATUS
{
    Ndis802_11WEPEnabled,
    Ndis802_11Encryption1Enabled = Ndis802_11WEPEnabled,
    Ndis802_11WEPDisabled,
    Ndis802_11EncryptionDisabled = Ndis802_11WEPDisabled,
    Ndis802_11WEPKeyAbsent,
    Ndis802_11Encryption1KeyAbsent = Ndis802_11WEPKeyAbsent,
    Ndis802_11WEPNotSupported,
    Ndis802_11EncryptionNotSupported = Ndis802_11WEPNotSupported,
    Ndis802_11Encryption2Enabled,
    Ndis802_11Encryption2KeyAbsent,
    Ndis802_11Encryption3Enabled,
    Ndis802_11Encryption3KeyAbsent
} NDIS_802_11_WEP_STATUS, *PNDIS_802_11_WEP_STATUS,
  NDIS_802_11_ENCRYPTION_STATUS, *PNDIS_802_11_ENCRYPTION_STATUS;

typedef enum _NDIS_802_11_RELOAD_DEFAULTS
{
    Ndis802_11ReloadWEPKeys
} NDIS_802_11_RELOAD_DEFAULTS, *PNDIS_802_11_RELOAD_DEFAULTS;

#define NDIS_802_11_AI_REQFI_CAPABILITIES      1
#define NDIS_802_11_AI_REQFI_LISTENINTERVAL    2
#define NDIS_802_11_AI_REQFI_CURRENTAPADDRESS  4

#define NDIS_802_11_AI_RESFI_CAPABILITIES      1
#define NDIS_802_11_AI_RESFI_STATUSCODE        2
#define NDIS_802_11_AI_RESFI_ASSOCIATIONID     4

typedef struct _NDIS_802_11_AI_REQFI
{
    USHORT Capabilities;
    USHORT ListenInterval;
    NDIS_802_11_MAC_ADDRESS  CurrentAPAddress;
} NDIS_802_11_AI_REQFI, *PNDIS_802_11_AI_REQFI;

typedef struct _NDIS_802_11_AI_RESFI
{
    USHORT Capabilities;
    USHORT StatusCode;
    USHORT AssociationId;
} NDIS_802_11_AI_RESFI, *PNDIS_802_11_AI_RESFI;

typedef struct _NDIS_802_11_ASSOCIATION_INFORMATION
{
    ULONG                   Length;
    USHORT                  AvailableRequestFixedIEs;
    NDIS_802_11_AI_REQFI    RequestFixedIEs;
    ULONG                   RequestIELength;
    ULONG                   OffsetRequestIEs;
    USHORT                  AvailableResponseFixedIEs;
    NDIS_802_11_AI_RESFI    ResponseFixedIEs;
    ULONG                   ResponseIELength;
    ULONG                   OffsetResponseIEs;
} NDIS_802_11_ASSOCIATION_INFORMATION, *PNDIS_802_11_ASSOCIATION_INFORMATION;

typedef struct _NDIS_802_11_AUTHENTICATION_EVENT
{
    NDIS_802_11_STATUS_INDICATION       Status;
    NDIS_802_11_AUTHENTICATION_REQUEST  Request[1];
} NDIS_802_11_AUTHENTICATION_EVENT, *PNDIS_802_11_AUTHENTICATION_EVENT;

typedef struct _NDIS_802_11_TEST
{
    ULONG Length;
    ULONG Type;
    union
    {
        NDIS_802_11_AUTHENTICATION_EVENT AuthenticationEvent;
        NDIS_802_11_RSSI RssiTrigger;
    };
} NDIS_802_11_TEST, *PNDIS_802_11_TEST;

// Ralink defined 
typedef enum _RT_802_11_PREAMBLE {
    Rt802_11PreambleLong,
    Rt802_11PreambleShort,
    Rt802_11PreambleAuto
} RT_802_11_PREAMBLE, *PRT_802_11_PREAMBLE;

typedef enum _RT_802_11_PHY_MODE {
    PHY_11BG_MIXED,
    PHY_11B,
    PHY_11A,
    PHY_11ABG_MIXED
} RT_802_11_PHY_MODE;

// put all proprietery for-query objects here to reduce # of Query_OID
typedef struct _RT_802_11_LINK_STATUS {
    ULONG   CurrTxRate;         // in units of 0.5Mbps
    ULONG   ChannelQuality;     // 0..100 %
    ULONG   TxByteCount;        // both ok and fail
    ULONG   RxByteCount;        // both ok and fail
} RT_802_11_LINK_STATUS, *PRT_802_11_LINK_STATUS;

typedef struct _RT_802_11_EVENT_LOG {
    LARGE_INTEGER   SystemTime;  // timestammp via NdisGetCurrentSystemTime()
    UCHAR           Addr[ETH_LENGTH_OF_ADDRESS];
    USHORT          Event;       // EVENT_xxx
} RT_802_11_EVENT_LOG, *PRT_802_11_EVENT_LOG;

typedef struct _RT_802_11_EVENT_TABLE {
    ULONG       Num;
    ULONG       Rsv;     // to align Log[] at LARGE_INEGER boundary
    RT_802_11_EVENT_LOG   Log[MAX_NUM_OF_EVENT];
} RT_802_11_EVENT_TABLE, PRT_802_11_EVENT_TABLE;

typedef struct _RT_802_11_MAC_ENTRY {
    UCHAR       Addr[ETH_LENGTH_OF_ADDRESS];
    UCHAR       Aid;
    UCHAR       Psm;     // 0:PWR_ACTIVE, 1:PWR_SAVE
} RT_802_11_MAC_ENTRY, *PRT_802_11_MAC_ENTRY;

typedef struct _RT_802_11_MAC_TABLE {
    ULONG       Num;
    RT_802_11_MAC_ENTRY Entry[MAX_LEN_OF_MAC_TABLE];
} RT_802_11_MAC_TABLE, *PRT_802_11_MAC_TABLE;

// structure for query/set hardware register - MAC, BBP, RF register
typedef struct _RT_802_11_HARDWARE_REGISTER {
    ULONG   HardwareType;       // 0:MAC, 1:BBP, 2:RF register
    ULONG   Offset;             // Q/S register offset addr
    ULONG   Data;               // R/W data buffer
} RT_802_11_HARDWARE_REGISTER, *PRT_802_11_HARDWARE_REGISTER;

// structure to tune BBP R13 "RX AGC VGC init"
typedef struct _RT_802_11_RX_AGC_VGC_TUNING {
    UCHAR   Duration;           // unit: sec
    UCHAR   FlaseCcaThreshold;  // delta of flase CCA happen in the latest "Duration"
    UCHAR   RxAgcVgcDelta;      // R13 += RxAgcVgcDelta whenever flase CCA >= threshold
    UCHAR   MaxRxAgcVgc;        // max value of R13
} RT_802_11_RX_AGC_VGC_TUNING, *PRT_802_11_RX_AGC_VGC_TUNING;

// structure to define Radius Data
typedef struct _RT_802_11_RADIUS_DATA {
	DWORD RediusIP;				// Radius Server IP Address
	ULONG RediusPort;           // Radius Server Port
	UCHAR RediusKey[64];
}RT_802_11_RADIUS_DATA, *PRT_802_11_RADIUS_DATA;

// structure to define WPA Group Key Rekey Interval
typedef struct _RT_802_11_WPA_REKEY {
	ULONG ReKeyMethod;          // mechanism for rekeying: 0:disable, 1: time-based, 2: packet-based
	ULONG ReKeyInterval;		// time-based: seconds, packet-based: kilo-packets
}RT_802_11_WPA_REKEY, *PRT_802_11_WPA_REKEY;

typedef struct _RT_802_11_AP_CONFIG {
    ULONG   EnableTxBurst;      // 0-disable, 1-enable
    ULONG   EnableTurboRate;    // 0-disable, 1-enable 72/100mbps turbo rate
    ULONG   IsolateInterStaTraffic;     // 0-disable, 1-enable isolation
    ULONG   HideSsid;           // 0-disable, 1-enable hiding
    ULONG   UseBGProtection;    // 0-AUTO, 1-always ON, 2-always OFF
    ULONG   UseShortSlotTime;   // 0-no use, 1-use 9-us short slot time
    ULONG   Rsv1;               // must be 0
    ULONG   Rsv2;               // must be 0
} RT_802_11_AP_CONFIG, *PRT_802_11_AP_CONFIG;

// structure to query/set STA_CONFIG
typedef struct _RT_802_11_STA_CONFIG {
    ULONG   EnableTxBurst;      // 0-disable, 1-enable
    ULONG   EnableTurboRate;    // 0-disable, 1-enable 72/100mbps turbo rate
    ULONG   UseBGProtection;    // 0-AUTO, 1-always ON, 2-always OFF
    ULONG   UseShortSlotTime;   // 0-no use, 1-use 9-us short slot time when applicable
    ULONG   UseOfdmRatesIn11gAdhoc; // 0-11b rates only (WIFI spec), 1 - allow OFDM rates
    ULONG   Rsv1;               // must be 0
    ULONG   Rsv2;               // must be 0
    ULONG   SystemErrorBitmap;  // ignore upon SET, return system error upon QUERY
} RT_802_11_STA_CONFIG, *PRT_802_11_STA_CONFIG;

typedef struct _RT_802_11_ACL_ENTRY {
    UCHAR   Addr[ETH_LENGTH_OF_ADDRESS];
    USHORT  Rsv;
} RT_802_11_ACL_ENTRY, *PRT_802_11_ACL_ENTRY;

typedef struct _RT_802_11_ACL {
    ULONG   Policy;             // 0-disable, 1-positive list, 2-negative list
    ULONG   Num;
    RT_802_11_ACL_ENTRY Entry[MAX_LEN_OF_MAC_TABLE];
} RT_802_11_ACL, *PRT_802_11_ACL;

typedef struct _RT_802_11_WDS {
    ULONG   Num;
    NDIS_802_11_MAC_ADDRESS MacAddress[3];
	ULONG	KeyLength;
	UCHAR	KeyMaterial[32];
} RT_802_11_WDS, *PRT_802_11_WDS;

typedef struct _RT_PROFILE_SETTING {
    ULONG                                       ProfileDataType; //0x18140201
    UCHAR                                       Profile[32+1];
    UCHAR                                       SSID[NDIS_802_11_LENGTH_SSID+1];
    ULONG                                       SsidLen;
    UINT                                        Channel;
    NDIS_802_11_AUTHENTICATION_MODE             Authentication; //Ndis802_11AuthModeOpen, Ndis802_11AuthModeShared
                                                                //Ndis802_11AuthModeWPAPSK
    NDIS_802_11_WEP_STATUS                      Encryption; //Ndis802_11WEPEnabled, Ndis802_11WEPDisabled
                                                            //Ndis802_11Encryption2Enabled, Ndis802_11Encryption3Enabled
    NDIS_802_11_NETWORK_INFRASTRUCTURE          NetworkType;
    UINT                                        KeyDefaultId;
    UINT                                        Key1Type;
    UINT                                        Key2Type;
    UINT                                        Key3Type;
    UINT                                        Key4Type;
    char                                        Key1[26+1];
    char                                        Key2[26+1];
    char                                        Key3[26+1];
    char                                        Key4[26+1];
    char                                        WpaPsk[32+1];
    ULONG                                       TransRate;
    UINT                                        TransPower;
    bool                                        RTSCheck;
    ULONG                                       RTS;
    bool                                        FragmentCheck;
    ULONG                                       Fragment;
    NDIS_802_11_POWER_MODE                      PSmode;
    RT_802_11_PREAMBLE                          PreamType;
    ULONG                                       AntennaRx;
    ULONG                                       AntennaTx;
    UINT                                        CountryRegion;
    UCHAR                                       reserved[64];
    struct  _RT_PROFILE_SETTING                 *Next;
} RT_PROFILE_SETTING, *PRT_PROFILE_SETTING;

typedef struct _RT_DEVICE_ADAPTER {
    char    *Device_Name;
    struct _RT_DEVICE_ADAPTER *Next;
} RT_DEVICE_ADAPTER, *PRT_DEVICE_ADAPTER;

typedef struct _RT_VERSION_INFO{
    UCHAR       DriverVersionW;
    UCHAR       DriverVersionX;
    UCHAR       DriverVersionY;
    UCHAR       DriverVersionZ;
    UINT        DriverBuildYear;
    UINT        DriverBuildMonth;
    UINT        DriverBuildDay;
} RT_VERSION_INFO, *PRT_VERSION_INFO;

int Open_Socket(void);
int OidQueryInformation(USHORT OidQueryCode, int socket_id, char *DeviceName, void *ptr, ULONG PtrLength);
int OidSetInformation(USHORT OidQueryCode, int socket_id, char *DeviceName, void *ptr, ULONG PtrLength);
UINT ConvertRssiToSignalQuality(NDIS_802_11_RSSI RSSI);
unsigned char BtoH(char ch);
void AtoH(char * src, UCHAR * dest, int destlen);
int PasswordHash(char *password, unsigned char *ssid, int ssidlength, unsigned char *output);

#define WPA_OUI_TYPE                        0x01F25000
#define WPA_OUI						0x00F25000

#define MAX_TX_POWER_LEVEL                100   /* mW */
#define MAX_RTS_THRESHOLD                 2347  /* byte count */

#define PROFILE_EDIT 1
#define PROFILE_ADD  0

#endif //RT_TOOL_H
