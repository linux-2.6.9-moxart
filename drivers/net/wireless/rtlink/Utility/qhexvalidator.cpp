/*
 ***************************************************************************
 * Ralink Tech Inc.
 * 4F, No. 2 Technology	5th	Rd.
 * Science-based Industrial	Park
 * Hsin-chu, Taiwan, R.O.C.
 *
 * (c) Copyright 2002, Ralink Technology, Inc.
 *
 * All rights reserved.	Ralink's source	code is	an unpublished work	and	the
 * use of a	copyright notice does not imply	otherwise. This	source code
 * contains	confidential trade secret material of Ralink Tech. Any attemp
 * or participation	in deciphering,	decoding, reverse engineering or in	any
 * way altering	the	source code	is stricitly prohibited, unless	the	prior
 * written consent of Ralink Technology, Inc. is obtained.
 ***************************************************************************

	Module Name:
	qhexvalidator.cpp

	Abstract:
		Implement hex validator for WEP security.

	Revision History:
	Who			When		  What
	--------	----------	  ----------------------------------------------
	Paul Wu		01-22-2003	  created

*/


#include <qvalidator.h>
#include <qwidget.h>
#include <malloc.h>

#include "qhexvalidator.h"

QHexValidator::QHexValidator ( QWidget * parent, const char * name )
  : QValidator(parent, name)
{

}

QHexValidator::~QHexValidator()
{}

QValidator::State QHexValidator::validate ( QString &str, int &pos ) const
{
	char *tmp;
	char ch;
	unsigned int i;
	bool ok=FALSE;

	if(str.length() == 0)
		return QValidator::Acceptable;

	tmp = (char *)malloc(str.length()+1);	
	if(!tmp)
		return QValidator::Invalid;
	strcpy(tmp, str.data());
	for(i=0; i<str.length(); i++)
	{
		ch = *(tmp+i);		
		if( ((ch >= 'a') && (ch <= 'f')) || ((ch >= 'A') && (ch <= 'F')) ||
		  ((ch >= '0') && (ch <= '9')) )
		{
			ok=TRUE;
		}
		else
		{
			ok=FALSE;
			break;
		}
	}

	//free(tmp);
	if(ok)
	{
		return QValidator::Acceptable;
	}
	else
	{
		return QValidator::Invalid;
	}
	pos=0; //avoid compile warning

}


