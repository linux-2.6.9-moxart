/*
 ***************************************************************************
 * Ralink Tech Inc.
 * 4F, No. 2 Technology 5th Rd.
 * Science-based Industrial Park
 * Hsin-chu, Taiwan, R.O.C.
 *
 * (c) Copyright 2002, Ralink Technology, Inc.
 *
 * All rights reserved. Ralink's source code is an unpublished work and the
 * use of a copyright notice does not imply otherwise. This source code
 * contains confidential trade secret material of Ralink Tech. Any attemp
 * or participation in deciphering, decoding, reverse engineering or in any
 * way altering the source code is stricitly prohibited, unless the prior
 * written consent of Ralink Technology, Inc. is obtained.
 ***************************************************************************

    Module Name:
    cardselect.h

    Abstract:
        Implement card select Dialog.

    Revision History:
    Who            When          What
    --------    ----------      ----------------------------------------------
    Paul Wu     01-22-2003      created

*/

#ifndef CARDSELECT_H
#define CARDSELECT_H

#include <qvariant.h>
#include <qdialog.h>
#include <rt_tool.h>

class QVBoxLayout; 
class QHBoxLayout; 
class QGridLayout; 
class QLabel;
class QListView;
class QListViewItem;
class QPushButton;


class CardSelect : public QDialog
{ 
    Q_OBJECT

public:
    CardSelect( int Socket_Id = 0, PRT_DEVICE_ADAPTER prtAdapter = NULL, QWidget* parent = 0, const char* name = 0, bool modal = FALSE, WFlags fl = 0 );
    ~CardSelect();

    QPushButton* OkPushButton;
    QListView*  AdapterListView;
    QLabel*     TextLabel1;
    QString     device;
    const char  *Get_Device_Name();
    bool        bClickOk;
    bool        isClickOk();
public slots:

    virtual void AdapterListView_doubleClicked();
    virtual void OkPushButton_Clicked();
};

#endif // CARDSELECT_H
