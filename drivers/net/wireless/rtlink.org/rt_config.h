/*
 ***************************************************************************
 * Ralink Tech Inc.
 * 4F, No. 2 Technology 5th Rd.
 * Science-based Industrial Park
 * Hsin-chu, Taiwan, R.O.C.
 *
 * (c) Copyright 2002, Ralink Technology, Inc.
 *
 * All rights reserved. Ralink's source code is an unpublished work and the
 * use of a copyright notice does not imply otherwise. This source code
 * contains confidential trade secret material of Ralink Tech. Any attemp
 * or participation in deciphering, decoding, reverse engineering or in any
 * way altering the source code is stricitly prohibited, unless the prior
 * written consent of Ralink Technology, Inc. is obtained.
 ***************************************************************************

    Module Name:
    rt_config.h

    Abstract:
    Central header file to maintain all include files for all NDIS
    miniport driver routines.

    Revision History:
    Who         When          What
    --------    ----------    ----------------------------------------------
    Rory Chen   12-21-2002    created

*/

#ifndef __RT_CONFIG_H__
#define __RT_CONFIG_H__

#define PROFILE_PATH                "/etc/Wireless/RT2500STA/RT2500STA.dat"
#define NIC_DEVICE_NAME             "RT2500STA"

/* Operational parameters that are set at compile time. */
#if !defined(__OPTIMIZE__)  ||  !defined(__KERNEL__)
#warning  You must compile this file with the correct options!
#warning  See the last lines of the source file.
#error  You must compile this driver with "-O".
#endif

#include <linux/config.h>
#include <linux/module.h>
#include <linux/version.h>
#include <linux/kernel.h>
#include <linux/string.h>
#include <linux/timer.h>
#include <linux/errno.h>
#include <linux/ioport.h>
#include <linux/slab.h>
#include <linux/interrupt.h>
#include <linux/pci.h>
#include <linux/netdevice.h>
#include <linux/etherdevice.h>
#include <linux/skbuff.h>
#include <linux/init.h>
#include <linux/delay.h>
#include <linux/ethtool.h>
#include <linux/wireless.h>
#include <linux/proc_fs.h>
#include <linux/delay.h>
#include <linux/if_arp.h>
#include <linux/ctype.h>

#if LINUX_VERSION_CODE >= 0x20407
#include <linux/mii.h>
#endif
#include <asm/processor.h>      /* Processor type for cache alignment. */
#include <asm/bitops.h>
#include <asm/io.h>
#include <asm/irq.h>
#include <asm/uaccess.h>

// The type definition has to be placed before including rt2460.h
#ifndef ULONG
#define CHAR            char
#define INT             int
#define UINT            u32
#define ULONG           u32
#define USHORT          u16
#define UCHAR           u8

#define BOOLEAN         u8
//#define LARGE_INTEGER s64
#define VOID            void
#define LONG            int
#define ULONGLONG       u64
typedef VOID            *PVOID;
typedef CHAR            *PCHAR;
typedef UCHAR           *PUCHAR;
typedef LONG            *PLONG;
typedef ULONG           *PULONG;

typedef union _LARGE_INTEGER {
#if 0	// mask by Victor Yu. 05-23-2006
	struct {
		ULONG LowPart;
		LONG HighPart;
	}vv;
#endif
	struct {
        	ULONG LowPart;
		LONG HighPart;
	} u;
	s64 QuadPart;
} LARGE_INTEGER;

#endif

#define IN
#define OUT
#define TRUE	1
#define FALSE	0

#define ETH_LENGTH_OF_ADDRESS			6
#define NDIS_STATUS                             INT
#define NDIS_STATUS_SUCCESS                     0x00
#define NDIS_STATUS_FAILURE                     0x01
#define NDIS_STATUS_RESOURCES                   0x03
#define NDIS_STATUS_MEDIA_DISCONNECT            0x04
#define NDIS_STATUS_MEDIA_CONNECT               0x05

#include    "rtmp_type.h"
#include    "rtmp_def.h"
#include    "rt2560.h"
#include    "rtmp.h"
#include    "mlme.h"
#include    "oid.h"
#include    "wpa.h"
#include    "md5.h"

#define DEBUG_TASK_DELAY        2000

enum rt2560_chips {
	RT2560A = 0,
};

#endif  // __RT_CONFIG_H__
