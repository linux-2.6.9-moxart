/****************************************************************************
 * Ralink Tech Inc.
 * 4F, No. 2 Technology 5th Rd.
 * Science-based Industrial Park
 * Hsin-chu, Taiwan, R.O.C.
 * (c) Copyright 2002, Ralink Technology, Inc.
 *
 * All rights reserved. Ralink's source code is an unpublished work and the
 * use of a copyright notice does not imply otherwise. This source code
 * contains confidential trade secret material of Ralink Tech. Any attemp
 * or participation in deciphering, decoding, reverse engineering or in any
 * way altering the source code is stricitly prohibited, unless the prior
 * written consent of Ralink Technology, Inc. is obtained.
 ****************************************************************************/

#include "rt_config.h"

/*
    ==========================================================================
    Description:
        authentication state machine init procedure
    Parameters:
        Sm - the state machine
    Note:
        the state machine looks like the following 
        
                                        AUTH_RSP_IDLE                   AUTH_RSP_WAIT_CHAL
    MT2_AUTH_CHALLENGE_TIMEOUT    auth_rsp_challenge_timeout_action    auth_rsp_challenge_timeout_action
    MT2_PEER_AUTH_ODD        peer_auth_at_auth_rsp_idle_action peer_auth_at_auth_rsp_wait_action
    MT2_PEER_DEAUTH                 peer_deauth_action                 peer_deauth_action
    ==========================================================================
 */
VOID AuthRspStateMachineInit(
    IN PRTMP_ADAPTER pAd, 
    IN PSTATE_MACHINE Sm, 
    IN STATE_MACHINE_FUNC Trans[]) 
{
    ULONG        NOW;

    StateMachineInit(Sm, (STATE_MACHINE_FUNC*)Trans, MAX_AUTH_RSP_STATE, MAX_AUTH_RSP_MSG, (STATE_MACHINE_FUNC)Drop, AUTH_RSP_IDLE, AUTH_RSP_MACHINE_BASE);

    // column 1
//  StateMachineSetAction(Sm, AUTH_RSP_IDLE, MT2_AUTH_CHALLENGEG_TIMEOUT, (STATE_MACHINE_FUNC)AuthRspChallengeTimeoutAction);
//  StateMachineSetAction(Sm, AUTH_RSP_IDLE, MT2_PEER_AUTH_ODD, (STATE_MACHINE_FUNC)PeerAuthAtAuthRspIdleAction);
    StateMachineSetAction(Sm, AUTH_RSP_IDLE, MT2_PEER_DEAUTH, (STATE_MACHINE_FUNC)PeerDeauthAction);

    // column 2
//  StateMachineSetAction(Sm, AUTH_RSP_WAIT_CHAL, MT2_PEER_AUTH_ODD, (STATE_MACHINE_FUNC)PeerAuthAtAuthRspWaitAction);
//  StateMachineSetAction(Sm, AUTH_RSP_WAIT_CHAL, MT2_AUTH_CHALLENGE_TIMEOUT, (STATE_MACHINE_FUNC)AuthRspChallengeTimeoutAction);
    StateMachineSetAction(Sm, AUTH_RSP_WAIT_CHAL, MT2_PEER_DEAUTH, (STATE_MACHINE_FUNC)PeerDeauthAction);

    // initialize timer
    RTMPInitTimer(pAd, &pAd->Mlme.AuthRspAux.AuthRspTimer, AuthRspChallengeTimeout);

    // initialize the random number generator
    NOW = jiffies;
    LfsrInit(pAd, NOW);
}


/*
    ==========================================================================
    Description:
        challenge time out, called by timer thread
    ==========================================================================
 */
VOID AuthRspChallengeTimeout(
    IN  unsigned long data) 
{
    RTMP_ADAPTER *pAd = (RTMP_ADAPTER *)data;
    
    DBGPRINT(RT_DEBUG_TRACE,"AUTH_RSP - AuthRspChallengeTimeout \n");
    MlmeEnqueue(&pAd->Mlme.Queue, AUTH_RSP_STATE_MACHINE, MT2_AUTH_CHALLENGE_TIMEOUT, 0, NULL);
    MlmeHandler(pAd);
}

/*
    ==========================================================================
    Description:
    ==========================================================================
*/
VOID PeerAuthSimpleRspGenAndSend(
    IN PRTMP_ADAPTER pAd, 
    IN PMACHDR Hdr, 
    IN USHORT Alg, 
    IN USHORT Seq, 
    IN USHORT Reason, 
    IN USHORT Status) 
{
    MACHDR            AuthHdr;
    UINT              FrameLen = 0;
    UCHAR            *OutBuffer = NULL;
    NDIS_STATUS       NStatus;

    NStatus = MlmeAllocateMemory(pAd, (PVOID)&OutBuffer);  //Get an unused nonpaged memory
    if (NStatus != NDIS_STATUS_SUCCESS)
        return;

    if (Reason == MLME_SUCCESS)
    {
        DBGPRINT(RT_DEBUG_TRACE, "Send AUTH response (seq#2)...\n");
        MgtMacHeaderInit(pAd, &AuthHdr, SUBTYPE_AUTH, 0, &Hdr->Addr2, &pAd->PortCfg.Bssid);
        MakeOutgoingFrame(OutBuffer,            &FrameLen, 
                          sizeof(MACHDR),       &AuthHdr, 
                          2,                    &Alg, 
                          2,                    &Seq, 
                          2,                    &Reason, 
                          END_OF_ARGS);
        MiniportMMRequest(pAd, OutBuffer, FrameLen);
    }
    else
    {
        MlmeFreeMemory(pAd, OutBuffer);
        DBGPRINT(RT_DEBUG_TRACE, "Peer AUTH fail...\n");
    }
}

/*
    ==========================================================================
    Description:
    ==========================================================================
*/
VOID PeerDeauthAction(
    IN PRTMP_ADAPTER pAd, 
    IN PMLME_QUEUE_ELEM Elem) 
{
    MACADDR     Addr2;
    USHORT      Reason;

    if (PeerDeauthSanity(pAd, Elem->Msg, Elem->MsgLen, &Addr2, &Reason))
    {
        if (INFRA_ON(pAd) && MAC_ADDR_EQUAL(&Addr2, &pAd->PortCfg.Bssid))
        {
            RTMPCancelTimer(&pAd->Mlme.AuthRspAux.AuthRspTimer);
            DBGPRINT(RT_DEBUG_TRACE,"AUTH_RSP - receive DE-AUTH from our AP\n");
            LinkDown(pAd);
        }
    }
    else
    {
        DBGPRINT(RT_DEBUG_TRACE,"AUTH_RSP - PeerDeauthAction() sanity check fail\n");
    }
}

